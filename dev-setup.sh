#!/bin/bash
#################################################################
#
# A script which can be used to install tools needed for
# development of vallex-tools and setup the development environment
#
# Run it with the help option to see more info:
#
# $ ./dev-setup.sh --help
#################################################################

LOG_FILE=$(pwd)/dev-setup.log


# Some minimal systems don't set this; also, it
# should make all messages be in english?
export LC_ALL=C.UTF-8
export LANG=C.UTF-8


#################################################
NODE_SERIES=14
NODE_EXECUTABLE="node"

#################################################

#################################################
#         CONSOLE COLOR UTILITY FUNCTIONS       #
#################################################

YELLOW="1;33m"
RED="0;31m"
GREEN="0;32m"
UNDERLINE="4m"
BOLD="1m"
REVERT_COLOR="0m"
STATUS_COL=10;

function status_msg {
    echo -e "$@" 1>&2;
    echo -e "$@" >> $LOG_FILE;
}

function progress_msg {
    echo -n "$@" 1>&2;
}

function status_start {
    msg="$@";
    msgln=`echo "$msg" | wc -c`;
    let ntabs=$STATUS_COL-\(\($msgln-1\)/8\)
    for i in `seq $ntabs`; do
        msg="$msg\t";
    done;
    echo -e -n "$msg" 1>&2;
    echo "" &>> $LOG_FILE
    echo "------------------------------------" &>> $LOG_FILE
    echo "$@" &>> $LOG_FILE
    echo "------------------------------------" &>> $LOG_FILE
}

function add_spaces_to_len { # string target_length
    str="$1"
    target_length="$2"
    strlen=`echo "$str" | wc -c`;
    let nspaces=$target_length-$strlen;
    for i in `seq $nspaces`; do
      str="$str ";
    done;
    echo -e "$str";
}

function colorize {
    echo "\e[$1 $2 \e[$REVERT_COLOR"
}

function status_ok {
    local msg
    if [ ! -z "$1" ]; then
        msg="($1)";
    fi;
    echo -e "\e[$GREEN OK \e[$REVERT_COLOR $msg" 1>&2;
    echo "---------------OK-------------------" &>> $LOG_FILE
}

function status_fail {
    echo -e "\e[$RED FAIL \e[$REVERT_COLOR" 1>&2;
    echo "--------------FAIL------------------" &>> $LOG_FILE
}

function status_warn {
    echo -e "\e[$YELLOW WARNING: \$@ \e[$REVERT_COLOR"
    echo "---------------WARN-----------------" &>> $LOG_FILE
}

function status_err {
    echo -e "\e[$RED ERROR: $@ \e[$REVERT_COLOR"
    echo "---------------ERROR-----------------" &>> $LOG_FILE
}

function status_end {
    echo "$@" 1>&2;
    echo "---------------OK-------------------" &>> $LOG_FILE
}

function to_upper {
    echo "$@" | tr [:lower:] [:upper:]
}

function to_lower {
    echo "$@" | tr [:upper:] [:lower:]
}

function ask_question {
    _OPTIONS=""
    _PROMPT="$1"
    shift

    # Read arguments and put them into a space separated list
    while [ $# -gt 0 ] ; do
        if [ -z "$_OPTIONS" ]; then
            _OPTIONS="$1";
            _DEFAULT="$1";
            # The default should be printed in yellow and have the first char underlined
            _OPT_PRINT=`echo $1 | sed -es"/^\(.\)\(.*\)/%e[$YELLOW%e[$UNDERLINE\1%e[$REVERT_COLOR%e[$YELLOW\2%e[$REVERT_COLOR/" | tr '%' '\\\\'`;
        else
            _OPTIONS="$_OPTIONS $1";
            # The the first char should be underlined
            _UNDERLINE_FIRST=`echo $1 | sed -es"/^\(.\)\(.*\)/%e[$UNDERLINE\1%e[$REVERT_COLOR\2/" | tr '%' '\\\\'`;
            _OPT_PRINT="$_OPT_PRINT/$_UNDERLINE_FIRST";
        fi;
        shift
    done

    # Print the prompt (-e to enable escape sequences --- color, underlines)
    echo -e -n "$_PROMPT [$_OPT_PRINT]? " >&2
    read _ANSWER >&2;
    if [ -z "$_ANSWER" ]; then
        _ANSWER="$_DEFAULT";
    fi;
    _NORMALIZED_ANSWER=`to_upper $_ANSWER`

    # Find which option corresponds to the answer
    for _OPT in $_OPTIONS; do
        _NORMALIZED_OPT=`echo $_OPT | tr [:lower:] [:upper:]`
        if [ "$_NORMALIZED_ANSWER" == "$_NORMALIZED_OPT" ]; then
            echo "$_OPT";
            return 0;
        fi;
        if echo "$_NORMALIZED_OPT" | grep -q "^$_NORMALIZED_ANSWER"; then
            echo "$_OPT";
            return 0;
        fi;
    done;
    echo $_DEFAULT;
    return 1;
}




#################################################
#             UTILITY FUNCTIONS                 #
#################################################

# Installs system packages
UPDATE_RUN=""
function install_pkgs {
    if [ -z $UPDATE_RUN ]; then
        sudo apt-get update
        UPDATE_RUN="Yes";
    fi;
    sudo DEBIAN_FRONTEND=noninteractive apt-get install -y $@
}

# Adds a package source to the system package manager
function add_repo {
    # Determine which distro we are running
    install_pkgs lsb-release apt-transport-https curl ca-certificates gnupg2
    NAME=$1
    URL=$2
    KEY_URL=$3
    DISTRO=$(lsb_release -s -c)
    curl -sSL $KEY_URL | sudo apt-key add -
    echo "deb $URL $DISTRO main" | sudo tee  /etc/apt/sources.list.d/$NAME.list
    echo "deb-src $URL $DISTRO main" | sudo tee  -a /etc/apt/sources.list.d/$NAME.list
    sudo apt-get update
    UPDATE_RUN="Yes";
}

# Adds an element to the path variable (by putting a line in ~/.bash_profile)
# Note that this relies on bash being the default shell,
# and execution of the complete content of ~/.bash_profile
function add_to_path {
    echo export PATH="$1"':$PATH' >> ~/.bash_profile
    export PATH="$!:$PATH"
}

# Determine whether a program is installed and return the installed
# version (or None if not installed)
function get_version {
    if ! which "$1" >/dev/null; then
      echo "None";
    else
        VERSION=`$1 --version 2>&1 | head -1 | sed -e's/[^0-9]*\([0-9.-]\+\).*/\1/'`;
        echo "$VERSION";
    fi;
}


#################################################
#          COMPONENT INSTALL SCRIPTS            #
#################################################

RET_CANCEL=2
RET_ERROR=1
RET_OK=0

# Available components
COMPONENTS="PYENV NODE YARN PYTHON_SCRIPT_INSTALLER POETRY PRE_COMMIT SELENIUM"

# The component path & versions are stored in a hash
# keyed by the upper-case component name
declare -A COMPONENT_PATHS
declare -A COMPONENT_VERSIONS
for COMP in $COMPONENTS; do
    COMPONENT_PATHS[$COMP]=""
    COMPONENT_VERSIONS[$COMP]=""
done;

# Python interpreter is handled specially
COMPONENT_PATHS[PYTHON]=""
COMPONENT_VERSIONS[PYTHON]=""

# Paths where misc. executables are stored
PYTHON_SCRIPT_PREFIX=$HOME/.local/bin # pipsi, pipx, poetry, pre-commit
NPM_SCRIPT_PREFIX=$HOME/.npm-packages/bin # yarn
PYENV_BINDIR=$HOME/.pyenv/bin # pyenv


# Utility functions for manipulating the component hash
function update_component_info {
    COMPONENT_PATHS[$1]="$2"
    COMPONENT_VERSIONS[$1]="$(get_version ${COMPONENT_PATHS[$1]}) $3"
}

function have_component {
    [ -n "${COMPONENT_PATHS[$1]}" ];
    return $?
}

function get_component {
    echo "${COMPONENT_PATHS[$1]}";
}

function get_component_version {
    echo "${COMPONENT_VERSIONS[$1]}";
}

function run_component {
    _COMPONENT=$(get_component $1)
    shift;
    $_COMPONENT $@;
}

# A function which tries to detect which components
# are already installed on the system; it updates the
# component hash with the available programs
function detect_available_components {
    # Pyenv & Python
    _VER=$(get_version $PYENV_BINDIR/pyenv)
    if [ "$_VER" != "None" ]; then
        update_component_info PYENV $PYENV_BINDIR/pyenv
        update_component_info PYTHON $(run_component PYENV which python3) "(pyenv)"
    else
        _VER=$(get_version python3)
        if [ "$_VER" != "None" ]; then
            update_component_info PYTHON $(which python3) "(system)"
        fi;
    fi;

    # Python script installer
    _VER=$(get_version $PYTHON_SCRIPT_PREFIX/pipsi);
    if [ "$_VER" != "None" ]; then
        update_component_info PYTHON_SCRIPT_INSTALLER $PYTHON_SCRIPT_PREFIX/pipsi "(source)"
    else
        _VER=$(get_version pipsi);
        if [ "$_VER" != "None" ]; then
            update_component_info PYTHON_SCRIPT_INSTALLER $(which pipsi) "(system)"
        fi;
    fi;
    _VER=$(get_version $PYTHON_SCRIPT_PREFIX/pipx);
    if [ "$_VER" != "None" ]; then
            update_component_info PYTHON_SCRIPT_INSTALLER $PYTHON_SCRIPT_PREFIX/pipx
    fi;

    # Pre-commit
    _VER=$(get_version $PYTHON_SCRIPT_PREFIX/pre-commit);
    if [ "$_VER" != "None" ]; then
        update_component_info PRE_COMMIT $PYTHON_SCRIPT_PREFIX/pre-commit
    fi;

    # Poetry
    _VER=$(get_version $PYTHON_SCRIPT_PREFIX/poetry)
    if [ "$_VER" != "None" ]; then
        update_component_info POETRY $PYTHON_SCRIPT_PREFIX/poetry
    fi;

    # Node
    _VER=$(get_version $NODE_EXECUTABLE)
    if [ "$_VER" != "None" ]; then
        update_component_info NODE $(which $NODE_EXECUTABLE)
    fi;

    # Yarn
    _VER=$(get_version $NPM_SCRIPT_PREFIX/yarn)
    if [ "$_VER" != "None" ]; then
        update_component_info YARN $NPM_SCRIPT_PREFIX/yarn
    fi;

    # Selenium
    _XVFB_VERSION=$(dpkg -s xvfb 2>/dev/null | grep Version | sed -e's/Version:\s*//')
    if [ -z "$_XVFB_VERSION" ]; then
        _XVFB_VERSION='None';
    fi;
    _CHR_VERSION=$(get_version chromium-browser)
    _CHDRV_VERSION=$(get_version chromedriver)
    _FF_VERSION=$(get_version firefox)
    _FFDRV_VERSION=$(get_version geckodriver)

    INFO="Chrome($_CHR_VERSION)%ChDriver($_CHDRV_VERSION)%Firefox($_FF_VERSION)%FFDriver($_FFDRV_VERSION)%Xvfb($_XVFB_VERSION)"
    COMPONENT_PATHS[SELENIUM]="$(which Xvfb):$(which chromium-browser):$(which chromedriver):$(which firefox):$(which geckodriver)"
    COMPONENT_VERSIONS[SELENIUM]="$INFO"
}

# A helper function which calls the function named install_{lower case COMPONENT_NAME}
# to installe the component given as argument
function install_component {
    _COMPONENT_NAME=$(to_lower "$1");
    _FUNC_NAME="install_$_COMPONENT_NAME";
    status_msg "Installing component $(colorize $YELLOW "$1")"
    $_FUNC_NAME
    return $?
}

function install_selenium {
    _versions=$(get_component_version SELENIUM)
    _CHR_VERSION=$(echo "$_versions" | cut -f 1 -d '%')
    _CHDRV_VERSION=$(echo "$_versions" | cut -f 2 -d '%')
    _FF_VERSION=$(echo "$_versions" | cut -f 3 -d '%')
    _FFDRV_VERSION=$(echo "$_versions" | cut -f 4 -d '%')
    _XVFB_VERSION=$(echo "$_versions" | cut -f 5 -d '%')
    _ret=$RET_OK

    if echo "$_CHR_VERSION" | grep -q "None"; then
        status_start "  Installing Chromium browser"
        install_pkgs chromium-browser &>> $LOG_FILE
        if [ $? -gt 0 ]; then
            status_fail
            _ret=$RET_ERROR
            _CHR_VERSION="None"
        else
            _CHR_VERSION=$(get_version chromium-browser)
            status_ok
        fi;
    fi;

    if echo "$_CHDRV_VERSION" | grep -q "None"; then
        status_start "  Installing Chromium driver for selenium"
        install_pkgs chromium-chromedriver &>> $LOG_FILE
        if [ $? -gt 0 ]; then
            status_fail
            _ret=$RET_ERROR
            _CHDRV_VERSION="None"
        else
            _CHDRV_VERSION=$(get_version chromedriver)
            status_ok
        fi;
    fi;

    if echo "$_FF_VERSION" | grep -q "None"; then
        status_start "  Installing Firefox browser"
        install_pkgs firefox &>> $LOG_FILE
        if [ $? -gt 0 ]; then
            status_fail
            _ret=$RET_ERROR
            _FF_VERSION="None"
        else
            _FF_VERSION=$(get_version firefox)
            status_ok
        fi;
    fi;

    if echo "$_CHDRV_VERSION" | grep -q "None"; then
        status_start "  Installing Firefox driver for selenium"
        install_pkgs firefox-geckodriver &>> $LOG_FILE
        if [ $? -gt 0 ]; then
            status_fail
            _ret=$RET_ERROR
            _FFDRV_VERSION="None"
        else
            _FFDRV_VERSION=$(get_version geckodriver)
            status_ok
        fi;
    fi;

    if echo "$_XVFB_VERSION" | grep -q "None"; then
        status_start "  Installing Xvfb (X11 virtual framebufer server)"
        install_pkgs xvfb &>> $LOG_FILE
        if [ $? -gt 0 ]; then
            status_fail
            _ret=$RET_ERROR
            _XVFB_VERSION='None';
        else
            _XVFB_VERSION=$(dpkg -s xvfb 2>/dev/null | grep Version)
            status_ok
        fi;
    fi;

    INFO="Chrome($_CHR_VERSION)%ChDriver($_CHDRV_VERSION)%Firefox($_FF_VERSION)%FFDriver($_FFDRV_VERSION)%Xvfb($_XVFB_VERSION)"
    COMPONENT_PATHS[SELENIUM]="$(which Xvfb):$(which chromium-browser):$(which chromedriver):$(which geckodriver)"
    COMPONENT_VERSIONS[SELENIUM]="$INFO"
    return $_ret
}




# Installs pyenv to manage multiple versions of python
# and use it to install python3.6
# If your system has python>=3.6, this can be skipped
# https://github.com/pyenv/pyenv
function install_pyenv {
    if ! have_component PYENV; then

        _python_version=`get_version python3`;

        if [ "$_python_version" != "None" -a '3.6' \< "$_python_version" -a "$INTERACTIVE" == "YES" ]; then
            status_msg "  Your system has a good enough Python ($_python_version) located at $(which python3)"
	    answer=$(ask_question "  Do you want to install pyenv (in order to install Python 3.6.8)" Yes No);
            echo "ANSWERED: $answer"
            if [ "$answer" == "No" ]; then
                # Make sure we have the virtualenv package otherwise
                # things will fail (see Issue 95)
                install_pkgs python3-venv
                update_component_info PYTHON $(which python3) "(system)"
                return $RET_CANCEL;
            fi;
        fi;

        status_start "  Installing dependencies (pyenv needs a compiler)"
        install_pkgs git make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev python3-venv virtualenv &>> $LOG_FILE
        if [ $? -gt 0 ]; then
            status_fail
            return $RET_ERROR
        fi;
        status_ok

        # Clone the pyenv repo
        status_start "  Cloning the pyenv repo"
        git clone https://github.com/pyenv/pyenv.git ~/.pyenv &>> $LOG_FILE
        if [ $? -gt 0 ]; then
            status_fail
            return $RET_ERROR
        fi;
        status_ok

        # Define the environment variable PYENV_ROOT
        echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bash_profile
        PYENV_ROOT="$HOME/.pyenv"

        # Add pyenv to the path
        add_to_path "$PYENV_ROOT/bin"

        # Add pyenv init to your shell
        echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bash_profile

        . ~/.bash_profile

        eval "$($PYENV_ROOT/bin/pyenv init -)"

        update_component_info PYENV $PYENV_ROOT/bin/pyenv
    else
        status_msg "  Pyenv v$(get_component_version PYENV) already installed: $(get_component PYENV)"
    fi;



    # Install & activate Python 3.6
    status_start "  Installing Python 3.6.8 using pyenv";
    run_component PYENV install --skip-existing 3.6.8 &>> $LOG_FILE
    if [ $? -gt 0 ]; then
        status_fail
        return $RET_ERROR
    fi;
    status_ok
    run_component PYENV local 3.6.8

    update_component_info PYTHON $(run_component PYENV which python3) "(pyenv)"

    return $RET_OK
}

# Installs the nodejs javascript engine
# https://nodejs.org/en/
# Investigate nvm for node installation (https://github.com/nvm-sh/nvm)
function install_node {
    _node_version=`get_version $NODE_EXECUTABLE`;

    if [ $_node_version != "None" -a '11.1' \< "$_node_version" -a '15.1' \> "$_node_version" -a "$INTERACTIVE" == "YES" ]; then
        status_msg "  Your system has a good enough NodeJS ($_node_version) located at $(which $NODE_EXECUTABLE)"
        answer=$(ask_question "  Do you want to install official nodejs" No Yes);
        if [ "$answer" == "No" ]; then
            update_component_info NODE $(which $NODE_EXECUTABLE) "(system)"
            return $RET_OK;
        fi;
    fi;

    # Add a package repo for nodejs
    status_start "  Adding nodesource repository ";
    add_repo nodesource https://deb.nodesource.com/node_$NODE_SERIES.x https://deb.nodesource.com/gpgkey/nodesource.gpg.key &>> $LOG_FILE
    if [ $? -gt 0 ]; then
        status_fail
        return $RET_ERROR
    fi;
    status_ok

    # Install nodejs
    status_start "  Installing nodejs version $NODE_SERIES.*";
    install_pkgs nodejs=$NODE_SERIES.\* &>> $LOG_FILE
    if [ $? -gt 0 ]; then
        status_fail
        return $RET_ERROR
    fi;
    status_ok

    # Set up npm package manager package installation directory
    # and add it to the path
    status_start "  Setting global npm package directory to ~/.npm-packages"
    mkdir -p ~/.npm-packages &>> $LOG_FILE
    echo "prefix=$HOME/.npm-packages" >> ~/.npmrc
    add_to_path ~/.npm-packages/bin &>> $LOG_FILE
    status_ok

    update_component_info NODE $(which $NODE_EXECUTABLE) "(nodesource package)"
    return $RET_OK;
}

# Installs the yarn javascript package manager
# https://yarnpkg.com/
function install_yarn {
    if have_component YARN; then
        status_msg "  Yarn v$(get_component_version YARN) already installed: $(get_component YARN)";
        return $RET_OK;
    fi;

    status_start "  Install yarn javascript package manager"
    if ! have_component NODE; then
        echo "  Yarn depends on the nodejs component which is not installed." &>> $LOG_FILE
        status_fail
        status_msg Yarn depends on the nodejs component which is not installed.
        return $RET_ERROR
    fi;
    npm install -g yarn &>> $LOG_FILE
    if [ $? -gt 0 ]; then
        status_fail
        return $RET_ERROR
    fi;
    status_ok

    update_component_info YARN $NPM_SCRIPT_PREFIX/yarn
    return $RET_OK
}

# Installs a python script installer
# (either pipsi which is unmaintained or the newer pipx)
# pipsi: https://github.com/mitsuhiko/pipsi
# pipx: https://pipxproject.github.io/pipx/
#
# pipsi can be installed from system packages (at least on ubuntu 18.04) or from source,
# pipx is installed via pip. Pipx is preferred. If run in interactive mode, the user is
# given a choice.
function install_python_script_installer {
    if have_component PYTHON_SCRIPT_INSTALLER; then
        status_msg "  Python script installer v$(get_component_version PYTHON_SCRIPT_INSTALLER) already installed: $(get_component PYTHON_SCRIPT_INSTALLER)";
        return $RET_OK;
    fi;

    _pipsi_version=$(get_version pipsi)
    _pipx_version=$(get_version pipx)

    if [ "$INTERACTIVE" == "YES" ]; then
        OPTIONS=""
        if [ "$_pipx_version" != "None" ]; then
          OPTIONS="A)pipx:$(which pipx)"
        else
          OPTIONS="A)pipx:pip-package"
        fi;
        if [ "$_pipsi_version" != "None" ]; then
          OPTIONS="$OPTIONS B)pipsi:$(which pipsi)"
        else
          _HAVE_PIPSI_PACKAGE=`apt-cache search pipsi`
          if [ -z "$_HAVE_PIPSI_PACKAGE" ]; then
              OPTIONS="$OPTIONS B)pipsi:source"
          else
              OPTIONS="$OPTIONS B)pipsi:system-package"
              OPTIONS="$OPTIONS C)pipsi:source"
          fi;
        fi;
        answer=$(ask_question "  Which python script installer do you want" $OPTIONS);
        case "$answer" in
            "A)pipx:$(which pipx)")
                # Add scripts installed by the script installer to $PATH
                add_to_path $PYTHON_SCRIPT_PREFIX

                update_component_info PYTHON_SCRIPT_INSTALLER $(which pipx)
                return $RET_OK
                ;;
            "B)pipsi:$(which pipsi)")
                # Add scripts installed by the script installer to $PATH
                add_to_path $PYTHON_SCRIPT_PREFIX

                update_component_info PYTHON_SCRIPT_INSTALLER $(which pipsi)
                return $RET_OK
                ;;
            "A)pipx:pip-package")
                _VARIANT=pipx
                ;;
            "B)pipsi:system-package")
                _VARIANT=pipsi-package
                ;;
            "B)pipsi:source")
                _VARIANT=pipsi-source
                ;;
            "C)pipsi:source")
                _VARIANT=pipsi-source
                ;;
        esac;
    else
        _VARIANT=pipx
    fi;

    case $_VARIANT in
        pipx)
            _pip3=$(which pip3)
            if [ -z "$_pip3" ]; then
                status_start "  Install pip3"
                install_pkgs python3-pip &>> $LOG_FILE
                if [ $? -gt 0 ]; then
                    status_fail
                    return $RET_ERROR
                fi;
                status_ok
            fi;
            status_start "  Install pipx script installer using pip"
            python3 -m pip install --user pipx &>> $LOG_FILE
            if [ $? -gt 0 ]; then
                status_fail
                return $RET_ERROR
            fi;
            status_ok
            python3 -m pipx ensurepath

            # Add scripts installed by the script installer to $PATH
            add_to_path $PYTHON_SCRIPT_PREFIX

            update_component_info PYTHON_SCRIPT_INSTALLER $HOME/.local/bin/pipx
            return $RET_OK
            ;;
        pipsi-source)
            status_start "  Install pipsi script installer from git"
            curl -s https://raw.githubusercontent.com/mitsuhiko/pipsi/master/get-pipsi.py | python3 &>> $LOG_FILE
            if [ $? -gt 0 ]; then
                status_fail
                return $RET_ERROR
            fi;
            status_ok

            # Add scripts installed by the script installer to $PATH
            add_to_path $PYTHON_SCRIPT_PREFIX

            update_component_info PYTHON_SCRIPT_INSTALLER $HOME/.local/bin/pisi "(git source)"
            return $RET_OK
            ;;
        pipsi-package)
            status_start "  Install pipsi script installer from system repo"
            install_pkgs pipsi &>> $LOG_FILE
             if [ $? -gt 0 ]; then
                status_fail
                return $RET_ERROR
            fi;
            status_ok

            # Add scripts installed by the script installer to $PATH
            add_to_path $PYTHON_SCRIPT_PREFIX

            update_component_info PYTHON_SCRIPT_INSTALLER $(which pipsi) "(system package)"
            return $RET_OK
            ;;
    esac;

}

# Installs the poetry python package & dependency manager
# https://python-poetry.org/
function install_poetry {
    if have_component POETRY; then
        status_msg "  Poetry v$(get_component_version POETRY) already installed: $(get_component POETRY)";
        return $RET_OK;
    fi;

    status_start "  Install poetry Python dependency manager"
    if ! have_component PYTHON_SCRIPT_INSTALLER; then
        echo "  Poetry depends on a python script installer which is not installed." &>> $LOG_FILE
        status_fail
        status_msg "  Poetry depends on a python script installer which is not installed."
        return $RET_ERROR
    fi;

    run_component PYTHON_SCRIPT_INSTALLER install poetry &>> $LOG_FILE
    if [ $? -gt 0 ]; then
        status_fail
        return $RET_ERROR
    fi;
    status_ok

    update_component_info POETRY $PYTHON_SCRIPT_PREFIX/poetry
    return $RET_OK
}

# Installs the pre-commit git hook manager
# https://pre-commit.com/
function install_pre_commit {
    if have_component PRE_COMMIT; then
        status_msg "  Pre-commit v$(get_component_version PRE_COMMIT) already installed: $(get_component PRE_COMMIT)";
        return $RET_OK;
    fi;

    status_start "  Install pre-commit program to run git hooks"
    if ! have_component PYTHON_SCRIPT_INSTALLER; then
        echo "  pre-commit depends on a python script installer which is not installed." &>> $LOG_FILE
        status_fail
        status_msg "  pre-commit depends on a python script installer which is not installed."
        return $RET_ERROR
    fi;

    run_component PYTHON_SCRIPT_INSTALLER install pre-commit &>> $LOG_FILE
    if [ $? -gt 0 ]; then
        status_fail
        return $RET_ERROR
    fi;
    status_ok
    update_component_info PRE_COMMIT $PYTHON_SCRIPT_PREFIX/pre-commit
    return $RET_OK
}


# Setup the development environment
#   -- install python dependencies
#   -- install git repo hooks
#   -- install javascript packages
#   -- creates the vallex/__version__ file
function setup_dev_env {
    # Install python dependencies

    status_start "  Creating a virtual environment in .venv"
    # Remove any leftover virtual environment first
    # since if pipenv removes it, it removes it with .venv and then
    # does creates the virtual env somewhere under ~/.local which
    # is not what we want or expect.
    if [ -d .venv ]; then
        rm -rf .venv;
    fi;
    run_component PYENV exec python3 -m venv .venv
    if [ $? -gt 0 ]; then
        status_fail
    else
        status_ok
    fi;

    touch .venv/.empty # git doesn't track empty directories, so we have .venv/.empty in git

    status_start "  Upgrading pip"

    # Upgrade pip first
    run_component POETRY run pip install --upgrade pip &>> $LOG_FILE
    if [ $? -gt 0 ]; then
        status_fail
    else
        status_ok
    fi;

    status_start "  Installing python dependencies (poetry install)"
    run_component POETRY install --no-root -E server &>> $LOG_FILE
    if [ $? -gt 0 ]; then
        status_fail
    else
        status_ok
    fi;



    # Install git hooks
    status_start "  Installing repo commit-hooks"
    run_component PRE_COMMIT install &>> $LOG_FILE
    if [ $? -gt 0 ]; then
        status_fail
    else
        status_ok
    fi;

    # Use yarn to install the necessary javascript packages
    # (see vallex/server/frontend/package.json)
    status_start "  Installing javascript dependencies"
    pushd vallex/server/frontend &>> $LOG_FILE
    run_component YARN install &>> $LOG_FILE
    if [ $? -gt 0 ]; then
        status_fail
    else
        status_ok
    fi;
    popd &>> $LOG_FILE

    # Create vallex/__version__ so that the scripts & web ui can
    # display version info
    status_start "  Creating vallex/__version__"
    ./deploy/update_version.sh
    status_ok
}

function print_help {
    echo -e "$(colorize $YELLOW dev-setup.sh): A script to help prepare a development environment."
    echo
    echo -e "$(colorize $BOLD Commands)"
    echo "   install [component]    ... installs the specified component or all of them"
    echo "           AVAILABLE COMPONENTS: "
    echo -e "          $(colorize $YELLOW "$COMPONENTS")"
    echo
    echo "   setup                  ... sets up the development environment (installs commit hooks, packages, ...)"
    echo
    echo "   status                 ... shows versions of available components"
    echo
    echo -e "$(colorize $BOLD Options)"
    echo "  --batch         do not ask any questions and use defaults"
    echo "  --help          print this help"
    echo
    echo "The script tries to install the following components"
    echo
    echo -e "$(colorize $YELLOW pyenv)       ... a program to manage & install different python versions"
    echo "(see https://github.com/pyenv/pyenv)"
    echo "pyenv is not strictly required (but recommended) if your system"
    echo "provides a python version >= 3.6."
    echo
    echo -e "$(colorize $YELLOW pipsi/pipx)  ... a program to make installing python scripts easier"
    echo "(see pipsi: https://github.com/mitsuhiko/pipsi, pipx: https://pipxproject.github.io/pipx)"
    echo "these scripts is not strictly required, however the setup script depends on them"
    echo "and if these are not available, it will fail to install some components which you must"
    echo "then install yourself"
    echo
    echo -e "$(colorize $YELLOW poetry)      ... a program to manage python package dependencies"
    echo "(see https://python-poetry.org)"
    echo "poetry is used to manage python dependencies of the project,"
    echo "building the python package, etc. See also pyproject.toml, poetry.lock & poetry.toml."
    echo
    echo -e "$(colorize $YELLOW pre-commit)  ... a program to manage git commit hooks"
    echo "(see https://pre-commit.com)"
    echo "This program is used to install & setup commit hooks for the repository"
    echo "The commit hooks are not strictly necessary, but are strongly recommended"
    echo "Before every commit they run lint programs (to check for python/javascript coding style),"
    echo "a static type checker to check that your code has correctly typed variables,"
    echo "and finally it runs a battery of (mostly) unit-test to test that your code does not,"
    echo "introduce old bugs."
    echo
    echo -e "$(colorize $YELLOW nodejs/yarn) ... The NodeJS javascript engine & Yarn javascript package manager"
    echo "(see nodejs: https://nodejs.org/en/, yarn: https://yarnpkg.com/)"
    echo "These are used to build the frontend files."
    echo
    echo -e "$(colorize $YELLOW selenium)    ... The Selenium tools for automated UI testing (chromium, firefox, xvfb & drivers)"
    echo "(see https://www.selenium.dev/)"
    echo "These are used to run automated tests of the web interface."
    echo
    echo -e "The script logs its operation into $(colorize $YELLOW $LOG_FILE)."
}

function print_components {
    echo -e "----------------------- $(colorize $YELLOW "Installed Components") -----------------------"
    for COMP in $COMPONENTS PYTHON; do
        echo -n "$(add_spaces_to_len $COMP 30)"
        if have_component $COMP; then
          VER=$(get_component_version $COMP)
          VER="$(add_spaces_to_len "$VER" 15)"
          echo -n -e "$(colorize $GREEN "$VER")"
          echo $(get_component $COMP)
        else
          echo -e $(colorize $RED "Not found")
        fi;
    done;
    echo -e "---------------------------------------------------------------------"
}

# Process command line options
INTERACTIVE="YES"
COMMAND=""
ARGUMENTS=""
while [ $# -gt 0 ] ; do
    case "$1" in
        --batch)
            INTERACTIVE="NO"
            ;;
        --help)
            print_help
            exit 0
            ;;
        *)
            if [ -z "$COMMAND" ]; then
                COMMAND="$1";
            else
                ARGUMENTS="$ARGUMENTS $1";
            fi;
            ;;
    esac;
    shift;
done

if [ -z "$COMMAND" ]; then
    COMMAND="install"
fi;

detect_available_components
case $COMMAND in
    install)
        if [ -z $ARGUMENTS ]; then
            COMPS=$COMPONENTS;
        else
            COMPS=$ARGUMENTS;
        fi;
        for COMP in $COMPS; do
            install_component $COMP;
            echo
        done;
        if [ -z $ARGUMENTS ]; then
            echo
            status_msg "Setting up the development environment"
            setup_dev_env
        fi;
        echo
        print_components
        ;;
    status)
        echo
        print_components
        ;;
    setup)
        echo
        status_msg "Setting up the development environment"
        setup_dev_env
        ;;
esac

#install_python_dev
#install_javascript_dev

