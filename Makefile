# If pipev is installed, run commands through poetry
# otherwise assume the user has the necessary commands
# in the path (either by installing them globally with
# pip or by activating a correct virtualenv)

POETRY=$(shell which poetry)
DISTRO=$(shell lsb_release -s -c)
GITLAB_CI_YAML=$(shell echo "import yaml, json%c=json.dumps(json.dumps(yaml.safe_load(open('.gitlab-ci.yml','r', encoding='utf-8'))))%print('{\"content\":',c,'}')" | tr '%' "\n" | $(POETRY) run python3)

YELLOW="1\;33m"
RED="0\;31m"
GREEN="0\;32m"
UNDERLINE="4m"
BOLD="1m"
REVERT_COLOR="0m"

ifeq ($(POETRY), '')
	TOX=.venv/bin/tox
else
	TOX=poetry run tox
endif

verbosity = ERROR

.EXPORT_ALL_VARIABLES:

help:
	@echo "\e[$(BOLD) Makefile for running different development scripts for pyvallex\e[$(REVERT_COLOR)	"
	@echo "												       	"
	@echo " Available targets:                                                                             	"
	@echo "												       	"
	@echo "\e[$(YELLOW) Development\e[$(REVERT_COLOR)                                                       "
	@echo " -----------                                                                                    	"
	@echo "\e[$(BOLD)	lint-gitlab-ci\e[$(REVERT_COLOR)		checks the .gitlab-ci.yml file which configures Continuous integration 	"
	@echo "				for syntax errors							"
	@echo "\e[$(BOLD)	test\e[$(REVERT_COLOR)			runs the test suite (via pytest, not through tox, so should be slightly "
	@echo "				faster, but does not setup a clean environment)                         "
	@echo "\e[$(BOLD)	ui-test\e[$(REVERT_COLOR)			same as test but runs just the selenium (UI) tests"
	@echo "\e[$(BOLD)	showdoc\e[$(REVERT_COLOR)			builds the documentation and opens it in a browser on 			"
	@echo "				http://localhost:8331     						"
	@echo "\e[$(BOLD)	bench\e[$(REVERT_COLOR)			run the bechmark suite                                                  "
	@echo "\e[$(BOLD)	webdev\e[$(REVERT_COLOR)   \e[$(BOLD)verbosity=DEBUG|INFO|WARNING|ERROR|CRITICAL\e[$(REVERT_COLOR)  (default: ERROR)"
	@echo "				starts the backend & frontend servers in the correct order and runs the "
	@echo "				frontend in debug mode         						"
	@echo "				(enabling livereload for the frontend, backend livereload is not 	"
	@echo "				implemented, when changing python code, the servers need to 		"
	@echo "				be restarted)                                                         	"
	@echo "\e[$(BOLD)	changelog\e[$(REVERT_COLOR)		show changes since last CHANGELOG.rst update                            "
	@echo "                                                                                                 "
	@echo "\e[$(YELLOW) Packaging\e[$(REVERT_COLOR)                                                         "
	@echo " ---------                                                                                       "
	@echo "\e[$(BOLD)	build_winpyinstaller\e[$(REVERT_COLOR)	Build the docker image used to build windows executables                                       "
	@echo "\e[$(BOLD)	build\e[$(REVERT_COLOR)			A target running all of the below                                       "
	@echo "\e[$(BOLD)	build_js\e[$(REVERT_COLOR)		Builds the frontend files                                               "
	@echo "\e[$(BOLD)	build_win, build_linux\e[$(REVERT_COLOR)	Builds the client executables for windows/linux                 "
	@echo "\e[$(BOLD)	build_package \e[$(REVERT_COLOR) 		Builds the python package					"
	@echo "\e[$(BOLD)	doc\e[$(REVERT_COLOR) 			builds the documentation                                                "

	@echo ""
	@echo "\e[$(YELLOW) First time setup\e[$(REVERT_COLOR)                                                  "
	@echo " --------------                                                                                  "
	@echo "\e[$(BOLD)	setup\e[$(REVERT_COLOR)		runs the \e[$(BOLD)dev-setup.sh\e[$(REVERT_COLOR) script to install dev dependencies, setup repo hooks, etc."
	@echo ""


setup:
	./dev-setup.sh

##################################################
# Targets useful for development (run tests,
# compile docs, run dev-server)
##################################################
lint-gitlab-ci: .gitlab-ci.yml
	./quality/validate-gitlab-ci-yml.sh

test:
	poetry run pytest --driver Chrome --show-capture=no tests

ui-test:
	poetry run pytest --driver Chrome --show-capture=no tests/browser

showdoc: doc
	cd ./doc/_build/ && xdg-open http://localhost:8331& cd ./doc/_build/ && python3 -m http.server --bind localhost 8331

bench:
	poetry run pytest  --driver Chrome --bench-save quality/benchmark-results.json tests/benchmarks

webdev:
	./deploy/devel/run.sh
	# ./quality/debug-frontend.sh $(verbosity)

changelog:
	poetry run gitchangelog

##################################################
# Targets for building the javascript files and
# for creating executables
##################################################
build:  doc build_win build_linux build_package

doc: 	FORCE
	$(TOX) -e docs

# A target for building the docker image (win-pyinstaller) used to build
# windows executables (in the build_win step)
build_winpyinstaller: deploy/docker/build_in_gitlab.sh deploy/docker/win-pyinstaller/Dockerfile deploy/docker/win-pyinstaller/entrypoint.sh
	cd ./deploy/docker && ./build_in_gitlab.sh local

build_js: FORCE
	deploy/update_version.sh
	cd ./vallex/server/frontend && yarn run build
	cd ./vallex/server/frontend && yarn run build-server
	sed -i -e "s/^\s*version\s*=.*/version=\"0.0\"/g" pyproject.toml

build_win: build_js
	deploy/update_version.sh
	docker run -v "$(PWD):/src/" registry.gitlab.com/verner/pyvallex/win-pyinstaller:latest "pip install setuptools==39.1.0 && pyinstaller --clean -y --dist ./dist/windows --workpath /tmp cli.spec"
	sed -i -e "s/^\s*version\s*=.*/version=\"0.0\"/g" pyproject.toml

build_linux: build_js
	deploy/update_version.sh
	poetry run pyinstaller --clean -y --dist ./dist/linux/ --workpath /tmp cli.spec
	sed -i -e "s/^\s*version\s*=.*/version=\"0.0\"/g" pyproject.toml

build_package: build_js
	deploy/update_version.sh
	poetry build
	sed -i -e "s/^\s*version\s*=.*/version=\"0.0\"/g" pyproject.toml

FORCE:
