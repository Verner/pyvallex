#!/bin/bash
# Script runs the diff-roundtrip.sh for each lexicon file from pyvallex.ini
# Use: ./quality.sh [ --diff | --gui ]

# Get all lexicons from .ini file
DIR=$(cat pyvallex.ini | grep "^lexicon-dirs\s*=.*" | sed "s/lexicon-dirs\s*=\s*//g")
if [ -z $DIR ]; then
  DIR=$(cat pyvallex.ini | grep "^vallex-repo\s*=.*" | sed "s/vallex-repo\s*=\s*//g")aktualni_data/data-txt/
fi

FILES=$(cat pyvallex.ini | sed -n -e"/^lexicons\s*=/,/^\s*$/p" | sed "s/lexicons\s*=\s*//g" | grep -v "\s*;" | tr "\n" " "  | sed "s/  */ /g")

for file in $FILES; do
    echo "FILE: $file"
    ./quality/diff-roundtrip.sh ${DIR}$file $1
done
