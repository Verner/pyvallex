#!/bin/bash
# This script is used for testing dev-setup.sh on various
# systems (e.g. Ubuntu 16.04, 18.04, Debian 10 etc) using docker
# Run it as follows:
#
#   $ ./quality/test-devsetup-docker.sh ubuntu:16.04
#

IMAGE=$1

YELLOW="1\;33m"
RED="0\;31m"
GREEN="0\;32m"
UNDERLINE="4m"
BOLD="1m"
REVERT_COLOR="0m"

# Create an overlay directory over the python repo so that
# docker can access it and modify it but the changes do not
# propagate outside
upper=`mktemp -t -d pyvallex-docker-XXX`
merged=`mktemp -t -d pyvallex-merged-XXX`
overlay=`mktemp -t -d pyvallex-overlay-XXX`
work=`mktemp -t -d pyvallex-work-XXX`
sudo mount -t overlay $overlay -o lowerdir=$PWD,upperdir=$upper,workdir=$work $merged/

echo -e "Please run \e[$BOLD apt update && apt install sudo\e[$REVERT_COLOR since a typical docker image donesn't have sudo"
# Run the image with the overlay mounted at /src/ inside
docker run -v "$merged/:/src/" -it $IMAGE

# Cleanup
echo "Cleaning up"
echo "sudo umount $merged"
sudo umount $merged
echo "sudo rm -rf $upper $merged $overlay $work"
sudo rm -rf $upper $merged $overlay $work

