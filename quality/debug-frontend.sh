#!/bin/bash
#
# This script is used to run the backend server & frontend development server
# in the correct order (i.e. backend runs on port 8080, frontend on 8081)
# and the web gui is accessible on http://localhost:8081/0/
#
# Also, the script makes sure to kill both servers on Ctrl-C


trap 'kill $PID_VALLEX && kill $PID_NODE' SIGTERM SIGINT

if [ $# -eq 0 ]
  then PYTHONPATH=. .venv/bin/python3 vallex/main.py web --no-browser --port 8080&
  else PYTHONPATH=. .venv/bin/python3 vallex/main.py --verbosity $1 web --no-browser --port 8080&
fi
PID_VALLEX=$!
pushd ./vallex/server/frontend
echo waiting 20 seconds for the backend to start
sleep 20
yarn serve&
PID_NODE=$!
popd
xdg-open http://localhost:8081/0/&

echo "**************VALLEX_PID $PID_VALLEX"
echo "**************NODE_PID $PID_NODE"

wait $PID_VALLEX
wait $PID_NODE
trap - SIGTERM SIGINT
wait $PID_VALLEX
wait $PID_NODE
EXIT_STATUS=$?
