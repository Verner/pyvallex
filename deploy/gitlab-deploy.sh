#!/bin/bash
set -e
SSH_ARGS="-o StrictHostKeyChecking=no"

if [ "$1z" == "betaz" ]; then
  echo "$DEPLOY_KEY_BETA_RSYNC" > /tmp/id_rsync
  echo "$DEPLOY_KEY_BETA_RELOAD" > /tmp/id_reload
  DIST_DIR=dist-server-beta
fi;

chmod 600 /tmp/id_rsync
chmod 600 /tmp/id_reload

REPO_PATH=`git rev-parse --show-toplevel`

mkdir /tmp/pyvallex
git archive $CI_COMMIT_SHA  | (cd /tmp/pyvallex && tar -x)

chmod a+x $REPO_PATH/deploy/update_version.sh
$REPO_PATH/deploy/update_version.sh /tmp/pyvallex/vallex/__version__

rsync -e "ssh $SSH_ARGS -i /tmp/id_rsync" --archive --delete vallex/server/frontend/$DIST_DIR/ vallex@$DEPLOY_HOST:static/
rsync -e "ssh $SSH_ARGS -i /tmp/id_rsync"  --archive --delete /tmp/pyvallex/ vallex@$DEPLOY_HOST:code/

ssh $SSH_ARGS -i /tmp/id_reload vallex@$DEPLOY_HOST
