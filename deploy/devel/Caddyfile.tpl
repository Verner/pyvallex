{
    admin off
}

:$UNIFIED_PORT {
    reverse_proxy /api/* localhost:$BACKEND_PORT
    reverse_proxy * localhost:$FRONTEND_PORT
}
