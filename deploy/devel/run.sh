#!/bin/bash
#
# This script is used to run the backend server & frontend development server
#
# Also, the script makes sure to kill both servers on Ctrl-C


# Waits until a server is listening on a given port
function wait_for_tcp { # port
    local retries port;
    retries=100;
    port=$1;
    while ! timeout --foreground 0.1s bash -c "echo >\"/dev/tcp/localhost/$port\"" >&/dev/null; do
        sleep 0.1;
        let retries=retries-1;
        if [ $retries -lt 0 ]; then
            exit 1;
        fi;
    done;
}

# Gets a free port where a server can listen
function get_unused_port {
    local port
    port=$1
    # Try connecting to the ports, increasing the port number in
    # each step; when we can't connect, we found a free port
    while bash -c "echo > \"/dev/tcp/localhost/$port\"" >&/dev/null; do
        let port=port+1;
    done
    echo $port;
}

# Runs the pyvallex backend
function run_backend {
    # Get an unused port (starting at $BACKEND_PORT) for the backend to listen on
    BACKEND_PORT=$(get_unused_port $BACKEND_PORT)

    # Try to run the frontend on a port one greater than the backend
    FRONTEND_PORT=$(get_unused_port $((BACKEND_PORT+1)))

    # Run the backend via gunicorn with hot reload (--reload) on http://localhost:$BACKEND_PORT
    # and gunicorn config under (deploy/devel/gunicorn_conf.py)
    # TODO: Get rid of the config file and specify the options as cmdline args?
    PYTHONPATH=. .venv/bin/gunicorn --reload --pid backend.pid --config=./deploy/devel/gunicorn_conf.py --access-logfile - --bind localhost:$BACKEND_PORT vallex.server.wsgi:app&
}

# Runs the javascript dev server which serves the frontend files
# and does js hot reload for us
# NOTE: needs to be run after `run_backend`, because it needs to know on what
# port the backend is running.
function run_frontend {
    FRONTEND_PORT=$(get_unused_port $FRONTEND_PORT)
    UNIFIED_PORT=$FRONTEND_PORT
    pushd ./vallex/server/frontend
    BACKEND_URL=http://localhost:$BACKEND_PORT VUE_APP_BASE_URL=http://localhost:$UNIFIED_PORT npx --node-arg --max-http-header-size=100000 vue-cli-service serve --port $FRONTEND_PORT&
    PID_NODE=$!
    popd
    echo $PID_NODE > frontend.pid;
}

# function run_caddy {
#     CADDY_FILE=$(mktemp -p /tmp -t $CADDY_FILE)
#     cat ./deploy/devel/Caddyfile.tpl | \
#         sed -es'/\$UNIFIED_PORT/'$UNIFIED_PORT'/g' | \
#         sed -es'/\$FRONTEND_PORT/'$FRONTEND_PORT'/g' | \
#         sed -es'/\$BACKEND_PORT/'$BACKEND_PORT'/g' > $CADDY_FILE;
#     caddy run --config $CADDY_FILE --adapter caddyfile --pidfile caddy.pid&
# }
#
# UNIFIED_PORT=$(get_unused_port 8111)
# CADDY_FILE=pyvallex-Caddyfile.XXX
# PID_CADDY=$(cat caddy.pid)

BACKEND_PORT=$(get_unused_port 8080)
FRONTEND_PORT=$(get_unused_port 8081)

PID_VALLEX=
PID_NODE=

# Cleanup when the user interrupts us
trap '(kill $(cat frontend.pid) || true) && (kill $(cat backend.pid) || true) && rm frontend.pid backend.pid' SIGTERM SIGINT


run_backend # needs to run before frontend !
run_frontend

# Wait until the servers are ready to accept connections
wait_for_tcp $BACKEND_PORT
wait_for_tcp $FRONTEND_PORT

# Open a browser window
xdg-open http://localhost:$FRONTEND_PORT&

PID_VALLEX=$(cat backend.pid)
PID_NODE=$(cat frontend.pid)


echo "************** BACKEND  ($PID_VALLEX) serving at http://localhost:$BACKEND_PORT"
echo "************** FRONTEND ($PID_NODE)   serving at http://localhost:$FRONTEND_PORT"

# Wait for the servers to exit
wait $PID_VALLEX
wait $PID_NODE

# Undo signal handling
trap - SIGTERM SIGINT

# Wait for the servers to really exit
wait $PID_VALLEX
wait $PID_NODE
EXIT_STATUS=$?
