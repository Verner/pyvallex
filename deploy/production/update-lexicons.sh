#!/bin/bash
cat << EOF
#############################################
# A script which can be run periodically    #
# (e.g. from cron or a systemd timer) to    #
# update the repo with the lexicons         #
#                                           #
# See *Manually updating the lexicon data*  #
# in the Maintenance section of             #
# doc/production.rst                        #
#############################################
EOF

# LOAD THE SERVER CONFIG INTO THE ENV
set -o allexport
. /etc/default/vallex   #= pyvallex/deploy/production/vallex.env
set +o allexport

# now $PY_VALLEX_CONFIG should be a link to deploy/production/server.ini;
# or at least some other file setting either lexicon-dirs or vallex-repo values

LEXICON_DIR=$(cat $PY_VALLEX_CONFIG | grep '^lexicon-dirs=' | sed -e 's/lexicon-dirs=//; s/#.*//; s/\s*$//')
if [ -z $LEXICON_DIR ] ; then
  vallexrepo=$(cat $PY_VALLEX_CONFIG | grep '^vallex-repo=' | sed -e 's/vallex-repo=//; s/#.*//; s/\s*$//')
  LEXICON_DIR=${vallexrepo}/aktualni_data/data-txt/
fi

# Logging will be done by systemd
# LOG_FILE=$VALLEX_HOME/logs/update-lexicons-`date +"%Y-%m-%d-%H-%M"`.log


echo "==== vallex-cli show config"
$VALLEX_HOME/bin/vallex-cli show config

# Update the repo; try svn first (and git only if it fails)
echo "==== cd $LEXICON_DIR"
cd $LEXICON_DIR

echo "==== Pull data from svn or gitlab"
if svn up --username $SVN_USER --password $SVN_PASS; then                          # &> $UPDATE_LOG
  # Load the changes into the database
  echo "==== vallex-cli web sync_db"
  $VALLEX_HOME/bin/vallex-cli web sync_db
  echo "==== svn commit --username $SVN_USER_FOR_COMMITTING --password [from config file] -m \"Automatic commit from the PyVallex web interface\""
  cd $LEXICON_DIR
  svn commit --username $SVN_USER_FOR_COMMITTING --password $SVN_USER_FOR_COMMITTING_PASS -m "Automatic commit from the PyVallex web interface"
else
    git fetch --all                 # &>> $UPDATE_LOG
    git reset --hard origin/master  # &>> $UPDATE_LOG
    # Load the changes into the database
    echo "==== vallex-cli web sync_db"
    $VALLEX_HOME/bin/vallex-cli web sync_db

    echo "==== git commit"
    cd $LEXICON_DIR
    git commit -a -m "Automatic commit from the web interface"
    echo "==== git push"
    git push origin
fi;

echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "YOU SHOULD NOW RUN"
echo chmod g+w $VALLEX_HOME/run/web-db
echo EVERYTHING WAS SUCCESSFUL, NOW EXIT 0
exit 0
