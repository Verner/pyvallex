#!/bin/bash
cat << EOF
#############################################
# A script to reload the vallex server when #
# the code changes.                         #
#                                           #
# See *Updating the pyvallex code* in the   #
# Maintenance section of doc/production.rst #
#                                           #
# Running this script REQUIRES SUDO RIGHTS. #
#############################################
EOF


# LOAD THE SERVER CONFIG INTO THE ENV
set -o allexport
. /etc/default/vallex   #= pyvallex/deploy/production/vallex.env
set +o allexport


#################################################
#         CONSOLE COLOR UTILITY FUNCTIONS       #
#################################################

YELLOW="1;33m"
BROWN="0;33m"
RED="0;31m"
GREEN="0;32m"
UNDERLINE="4m"
BOLD="1m"
REVERT_COLOR="0m"
STATUS_COL=10;

function colorize {
    echo -e "\e[$1 $2 \e[$REVERT_COLOR"
}


colorize $YELLOW "\nSTEP 1"
colorize $YELLOW "update the version file vallex/__version__"
cd $VALLEX_HOME/code
./deploy/update_version.sh

# the script also updates the pyproject.toml
# file with the version, which we don't want
git checkout pyproject.toml


colorize $YELLOW "\nSTEP 2"
colorize $YELLOW "compile the frontend javascript files if they have changed"
colorize $BROWN "cd $VALLEX_HOME/code/vallex/server/frontend"
cd $VALLEX_HOME/code/vallex/server/frontend
colorize $BROWN "yarn install"
yarn install        # installs possible new required packages
colorize $BROWN "yarn build-server"
yarn build-server   # compiles the javascript files


colorize $YELLOW "\nSTEP 3"
colorize $YELLOW "install possible new python requirements"
cd $VALLEX_HOME/code
. $VALLEX_HOME/code/.venv/bin/activate
pip install poetry                           # poetry is not a dev dependency, the next step will uninstall it
poetry install --no-root --no-dev -E server  # install dependencies needed for production (no devel dependencies)
deactivate


colorize $YELLOW "\nSTEP 4"
colorize $YELLOW "migrate the database, if there were any changes to the db schema"
$VALLEX_HOME/bin/vallex-cli web migrate_db
chmod g+w $VALLEX_HOME/run/web-db
chown :devs $VALLEX_HOME/run/web-db


colorize $YELLOW "\nSTEP 5"
colorize $YELLOW "restart the vallex service"

if systemctl is-active --quiet vallex.service && echo "vallex.service is running and will be reloaded"; then
  sudo systemctl reload vallex.service
else
  echo "vallex.service is not running and will be started"
  sudo systemctl start vallex.service
fi

sudo systemctl status vallex.service
