########################################################
# A powershell script to install vallex-tools          #
# https://verner.gitlab.io/pyvallex/                   #
#                                                      #
# Modify $install_path below to change the             #
# directory where everything will be installed         #
########################################################
$install_path = "C:\vallex-tools"


# Determine the correct python version to install
$ver="3.6.8"
$v_major_minor = (($ver -split '\.')[0,1] -join '')
if ((Get-WmiObject win32_operatingsystem | select osarchitecture).osarchitecture -like "64*") {
    $suffix="-amd64"
} else {
    $suffix=""
}

# Setup some variables
$installer_name="python-$ver$suffix.exe"
$installer_url="https://www.python.org/ftp/python/$ver/$installer_name"
$desktop_path = [environment]::GetFolderPath("Desktop")
$python_path = "$install_path\Python$v_major_minor"
$temp_path = "C:\vallex-tools\temp"

# Downloads the file $url into $target, if not
# already present
function DownloadFile ($target, $url) {
    $webclient = New-Object System.Net.WebClient
    $filename = ($url -split '/')[-1]

    # Make sure $target exists and is an absolute path
    # (as $webclient.DownloadFile doesn't use the CWD)
    mkdir $target -ea 0 >$null
    $target = (Resolve-Path $target).Path

    $filepath = (Join-Path $target $filename)
    if (Test-Path $filepath) {
        Write-Host $filepath "already downloaded"
        return $filepath
    }

    # Download and retry up to $retry_attempts times in case of network transient errors.
    Write-Host -fore Yellow "Downloading" $filename
    Write-Host "Source:" $url
    $retry_attempts = 3
    for ($i=0; $i -lt $retry_attempts; $i++) {
        try {
            $webclient.DownloadFile($url, $filepath)
            break
        }
        Catch [Exception]{
            Start-Sleep 1
        }
   }
   Write-Host "File saved at" $filepath
   return $filepath
}

# Downloads and installs Python + Pip
function InstallPython {
    DownloadFile $temp_path "$installer_url"
    DownloadFile $temp_path "https://bootstrap.pypa.io/get-pip.py"
    Start-Process "$temp_path\$installer_name" -Wait -ArgumentList /passive,PrependPath=1,TargetDir="$python_path"
    if (-not (Test-Path "$python_path\Scripts\pip.exe")) {
        & "$python_path\python.exe" $temp_path\get-pip.py
    }
    & "$python_path\python.exe" -m pip install -U pip setuptools wheel
}

# Installs vallex-tools & creates a shortcut on the desktop
function InstallVallexTools {
    & "$python_path\python.exe" -m pip install vallex-tools
    $WshShell = New-Object -comObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut("$desktop_path\Vallex Tools GUI.lnk")
    $Shortcut.TargetPath = "$python_path\Scripts\vallex-gui.exe"
    $Shortcut.IconLocation = "$python_path\Lib\site-packages\vallex\pyvallex.ico"
#     $Shortcut.Arguments = "argumentA ArgumentB"
    $Shortcut.WorkingDirectory = "$install_path"
    $Shortcut.Save()
}

InstallPython
InstallVallexTools
