#!/bin/bash
if [ "$1z" == "termz" ]; then
  DISCARD=gui
  MATCH=cterm
else
  MATCH=gui
  DISCARD=cterm
fi;

grep ^high | sed -e"s/$DISCARD[^ ]*//g" | sed -e's/.*def\s*//g' | sed -e's/^\([^ ]*\)/.LU-SRC-\1 {/g' | sed -e"s/$MATCH=\([^ ]*\)/font-weight: \1;/g" | sed -e's/[^ ]*fg=\([^ ]*\)/color: \1;/g' | sed -e's/[^ ]*bg=\([^ ]*\)/background-color: \1;/' | sed -e's/$/}/g'
