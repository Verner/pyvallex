""" This module contains classes for representing lexicon data.
"""


from .attribs import Attrib, Frame, FrameElement, Lemma, Valdiff, ValdiffElement, Examplerich, Reflexverb
from .collections import Lexicon, LexiconCollection
from .lexical_unit import Lexeme, LexicalUnit
from .utils import Comment, Ref, Text
from .constants import (
    LU_ID_REGEX,
    ATTRIBUTE_NAMES, ATTRIBUTE_NAME_REGEX,
    ATTR_REFLEXVERB_VALUES_REGEX,
    ATTR_VALUES_REGEX,
    ACTANT_FUNCTORS, ACTANTS_AND_CPHR,
    VALLEX_ASPECTS, VALLEX_ASPECTS_NUMBERED, PDTVALLEX_ASPECTS, VALLEX_ASPECTS_REGEX, ASPECTS_REGEX,
    FUNCTORS, FUNCTOR_RGX
)
