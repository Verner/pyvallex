import re

LU_ID_REGEX = re.compile(r'(?P<value>@?(blu-[avn]-|v-w)\w[\w+.-]*\w+|(Nom)?Vallex-no|PDT-Vallex-no|NA)')
"""Regex for matching the ids of lexical units,
including values "Vallex-no" and "PDT-Vallex-no" """

REFLEXIVE_LU_ID_REGEX = re.compile(r'-s[ei]\d?-')
"""Regex for recognizing ids of reflexive LUs."""

ATTRIBUTE_NAMES = [
    'alternations', 'class', 'control', 'conv',
    'derivedLUs', 'derivedFrom',
    'diat',
    'example', 'examplerich', 'example1', 'example2', 'example3', 'example4', 'example5', 'example6', 'example7',
    'exceptions', 'full',
    'instig', 'instig1', 'instig2', 'instig3', 'instig4', 'instig5', 'instig6', 'instig7',
    'limit',
    'full', 'lvc', 'lvc1', 'lvc2', 'lvc3', 'lvc4', 'lvc5', 'lvc6', 'lvc7', 'lvcN', 'lvcV',
    'map', 'map1', 'map2', 'map3', 'map4', 'map5', 'map6', 'map7',
    'multiple', 'negation', 'note', 'otherforms', 'pdt-vallex',
    'recipr', 'recipr1', 'recipr2', 'recipr3',
    'reciprevent', 'reciprevent1', 'reciprevent2', 'reciprevent3',
    'reciprtype',
    'reciprverb', 'reciprverb1', 'reciprverb2', 'reciprverb3',
    'reflex', 'reflex1', 'reflex2', 'reflex3',
    'reflexverb', 'reflexverb1', 'reflexverb2', 'reflexverb3', 'reflexverb4', 'reflexverb5',
    'semcategory', 'shortform', 'specval', 'status', 'synon', 'split', 'type', 'use', 'valdiff', 'valdiffNew', 'restriction',
    # and for Polish data also:
    'ref'
]
"""A list of all valid attribute names"""
ATTRIBUTE_NAME_REGEX = re.compile(r'-(?P<value>[\w-]+):[ \t]*')


FUNCTORS = [
    'ACMP', 'ACT', 'ADDR', 'AIM', 'APP', 'AUTH', 'BEN', 'CAUS',
    'COMPL', 'COND', 'CPHR', 'CPR', 'CRIT',
    'DIFF', 'DIR', 'DIR1', 'DIR2', 'DIR3', 'DPHR', 'EFF',
    'EXT', 'HER', 'ID', 'INTF', 'INTT', 'LOC', 'MANN', 'MAT',
    'MEANS', 'NORM', 'OBST', 'ORIG', 'PAT',
    'RCMP', 'REG', 'RESL', 'RESTR', 'RSTR',
    'SUBS', 'TFHL', 'TFRWH', 'THL', 'THO', 'TOWH', 'TPAR', 'TSIN', 'TTILL', 'TWHEN'
]
"""The list of valid functors"""
FUNCTOR_RGX = r'(' + r'|'.join(FUNCTORS) + r')'
"""Regex matching valid functors"""

ACTANT_FUNCTORS = ['ACT', 'ADDR', 'PAT', 'EFF', 'ORIG']
"Functors that are considered 'ACTANTS'"
ACTANTS_AND_CPHR = ACTANT_FUNCTORS + ['CPHR']

VALLEX_ASPECTS = ['pf', 'impf', 'iter', 'no-aspect', 'biasp']
VALLEX_ASPECTS_NUMBERED = [
    'pf', 'pf1', 'pf2', 'pf3',
    'impf', 'impf1', 'impf2',
    'iter', 'iter1', 'iter2',
    'no-aspect', 'no-aspect1', 'no-aspect2', 'no-aspect3',
    'biasp'
]
PDTVALLEX_ASPECTS = ['V', 'Nx', 'N', 'A', 'D']
VALLEX_ASPECTS_REGEX = re.compile(r'(?P<value>'+r'|'.join(VALLEX_ASPECTS_NUMBERED)+r'):\s')
ASPECTS_REGEX = re.compile(r'(?P<value>'+r'|'.join(VALLEX_ASPECTS_NUMBERED+PDTVALLEX_ASPECTS)+r'):\s')


CLASSES = [
    'appoint noun', 'appoint verb', 'cause motion', 'change', 'combining',
    'communication', 'contact', 'emission', 'exchange', 'expansion', 'extent',
    'intervention', 'location', 'mental action', 'modal verb', 'motion',
    'perception', 'phase of action', 'phase verb', 'providing', 'psych state',
    'psych verb', 'social interaction', 'transport'
]
ATTR_CLASS_RGX = r'(' + r'|'.join(CLASSES) + r')'

ATTR_REFLEXVERB_OLD_VALUES_RGX = r'derived-decaus[0-5]|derived-partobject[1-3]|derived-autocaus[1-4]|' + \
    r'derived-recipr[1-4]|derived-conv([1-9]|10)|derived-quasiconv2|' + \
    r'derived-decaus_(phenomenon|stimulus_phenomenon|experiencer)'
ATTR_REFLEXVERB_VALUES_RGX = r'tantum|derived-nonspecific|' + \
    r'derived-decaus_(theme|patient_PAT|patient_ADDR|phenomenon[12]|CPHR)|' + \
    r'derived-autocaus_(patient_PAT|patient_ADDR|recipient)|' + \
    r'derived-partobject_(PAT|EFF|DPHR)|' + \
    r'derived-recipr_(patient_(PAT_PAT|PAT_ADDR)|recipient_(ADDR_ADDR|PAT_PAT|ADDR_PAT))|' + \
    r'derived-conv_(bearer-of-action_location|donor_recipient_ACT_(ADDR_ORIG|PAT_PAT)|experiencer_stimulus|locatum_location[12]|speaker_recipient|subject_object_(PAT|ORIG)|substance_source)|' + \
    r'derived-quasiconv(_causator_object_(CAUS(_LOC)?|LOC(_DIR3)?|MEANS(_DIR3|_LOC)?|non-CAUS_LOC|OBST_(CAUS(_LOC)?|MEANS)))?|' + \
    r'derived-deaccus_(PAT|ADDR)'

ATTR_REFLEXVERB_OLD_VALUES_REGEX = re.compile(r'(?P<value>' + ATTR_REFLEXVERB_OLD_VALUES_RGX + r')\b')
ATTR_REFLEXVERB_VALUES_REGEX = re.compile(r'(?P<value>' + ATTR_REFLEXVERB_VALUES_RGX + r')\b')

ATTR_SEMCATEGORY_VALUES_RGX = r'((?<=semcategory: )(událost|událost / abstraktní výsledek děje|abstraktní výsledek děje|abstraktní výsledek děje / substance|substance|vlastnost|kontejner|jiné))'

ATTR_SPLIT_VALUES_RGX = r'((II|I): (Inference|Information|Path|Phenomenon|Stimulus))'
ATTR_CONV_VALUES_RGX = r'((II|I): (' + \
    r'Affliction-Patient|Bearer_of_action-Location|Hole-Affected_object|' + \
    r'Locatum-Location \((objectless|(destruction|formation)( person)?)\)|' + \
    r'Material-Product|Substance-Source|Theme-Path( \(objectless\))?|Theme_1-Theme_2' + \
    r'))'
ATTR_MULTIPLE_VALUES_RGX = r'((II|I): (' + \
    r'Area|Boundary|Goal|Impactee|Judgment|Location|Path|' + \
    r'Source|State( \(objectless\))?|Subject_matter( \(objectless\))?' + \
    r'))'

ATTR_VALUES_REGEX = re.compile(r'(?P<value>' +
                               ATTR_CLASS_RGX + r'|' +
                               ATTR_REFLEXVERB_VALUES_RGX + r'|' + ATTR_REFLEXVERB_OLD_VALUES_RGX + r'|' +
                               ATTR_SEMCATEGORY_VALUES_RGX + r'|' +
                               ATTR_SPLIT_VALUES_RGX + r'|' + ATTR_CONV_VALUES_RGX + r'|' + ATTR_MULTIPLE_VALUES_RGX +
                               r')\b')
