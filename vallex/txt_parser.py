""" Contains functions to parse lexicon data in txt format




    Parsing the lexicon data is done using the :func:`parse` function
    in two steps:

    1. In the first step, the input string (file) is converted
       into a stream of tokens (using the :class:`TokenStrea <.txt_tokenizer.TokenStream>`
       class).

    2. In the second step (in function :func:`parse_token_stream`), this stream
       is iterated over and whenever a :class:`LexemeStart <.txt_tokenizer.LexemeStart>`
       token is encountered, a single Lexeme is parsed (using the :func:`parse_lexeme`)
       and added to the collection of parsed lexemes.

    The second step is implemented via the :func:`parse_lexeme`, :func:`parse_lexical_unit`
    and :func:`parse_attr` functions. Since the attributes have a lot of variability
    in terms of their structure (some are plain values, some are lists, some contain links,
    ...) the function :func:`parse_attr` calls specialized functions which must be
    implemented separately for the different type of attributes. These functions take
    two arguments --- the head token (an instance of :class:`LexicalUnitAttr <.txt_tokenizer.LexicalUnitAttr>`)
    and a list of tokens comprising the body of the attribute. They each return an instance
    of :class:`Attrib <.data_structures.attribs.Attrib>` representing the parsed attribute.

    When implementing a parser for a new attribute, this parser needs to be decorated
    using the :meth:`AttributeFactory.register_parser` decorator, which is passed
    the list of attribute names as its argument, e.g.::

        @AttributeFactory.register_parser('examplerich', 'example', 'example1', ...)
        def parse_examplerich(head, body):
            ...

    Typically, one will also implement a subclass of :class:`Attrib <.data_structures.attribs.Attrib>`
    to represent a new attribute (and put it into the :mod:`.data_structures.attribs` package),
    however this is not strictly necessary.

    Currently the following parsers are implemented:

    - :func:`parse_note`
    - :func:`parse_examplerich`
    - :func:`parse_example`
    - :func:`parse_recipr`
    - :func:`parse_reflist` (lvc-type attributes)
    - :func:`synon`
    - :func:`reflexverb`

    Other attributes will raise a warning (``error.INVALID_ATTR``) during parsing and will
    be processed by a general parser which just makes the `_data` attr contain the source
    of the attribute body.
"""

import io
import re

from typing import Any, Callable, Dict, Iterable, List, Optional, Tuple, Union


from . import error
from . import txt_tokenizer as tok
from .error import data_error
from .data_structures import (
    Attrib, Comment, Frame, FrameElement, Lemma,
    Lexeme, LexicalUnit, Lexicon,
    Ref, Valdiff, ValdiffElement, Text, Examplerich, Reflexverb,
    ATTRIBUTE_NAMES
)
import logging
from .log import log


def parse(src: Union[str, io.IOBase], fname: Optional[str] = None) -> Lexicon:
    """
        Parses a txt-formatted lexicon.

        .. deprecated:: 0.5
           Use :func:`parse_token_stream` instead.
    """
    if isinstance(src, str):
        text = src
    else:
        text = src.read()  # type: ignore
    stream = tok.TokenStream(text, fname=getattr(src, 'name', ''))
    lexicon = parse_token_stream(stream)
    lexicon.path = fname
    return lexicon


def parse_token_stream(stream: tok.TokenStream, progress_cb: Optional[Callable[[int, int], None]] = None) -> Lexicon:
    """
        Parses the token stream `stream` into a collection of Lexemes.

        Note:
            1.  The stream represents a lexicon, in particular it might start
                with some comments which do not belong to any lexemes. This is
                dealt with by storing every comment that is encountered before
                the first lexeme in the preamble of the returned collection.
            2.  Comments which are on separate lines in between lexemes are
                always saved with the lexeme that immediately preceded them
                (the parser has no way to know if they should belong to the
                next lexeme or not); similarly comments between lexical units
                are automatically attached to the preceding lu
            3.  Parsing errors are reported via the :func:`data_error <.error.data_error>`
                function and ignored otherwise (i.e. the parser does its best
                to continue parsing).

        Todo:
            The error handling/reporting should be rethought.
    """
    preamble = [Comment(str(c)) for c in stream.cat_until([tok.LexemeStart, tok.EndOfSource]) if isinstance(c, tok.Comment)]

    # If present, strip the preamble we put in automatically when converting to txt
    if preamble and preamble[0].content.startswith('START ========== ') and preamble[0].content.endswith(' ========== START'):
        preamble = preamble[2:]
    ret = []
    count = 0
    while stream:
        stream.cat_until([tok.LexemeStart, tok.EndOfSource])
        if isinstance(stream.peek(), tok.EndOfSource):
            break
        ret.append(parse_lexeme(stream))
        count += 1
        if progress_cb and count % 20 == 0:
            progress_cb(stream.loc.pos, count)

    # If present, strip the postamble we put in automatically when converting to txt
    if ret and ret[-1].lexical_units:
        last_attr = list(ret[-1].lexical_units[-1].attribs.values())[-1]
        last_comments = last_attr.comments.get('all', [])
        if last_comments and last_comments[-1].content.startswith('END ========== ') and last_comments[-1].content.endswith(' ========== END'):
            last_comments.pop()
            last_comments.pop()

    lexicon = Lexicon(ret, preamble)
    lexicon._checksum = stream._checksum
    return lexicon


def parse_lexeme(stream: tok.TokenStream) -> Lexeme:
    """
        Parses one lexeme from the stream of tokens `stream`.

        Notes
        -----

        1. The function assumes that the first token of the stream
           is an :class:`LexemeStart <.txt_tokenizer.LexemeStart>` token.
        2. Tokens comprising the lexeme are consumed.
    """
    start_tok = stream.pop_left()

    # Get the Lexeme id for pdt-vallex like files -- e.g. ZPYTOVAT  (v-whsa_210)
    match = re.search('(?<=[(])(v-.+)(?=[)])', start_tok._src)
    if match:
        id = match.group(0)
        lex = Lexeme(start_tok._val[0:-len(id)-2].strip(), id)  # type: ignore
        lex._src_start = start_tok._loc.clone()  # type: ignore
    else:
        lex = Lexeme(start_tok._val)  # type: ignore
        lex._src_start = start_tok._loc.clone()  # type: ignore

    for token in stream:
        if isinstance(token, tok.LexicalUnitStart):
            stream.push_left(token)
            lex.lexical_units.append(parse_lexical_unit(stream, lex))
        elif isinstance(token, tok.Comment):
            lex.comments.append(Comment(str(token)))
        elif isinstance(token, tok.LexemeStart):
            stream.push_left(token)
            lex._src_end = token._loc.clone()   # type: ignore
            return lex
        elif isinstance(token, tok.EndOfSource):
            lex._src_end = token._loc.clone()  # type: ignore
            return lex
        elif isinstance(token, (tok.WhiteSpace, tok.Newline)):
            continue
        else:
            data_error("parser:txt", error.UNEXPECTED_TOKEN, token._loc, "Expecting", tok.LexemeStart.TOKEN_NAME+",", tok.LexicalUnitStart.TOKEN_NAME, "or", tok.EndOfSource.TOKEN_NAME, "found", str(token), 'instead')
            stream.cat_until([tok.LexicalUnitStart, tok.LexemeStart, tok.Comment, tok.EndOfSource])
    lex._src_end = stream.loc.clone()
    return lex


def parse_lexical_unit(stream: tok.TokenStream, parent: Lexeme) -> LexicalUnit:
    """
        Parses one lexical unit from the list of tokens `stream`.

        Example
        -------

        A typical lexical unit might look like::

            : id: blu-v-brát-vzít-1
            ~ impf: brát (si) pf: vzít (si) iter: brávat (si)
            + ACT(1;obl) PAT(4;obl) ORIG(od+2;opt) LOC(;typ) DIR1(;typ) RCMP(za+4;typ)
                -synon: impf: přijímat; získávat pf: přijmout; získat
                -example: impf: brát si od někoho mzdu / peníze za práci; vláda nebude mít odkud peníze brát; brát si snídani
                        pf: vzal si od něj peníze za práci; vláda nebude mít odkud peníze vzít; vzít si snídani
                -note: mohli loni brát na odměnách.COMPL měsíčně 26 až 40 tisíc korun
                    volné si
                -recipr: impf: ACT-ORIG %berou si od sebe peníze%
                        pf: ACT-ORIG %nikdy si od sebe nevzali peníze%

                ...

        The first line is the header of the lu and contains its id. It
        is represented in the token stream by a :class:`LexicalUnitStart <.txt_tokenizer.LexicalUnitStart>`
        token.

        The next two lines comprise the mandatory attributes which
        each lu must have: the lemma (the line started by ``~``) and the frame (the line
        started by ``+``). These are represented in the stream as
        a :class:`Lemma <.txt_tokenizer.Lemma>` token, followed by a list of tokens ended with a
        :class:`Newline <.txt_tokenizer.Newline>` token and, respectively, a
        :class:`Frame <.txt_tokenizer.Frame>` token, again followed by a list of tokens ending
        with a :class:`Newline <.txt_tokenizer.Newline>`.

        Finally, the remaining lines (until another :class:`LexicalUnitStart <.txt_tokenizer.LexicalUnitStart>`,
        :class:`LexemeStart <.txt_tokenizer.LexemeStart>` or :class:`EndOfSource <.txt_tokenizer.EndOfSource>`
        are parsed as attributes of the lexical unit, each attribute starting with a
        :class:`LexicalUnitAttr <.txt_tokenizer.LexicalUnitAttr>` token ending with the next such token
        or a token starting a new lu, lexeme or indicating the end of file.


        Notes
        -----

        1. The function assumes that the first token of the stream
           is an :class:`LexemeStart <.txt_tokenizer.LexemeStart>` token.
        2. Tokens comprising the lexeme are consumed.
        3. The parser does not assume that the frame and lemma attribute lines are in order
           or that there are no intervening lines (although in practice the ordering
           shown in the above example and without intervening lines can be expected).

    """

    start_tok = stream.pop_left()
    lu = LexicalUnit(start_tok._val, parent)  # type: ignore
    lu._src_start = start_tok._loc.clone()  # type: ignore

    # Set the Lexeme id
    match = re.search(r'blu-[anv]-\S+-[0-9+.]*', start_tok._val)
    if parent is not None and match and parent._id == parent._name:
        lastchar = match.group(0).rfind('-')
        parent._id = match.group(0)[:lastchar]

    # Parse the mandatory attributes
    mandatory_attrs = [tok.Lemma, tok.Frame]
    invalid_tokens = [tok.LexicalUnitStart, tok.LexemeStart, tok.EndOfSource, tok.LexicalUnitAttr]
    mandatory_attrs_preparsed = {}
    while mandatory_attrs:
        try:
            mattr_start = stream.loc.clone()
            lu.comments.extend([Comment(str(t)) for t in stream.cat_until(mandatory_attrs, invalid_token_types=invalid_tokens) if isinstance(t, tok.Comment)])
        except tok.InvalidToken:
            data_error("parser:txt", error.MISSING_ATTR, lu._src_start, "Missing mandatory attributes", [m.TOKEN_NAME for m in mandatory_attrs], "for lexical_unit", lu._id)
            stream.seek(mattr_start)
            break
        attr_head = stream.pop_left()
        val = stream.cat_until(tok.Newline)
        attr_comments = [Comment(str(t)) for t in val if isinstance(t, tok.Comment)]
        val = [t for t in val if not isinstance(t, tok.Comment)]
        mandatory_attrs_preparsed[attr_head._val] = val, attr_comments
        mandatory_attrs.remove(type(attr_head))

        if '+i' in attr_head._src:
            lu.attribs['idiom'] = Attrib('idiom', True)

    # Parse the lemma
    if '~' in mandatory_attrs_preparsed:
        val, comments = mandatory_attrs_preparsed['~']
        lu.lemma = Lemma(data=parse_keyval(val, 'all', stringify_vals=True)[0])
        # when the LU has multiple aspects but only one of them is non-iterative,
        # the attribute values are treated as if it was a unit with that single aspect
        non_iter_aspects = lu._compute_has_single_aspect(return_noniter_aspects=True)
        default_aspect = non_iter_aspects[0] if lu.has_single_noniter_aspect else 'all'
        lu.lemma.comments = {'all': comments}

    # Parse the frame
    if '+' in mandatory_attrs_preparsed:
        val, comments = mandatory_attrs_preparsed['+']
        lu.frame = parse_frame(val)
        lu.frame.comments = {'all': comments}

    # Parse the optional attributes
    stop_set = [tok.LexicalUnitStart, tok.LexemeStart, tok.EndOfSource, tok.LexicalUnitAttr]
    while stream:
        stream.cat_until(stop_set)
        if not isinstance(stream.peek(), tok.LexicalUnitAttr):
            lu._src_end = stream.loc.clone()
            return lu
        attr = parse_attr(stream, default_aspect=default_aspect)
        if attr.name in lu.attribs:
            if attr.name != 'note':
                data_error("parser:txt", error.DUPLICATE_ATTR, lu._src_start, "Attribute", attr.name, "already defined here:", str(lu.attribs[attr.name]._src_start), "for lexical_unit", lu._id)
            attr.duplicate, attr.name = attr.name, _generate_name(attr.name, lu.attribs.keys())
            if attr.name == '':
                data_error("parser:txt", error.DUPLICATE_ATTR, lu._src_start, "Unable to find unique name for Attribute", attr.duplicate, "for lexical_unit", lu._id)
        lu.attribs[attr.name] = attr
    lu._src_end = stream.loc.clone()
    return lu


def _generate_name(base: str, forbidden: Iterable[str]) -> str:
    """
        Tries to generate a new attribute name for a duplicate attribute
        by iteratively appending a character to `base` and testing if its not
        contained in `forbidden`.
    """
    for uniq in '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZ':
        if base+'-'+uniq not in forbidden:
            return base+uniq
    return ''


def parse_keyval(tokens: List[tok.Token], default_aspect: str, stringify_vals=False) -> Tuple[Dict[str, Union[str, List[tok.Token]]], Dict[str, List[Comment]]]:
    """
        Parses a list of tokens which consists of (aspect, value) pairs into a dictionary.
        The value will typically be a list of tokens. For example::

            Aspect(impf) Scope(ACT) Word(chodit) Newline() Aspect(pf) Scope(ACT) Word(dojít) Newline() 
            Scope(PAT) Word(dojito) Comment(comment) Newline() Word(nedojito) Newline() Word(přejito) Comment(comment2) 
            Newline() Aspect(iter) Word(hvízdat) Word(si)

        corresponding to source::

            impf: ACT: chodit
            pf: ACT: dojít
                PAT: dojito  # comment
                     nedojito
                     přejito  # comment2
            iter: hvízdat si

        will be converted into tuple of dictionaries::

            {'impf: ACT': 'chodit', 'pf: ACT': 'dojít', 'pf: PAT': ['dojito', 'nedojito', 'přejito'], 'iter': 'hvízdat si'},
            {'impf: ACT': Comment(None), 'pf: ACT': Comment(None), 
             'pf: PAT': [Comment('comment'), Comment(None), Comment('comment2')], 'iter': Comment(None)}

        Args:
            tokens: the list of tokens to parse
            stringify_vals: whether to convert tokens into strings when returning.

        Returns:
            The resulting pairs as a dictionary keyed by the stringified value of the first element
            and the values being either directly the second elements (a list of tokens) if stringify_vals is False or a
            concatenation of the stripped stringified values of the second element.
    """
    ret: Dict[str, Union[str, List[tok.Token]]] = {}
    comments = {}

    # This method does bulk of the work by splitting
    # such that the even elements are keys and the odd elements are values
    # (except perhaps the first...)
    parts = tok.split(tokens, [tok.FunctorCombination, tok.Aspect])

    # When the first element is not an empty list, this indicates that the list started with a value
    # without a key (there are two possible situations:
    # 1) the value applies to all keys 'all' aspect saved or
    # 2) there exists only a single aspect, see how default_aspect value is prepared).
    if parts[0]:
        if stringify_vals:
            val = ''.join([str(v) for v in parts[0] if not isinstance(v, tok.Comment) and str(v)]).strip()   # type: ignore
            ret[default_aspect] = val
        else:
            ret[default_aspect] = [v for v in parts[0] if not isinstance(v, tok.Comment)]   # type: ignore
        comments[default_aspect] = [Comment(v._val) for v in parts[0] if isinstance(v, tok.Comment)]   # type: ignore

    # Discard the first element (either it was empty or we took care of it above)
    parts = parts[1:]

    # Iterate over the (key, value) pairs and store them in the ret dictionary
    aspect = ''
    for i in range(int(len(parts)/2)):
        key = parts[2*i]

        if isinstance(key, tok.Aspect) and not parts[2*i+1]:
            aspect = str(key) + ': '

        if aspect[:-2] != str(key):
            if stringify_vals:
                val = ''.join([str(v) for v in parts[2*i+1] if not isinstance(v, tok.Comment) and str(v)]).strip()   # type: ignore
                ret[aspect + str(key)] = val
            else:
                ret[aspect + str(key)] = [v for v in parts[2*i+1] if not isinstance(v, tok.Comment)]   # type: ignore
            comments[aspect + str(key)] = None  # type: ignore

            # add comments in shape, which enables to correctly align lines with its comments afterwards
            lines_count = 0
            for v in parts[2*i+1]:  # type: ignore
                if isinstance(v, tok.Semicolon):
                    lines_count += 1
                elif isinstance(v, tok.Comment):
                    if comments.get(aspect + str(key), None):
                        for _ in range(lines_count-1):
                            comments[aspect + str(key)].append(None)  # type: ignore
                        comments[aspect + str(key)].append(Comment(v._val))
                    else:
                        comments[aspect + str(key)] = None if (lines_count-1) <= 0 else [Comment('') for _ in range(lines_count-1)]  # type: ignore
                        comments[aspect + str(key)] = [Comment(v._val)]
                    lines_count = 0
    return ret, comments


class AttributeFactory:
    """
        Used by the :func:`parse_attr` function to parse a head_token and a list of body tokens
        into a lexical unit attribute (an instance of :class:`Attrib <.data_structures.attribs.Attrib>`).
        This is done by the :meth:`AttributeFactory.parse_attribute`
        class (static) method; This method delegates the actual parsing to parser methods registered using
        the :meth:`AttributeFactory.register_parser` class (static) method.
    """

    REGISTERED_ATTRIBUTES: Dict[str, Callable[[tok.Token, List[tok.Token]], Attrib]] = {}
    "Contains a mapping from attribute names to functions used to parse them"

    @classmethod
    def parse_attribute(cls, head_token: tok.Token, body_token_list: List[tok.Token], default_aspect='all') -> Attrib:
        """
            Creates a lexical unit attribute (an instance of :class:`Attrib <vallex.data_structures.attribs.Attrib>`)
            from a `head_token` and the body `body_token_list` (a list of tokens).
        """
        attr_name = head_token._val
        if attr_name in cls.REGISTERED_ATTRIBUTES:
            return cls.REGISTERED_ATTRIBUTES[attr_name](head_token, body_token_list, default_aspect)  # type: ignore

        if attr_name not in ATTRIBUTE_NAMES:
            data_error("parser:txt", error.INVALID_ATTR, head_token._loc, "Invalid attribute type", attr_name)
        else:
            data_error("parser:txt", error.UNHANDLED_ATTR, head_token._loc, "Unhandled attribute type", attr_name)
        attr = Attrib(attr_name)   # type: ignore
        attr._data = ''.join([t._src for t in body_token_list if not isinstance(t, tok.Comment)]).strip()
        attr.comments = {'all': [Comment(str(t)) for t in body_token_list if isinstance(t, tok.Comment)]}
        return attr

    @classmethod
    def register_parser(cls, *args):
        """
            A decorator which registers a parser method for attributes whose name is in the `*args` list.
        """
        def decorator(parser_method):
            for arg in args:
                cls.REGISTERED_ATTRIBUTES[arg] = parser_method
            return parser_method
        return decorator


def parse_attr(stream: tok.TokenStream, default_aspect='all') -> Attrib:
    """
        Parses a single attribute of a lexical unit from the token stream `stream`.

        The bulk of the work is done by calling :meth:`AttributeFactory.parse_attribute` which in turn
        calls parsers for the various attributes.

        Returns:
            The parsed attribute.
    """

    # Get the attribute name
    head = next(stream)
    src_start = head._loc.clone()

    # Get the body of the attribute
    body = stream.cat_until([tok.LexicalUnitStart, tok.LexemeStart, tok.EndOfSource, tok.LexicalUnitAttr])
    src_end = stream.loc.clone()

    attr = AttributeFactory.parse_attribute(head, body, default_aspect=default_aspect)
    attr._src_start = src_start
    attr._src_end = src_end
    return attr


def process_text(tok_list: List[tok.Token]) -> Text:
    """
        Converts a list of tokens into a Text object.

        Replaces ASCII quotes with corresponding open/close quotes.
    """
    ret = Text()

    open_quotes: List[tok.Quote] = []
    for token in tok_list:
        if isinstance(token, tok.Quote):
            if not open_quotes:
                token.set_open(True)
                open_quotes.append(token)
            else:
                if open_quotes[-1]._val == token._val:
                    token.set_open(False)
                    open_quotes.pop()
                else:
                    token.set_open(True)
                    open_quotes.append(token)

        if isinstance(token, tok.Comment):
            ret.comments.append(Comment(str(token)))
        else:
            ret += str(token)

    if open_quotes:
        data_error('parser:txt', error.UNBALANCED_QUOTES, open_quotes[0]._loc, 'Unbalanced quote starting')

    ret.content = ret.content.strip()

    return ret


def parse_frame(body) -> Frame:
    """
        Parses the body of the frame attribute.

        Example:
            A typical frame attribute might look like::

                + ACT(1;obl) PAT(4;obl) ORIG(od+2;opt) LOC(;typ) DIR1(;typ) RCMP(za+4;typ)
                + EMPTY PAT(||;obl)

            The token corresponding to the '+' is not a part of the body, the rest of the line
            would be tokenized by the tokenizer as a list of :class:`.txt_tokenizer.ValencySlot`
            tokens::

                [ValencySlot('ACT', '1;obl'), ValencySlot('PAT', '4;obl'), ValencySlot('ORIG', 'od+2;obl'), ...]
                [ValencySlot('EMPTY', '')], ValencySlot('PAT', '||;obl')]

            whose :attr:`_val <.txt_tokenizer.Token._val>` attributes will be the
            functor name (e.g. ``'ACT'``) and whose :attr:`_args <.txt_tokenizer.Token._args>`
            attribute will contain the string inside the parentheses (e.g. ``'1;obl'``).

            TODO: it would be better to represent the Valency Slot as a 3-tuple
            consisting of functor, forms and obligatoriness
    """
    elts = []
    for token in body:
        if str(token).strip().upper() == 'TODO':
            elts.append(FrameElement(str(token).strip(), [], None))
        elif token._val == 'EMPTY':
            elts.append(FrameElement(token._val, [], None))
        elif isinstance(token, tok.ValencySlot):
            if ';' in token._args:
                forms, oblig = token._args.split(';', maxsplit=2)
                forms = forms.split(',')  # type: ignore
                if oblig not in ['obl', 'opt', 'typ']:
                    data_error('parser:txt', error.INVALID_ATTR_VALUE, token._loc, 'Invalid obligatory type', oblig, 'while parsing frame')
            else:
                forms, oblig = token._args.split(','), None  # type: ignore
                data_error('parser:txt', error.INVALID_ATTR_VALUE, token._loc, 'Bracket after functor does not contain a semicolon:', token._args, 'while parsing frame')
            if elts and elts[-1].functor.endswith('|'):  # grouped Functors (e.g. BEN(;obl)|MANN(;obl)|MEANS(;obl)) parsed into a single ValencySlot
                if list(filter(None, forms)):
                    elts[-1].forms.append(forms)
                if oblig and oblig != elts[-1].oblig:
                    if elts[-1].oblig:
                        elts[-1].oblig += (', ' + oblig)
                        data_error('parser:txt', error.INVALID_ATTR_VALUE, token._loc, 'Alternative functors with differing obligatoriness', elts[-1].oblig, 'while parsing frame')
                    else:
                        elts[-1].oblig = oblig
                elts[-1].functor += token._val
            else:
                elts.append(FrameElement(token._val, forms, oblig))  # type: ignore
        elif isinstance(token, tok.Pipe):
            elts[-1].functor += "|"
        elif isinstance(token, tok.WhiteSpace):
            pass
        else:
            data_error('parser:txt', error.UNEXPECTED_TOKEN, token._loc, 'Unexpected token', token._val, 'of type', token.TOKEN_NAME, 'while parsing frame')
    return Frame(data=elts)


@AttributeFactory.register_parser('valdiff', 'valdiffNew')
def parse_valdiff(head, body, default_aspect='all') -> Valdiff:
    """
        Parses the body of the valdiff attribute.

        Example:
            A valdiff attribute might look like (not a real-world example)::

                -valdiff: =ACT(=:1,add+4,2) =PAT(=:2;+:3,po+4;-:1,po+3) -ADDR(-:4,za+5,2) +LOC(+:4,za+5,2)

            The body would be tokenized by the tokenizer as a list of :class:`.txt_tokenizer.ValdiffSlot`
            tokens::

                [ValdiffSlot('=ACT', '=:1,add+4,2'), ValdiffSlot('=PAT', '=:2;+:3,po+4;-:1,po+3'),  ...]

            (or :class:`.txt_tokenizer.FunctorTupleList` in case the functor does not
            have any forms or oblig, i.e. stands alone without parentheses) whose
            :attr:`_val <.txt_tokenizer.Token._val>` attributes will be the
            functor name (e.g. ``'ACT'``) preceded by an action symbol (``=``, ``+``, or ``-``) and whose :attr:`_args <.txt_tokenizer.Token._args>`
            attribute will contain the string inside the parentheses (e.g. ``'=:1,add+4,2;->'``). This string
            is a collection of ``;``-separated substrings with the items
            of the form ``act:forms`` where ``act`` is an action symbol (``=``, ``+``, or ``-``) and ``forms`` is a ``,``-separated
            list of forms.
    """
    elts = []
    tokens = [t for t in body if not isinstance(t, tok.Comment)]
    comments = {'all': [Comment(str(t)) for t in body if isinstance(t, tok.Comment)]}

    for token in tokens:
        if isinstance(token, tok.FunctorTupleList):
            if ' ' in token._val:
                data_error('parser:txt', error.INVALID_ATTR_VALUE, token._loc, 'Invalid functor value', token._val, 'while parsing valdiff')
            else:
                if token._val[0] == '+':
                    elts.append(ValdiffElement(token._val[1:], spec='+'))
                elif token._val[0] == '-':
                    elts.append(ValdiffElement(token._val[1:], spec='-'))
                elif token._val[0] == '=':
                    elts.append(ValdiffElement(token._val[1:], spec='='))
                else:
                    data_error('parser:txt', error.INVALID_ATTR_VALUE, token._loc, 'Expected action (=,+, or -) got', token._val, 'instead while parsing valdiff')
        elif isinstance(token, tok.ValdiffSlot):
            if not token._args:
                elts.append(ValdiffElement(token._val[1:], spec=token._val[0]))
            else:
                formlist = token._args.split(';')
                forms: Dict[str, List[str]] = {
                    '=': [],
                    '+': [],
                    '-': []
                }
                typical: List[Tuple[str, List[str]]] = []
                for group in formlist:
                    try:
                        act, data = group.split(':')
                    except Exception as ex:
                        log("txt_parser", logging.ERROR, str(ex))
                    if act not in ['=', '+', '-', '>']:
                        data_error('parser:txt', error.INVALID_ATTR_VALUE, token._loc, 'Invalid action type', act, 'while parsing valdiff')
                    elif act == '>':
                        typical = [(ch.split('->')[0], ch.split('->')[1].split(',')) for ch in data.split('@')]
                    else:
                        forms[act] = data.split(',')
                elts.append(ValdiffElement(token._val[1:], spec=token._val[0],
                                           forms_typ=typical,
                                           forms_eq=forms['='],
                                           forms_add=forms['+'],
                                           forms_del=forms['-']))
        elif isinstance(token, tok.WhiteSpace) or isinstance(token, tok.Newline):
            pass
        else:
            data_error('parser:txt', error.UNEXPECTED_TOKEN, token._loc, 'Unexpected token', token._val, 'of type', token.TOKEN_NAME, 'while parsing valdiff')
    attr = Valdiff(data=elts, attr_name=str(head))
    attr.comments = comments
    return attr


@AttributeFactory.register_parser('class', 'note', 'semcategory', 'restriction',
                                  'status', 'otherforms', 'specval', 'use', 'type', 'shortform', 'negation',
                                  # TODO: handle these more graciously:
                                  'control', 'diat', 'split', 'conv', 'multiple',
                                  'map', 'map1', 'map2', 'map3', 'map4', 'map5', 'map6', 'map7', 'map8', 'map9',
                                  'instig', 'instig1', 'instig2', 'instig3', 'instig4', 'instig5', 'instig6', 'instig7', 'instig8', 'instig9',
                                  'reciprevent', 'reciprevent1', 'reciprevent2', 'reciprevent3',
                                  'reciprverb', 'reciprverb1', 'reciprverb2', 'reciprverb3',
                                  'reflex', 'reflex1', 'reflex2', 'reflex3',
                                  )
def parse_note(head, body, default_aspect='all') -> Attrib:
    """
        Parses the note attribute

        Returns:
            The parsed attribute whose :attr:`_data <.data_structures.attribs.Attrib._data>` attribute contains
            the content of the note as a simple string.
    """
    attr = Attrib(str(head))
    # the next line was replaced with ugly hack below to make a test work because somehow this comment was glued to the attribute value...
    # attr._data = [''.join(t._src for t in body]).strip()]
    attr._data = [''.join([re.sub(r'#( END .* END)?$', '', t._src) for t in body]).strip()]
    return attr


@AttributeFactory.register_parser('example', 'example1', 'example2', 'example3', 'example4', 'example5', 'example6', 'example7')
def parse_example(head, body, default_aspect='all') -> Attrib:
    """
        Parses an example-type attribute.

        The body of the example attribute consists of a list of exemplifying sentences
        separated by semicolons. Some sentences may be relevant for the whole lexical
        unit, while others may exemplify just a specific aspect. This latter case is
        indicated by prefixing the examples with the name of the aspect followed by a colon.

        For example the following source::

            -example2: impf: Od cizinců mají zakázáno brát úplatky.;
                                Podle dřívějších informací ho policisté viní z toho, že si jeho společnost brala úvěry od společností sídlících na britském ostrově Man a v Nizozemsku.;
                          pf: Zkraje 80. let prý vzal úplatek od Japonců.;
                              Ještě to neřeším, ale samozřejmě si budeme muset vzít překlenovací úvěr.

        has a list of four example sentences with the first two exemplifying the ``impf`` aspect
        while the latter two exemplify the ``pf`` aspect.

        Returns:
            The parsed attribute whose :attr:`_data <.data_structures.attribs.Attrib._data>` is a dictionary
            keyed by aspect names which contains a list of sentences (instances of the
            :class:`Text <.data_structures.utils.Text>` class) exemplifying each aspect.
    """
    # if attrib has a non 'all' aspect, it is the single valid (non iter) aspect
    attr = Attrib(str(head))
    attr._data = {}

    parsed_data, attr.comments = parse_keyval(body, default_aspect=default_aspect)
    data = {k: tok.split(v, tok.Semicolon, include_separators=False) for k, v in parsed_data.items()}  # type: ignore
    for key, val in data.items():
        attr._data[key] = list(filter(None, [process_text(ex) for ex in val]))  # type: ignore

    return attr


@AttributeFactory.register_parser('examplerich')
def parse_examplerich(head, body, default_aspect='all') -> Examplerich:
    """
        Parses an examplerich attribute.

        The body of the examplerich attribute consists of a list of exemplifying sentences
        separated by semicolons. Some sentences may be relevant for the whole lexical
        unit, while others may exemplify just a specific aspect. This latter case is
        indicated by prefixing the examples with the name of the aspect followed by a colon.
        The examples are further divided by functor combinations.

        For example::

            -examplerich: impf: ACT+ADDR+PAT+MEANS: Ve stavebních kruzích Palubní deník zaregistroval jeho.ACT přesvědčování stavařů.ADDR k větší vstřícnosti.PAT argumentem.MEANS, že Hudeček je sice jednička, ale;
                                ACT+ADDR+PAT: ... dalo usuzovat z nabídek výrobců tiskových zařízení a jejich.ACT přesvědčování zákazníků.ADDR, že barevný tisk potřebují.PAT.;
                                ACT+PAT: Podlehla jsem manželovu.ACT přesvědčování o kvalitách.PAT cestování na kole;
                          pf: ACT+ADDR: Po nepřesvědčení městského zastupitelstva.ADDR členem.ACT a odborníkem.ACT, jemuž byl vlastní námět svěřen k propracování, pokud se nákladů stavebních týká, dochází k zajištění ploch od p. Leop. Skály.;
                              ADDR+PAT: Podle Jiřího Kastnera nebývá s přesvědčením českých zaměstnanců.ADDR k pohybu.PAT zvláštní problém: „Češi vždy patřili, a věřím, že patřit budou, mezi sportovní národy.;
                                        Podpora prodeje vyžaduje cílené předávání všech informací, které jsou nezbytné k přesvědčení zákazníka.ADDR o tom, že by si měl produkt koupit.PAT.;

        has a list of six example sentences with the first three exemplifying the ``impf`` aspect
        while the latter three exemplify the ``pf`` aspect.

        Returns:
            The parsed attribute whose :attr:`_data <.data_structures.attribs.Attrib._data>` is a dictionary
            keyed by aspect names which contains dictionaries keyed by functor combination
            each of which contains a list of sentences (instances of the
            :class:`Text <.data_structures.utils.Text>` class).
    """
    # if attrib has a non 'all' aspect, it is the single valid (non iter) aspect
    attr = Examplerich(str(head))
    attr._data = {}

    parsed_data, comments = parse_keyval(body, default_aspect=default_aspect)
    data = {k: tok.split(v, tok.Semicolon, include_separators=False) for k, v in parsed_data.items()}  # type: ignore

    def add_to_dict_or_submerge(d: dict, key1: str, key2: str, value: Any):
        """Support function: add as dict value if key present or submerge into the dict"""
        if key1 in d:
            d[key1][key2] = value
        else:
            d[key1] = {key2: value}
        return d

    for key, val in data.items():
        val = list(filter(None, [process_text(ex) for ex in val]))  # type: ignore
        comment = comments.get(key, None)
        if val or comment:
            try:
                aspect, functor = key.split(': ')
                functor = 'all' if default_aspect.strip() == aspect.strip() else functor  # functor should not be the same as the default aspect
                attr._data = add_to_dict_or_submerge(attr._data, aspect, functor, val)
                attr.comments = add_to_dict_or_submerge(attr.comments, aspect, functor, comment)
            except:
                functor = 'all' if default_aspect.strip() == key.strip() else key  # functor should not be the same as the default aspect
                attr._data = add_to_dict_or_submerge(attr._data, default_aspect, functor, val)
                attr.comments = add_to_dict_or_submerge(attr.comments, default_aspect, functor, comment)
    return attr


@AttributeFactory.register_parser('recipr', 'recipr1', 'recipr2', 'recipr3')
def parse_recipr(head, body, default_aspect='all') -> Attrib:
    """
        Parses the recipr attribute.

        The body of the recipr attribute resembles the example attribute, except that
        the exemplifying sentences are relevant not just to a given aspect, but also
        for a given functor combination (i.e. there are two levels)::

            -recipr: impf: ACT-ADDR %děti si berou hračky navzájem%
                     pf: ACT-ADDR %vzali si navzájem peníze%

        Also, the examples are enclosed in ``%`` characters instead of being separated
        by the semicolon.

        Returns:
            The parsed attribute whose :attr:`_data <.data_structures.attribs.Attrib._data>` is a dictionary
            keyed by aspect names which contains a further dictionary keyed by functor names which
            finally contains a list of exemplifying sentences (instances of the
            :class:`Text <.data_structures.utils.Text>` class).

        TODO:
            The implementation should be revisited by someone more familiar with the
            lexicon format to ensure its correctness and coherence.
    """
    attr = Attrib(str(head))
    attr._data = {}

    parsed_data, attr.comments = parse_keyval(body, default_aspect=default_aspect)

    data = {aspect: tok.split(per_aspect_data, tok.FunctorTupleList) for aspect, per_aspect_data in parsed_data.items()}  # type: ignore
    for aspect, per_aspect_data in data.items():
        per_aspect_final = {}

        # When the first element is not an empty list, this indicates that we start with an unqualified example
        if per_aspect_data[0]:
            all_ = list(filter(None, [process_text(ex) for ex in tok.split(per_aspect_data[0], tok.Percent, include_separators=False)]))  # type: ignore
            if all_:  # FIXME: This should not happen, we should probably look at parsed_data.items() and do some filtering
                per_aspect_final[default_aspect] = all_

        per_aspect_data = per_aspect_data[1:]
        for i in range(int(len(per_aspect_data)/2)):
            tgt_list = per_aspect_data[2*i]
            examples = list(filter(None, [process_text(ex) for ex in tok.split(per_aspect_data[2*i+1], tok.Percent, include_separators=False)]))  # type: ignore
            per_aspect_final[str(tgt_list)] = examples

        attr._data[aspect] = per_aspect_final

    return attr


@AttributeFactory.register_parser('full', 'lvc', 'lvcN', 'lvcV', 'derivedLUs', 'derivedFrom', 'lvc1', 'lvc2', 'lvc3', 'lvc4', 'lvc5', 'lvc6', 'lvc7',
                                  'ref'         # for Polish data
                                  )
def parse_reflist(head, body, default_aspect='all') -> Attrib:
    """
        Parses the lvc-type attributes.

        The body of a lvc-type attribute consists of a space (or comma) separated list of lexical unit ids, optionally
        followed by ``|`` and a list of space separated lemmata , e.g.::

             -lvc: blu-n-chyba-1 blu-n-gesto-1 blu-n-kompromis-1 ...  blu-n-závěr-2 | narážka rozdíl

        Returns:
            The parsed attribute whose :attr:`_data <.data_structures.attribs.Attrib._data>` is a dictionary
            which contains, under the ``ids`` key, the list of lu references (instances of the
            :class:`Ref <.data_structures.utils.Ref>` class) and, under the ``lemmas`` key, the list
            of lemmas (pure strings).
    """
    attr = Attrib(str(head))
    attr._data = {'ids': [], 'lemmas': []}

    # Extract comments
    attr.comments['all'] = [Comment(t._val) for t in body if isinstance(t, tok.Comment)]

    parts = tok.split(body, tok.Pipe, include_separators=False)

    # Deal with ids first

    # Filter out any non-identifiers
    # FIXME: Need to check for unexpected tokens and report an error!
    part_1 = filter(lambda t: isinstance(t, tok.Identifier), parts[0])  # type: ignore

    # Convert them into References
    attr._data['ids'] = [Ref(str(id), 'LexicalUnit') for id in part_1]

    # Now we deal with lemmas
    if len(parts) >= 2:
        part_2 = filter(lambda t: isinstance(t, tok.Word), parts[1])  # type: ignore
        attr._data['lemmas'] = [str(lem) for lem in part_2]

    return attr


@AttributeFactory.register_parser('pdt-vallex')
def parse_pdt_vallex(head, body, default_aspect='all') -> Attrib:
    """
        Parses links to PDT-Vallex.

        For each aspectual counterpart covered by the given LU, the pdt-vallex attribute contains a space (or comma) separated list of PDT-Vallex ids corresponding to the given LU, e.g.:
            -pdt-vallex: PDT-Vallex-no
            -pdt-vallex: v-w322f1 v-w322f2
            -pdt-vallex: impf: v-w548f1  pf: PDT-Vallex-no

        It may also contain only the prefix that is common to all LUs in a given PDT-Vallex lexeme, e.g.:
            -pdt-vallex: v-w71

        Returns:
            The parsed attribute whose :attr:`_data <.data_structures.attribs.Attrib._data>` is a dictionary
            keyed by aspect names which contains lists of lu/lexeme references (instances of the
            :class:`Ref <.data_structures.utils.Ref>` class).
    """
    attr = Attrib(str(head))

    attr._data = {}

    parsed_data, attr.comments = parse_keyval(body, default_aspect=default_aspect)

    for key, val in parsed_data.items():
        ids = filter(lambda t: isinstance(t, tok.Identifier), val)  # type: ignore
        attr._data[key] = [Ref(str(id), 'LexicalUnit') for id in ids]

    return attr


@AttributeFactory.register_parser('synon')
def parse_synon(head, body, default_aspect='all') -> Attrib:
    """
        Parses the synon attribute.

        The body of the synon attribute consists of semicolon separated groups of synonymous
        words (possibly different for each aspect, specified by prefixing the aspect name
        followed by a colon), each group is a comma separated list of words (or multiple words
        separated by spaces), e.g.::

            -synon: impf: přemisťovat, vodit; příp. vybírat
                    pf: přemístit, přivést; zavést; příp. vybrat

        This would be parsed into the following dict::

            {
                'impf:[
                    ['přemisťovat', 'vodit'],
                    ['příp. vybírat']
                ],
                'pf':[
                    ['přemístit', 'přivést'],
                    ['zavést'],
                    ['příp. vybrat']
                ]
            }

        Returns:
            The parsed attribute whose :attr:`_data <.data_structures.attribs.Attrib._data>` is a dictionary
            keyed by the aspect whose values are the synonym groups, each being a list of
            (multi)word synonyms.
    """
    attr = Attrib(str(head))
    attr._data = {}

    parsed_data, attr.comments = parse_keyval(body, default_aspect=default_aspect)

    # Make a dictionary keyed by aspect, values are lists of synonym groups (separated by semicolons)
    data = {k: tok.split(v, tok.Semicolon, include_separators=False) for k, v in parsed_data.items()}  # type: ignore

    # Convert each synonym group into a list of multi words (separated by commas);
    # Moreover, convert each multi word (a list of tokens) into a string using tok.join
    attr._data = {
        k: [
            [
                tok.join(multi_word, '').strip() for multi_word in tok.split(syn_grp, tok.Comma, include_separators=False)  # type: ignore
            ] for syn_grp in groups
        ] for k, groups in data.items()
    }
    return attr


@AttributeFactory.register_parser('reflexverb', 'reflexverb1', 'reflexverb2', 'reflexverb3', 'reflexverb4',
                                  'reflexverb5')
def parse_reflexverb(head, body, default_aspect='all') -> Attrib:
    """
        Parses the reflexverb attribute.

        The body of the reflexverb attribute consists of a single reflexverb value and potentialy id of a referred LU and
        example wrapped in % signs. e.g.::
            -reflexverb: derived-decaus1 blu-v-shromáždit-shromažďovat-1 pf: %Pokud jsou implantáty PIP skutečně toxické, mělo by být zveřejněno jak a pak by se měly implantáty vyjímat a ženy léčit, například vyoperovat jim lymfatické uzliny, ve kterých se silikon shromáždil.% (SYN)
            -reflexverb: derived-autocaus3 blu-v-shromáždit-shromažďovat-1 pf: %Kolem náklaďáku se shromáždili mniši a čekali na Petridevy příkazy.% (SYN)

        This would be parsed into the following dict::
            {
                'derived-decaus': {
                    'ids': [Ref('blu-v-shromáždit-shromažďovat-1', 'LexicalUnit')],
                    'example': 'pf: %Pokud jsou implantáty PIP skutečně toxické, mělo by být zveřejněno jak a pak by se měly implantáty vyjímat a ženy léčit, například vyoperovat jim lymfatické uzliny, ve kterých se silikon shromáždil.% (SYN)'
                }
                'derived-autocaus': {
                    'ids': [Ref('blu-v-shromáždit-shromažďovat-1', 'LexicalUnit')],
                    'example': 'pf: %Kolem náklaďáku se shromáždili mniši a čekali na Petridevy příkazy.% (SYN)
                }
            }

        Returns:
            The parsed attribute whose :attr:`_data <.data_structures.attribs.Attrib._data>` is a dictionary
            keyed by the reflexverb value, whose values are dictionaries with two optional keys:
              ids         single-element list containing an element of type Ref
              example     type of example value: string
    """
    attr = Reflexverb(str(head))
    attr._data = {}

    cur_key = ''
    for t in body:
        val = str(t)
        if isinstance(t, tok.Value) or val.startswith('derived-'):
            cur_key = val
            if cur_key not in attr._data:
                attr._data[cur_key] = {'error': ''}
            else:
                data_error("parser:txt", error.INVALID_ATTR_VALUE, t._loc,
                           f"Annotation error: {cur_key} should be stated only once for a LU!")
                attr._data[cur_key]['error'] += f"Annotation error: {cur_key} should be stated only once for a LU!\n"
            if not isinstance(t, tok.Value):
                data_error("parser:txt", error.INVALID_ATTR_VALUE, t._loc,
                           f"Annotation error: attribute {str(head)} has an unexpected value {val}.")
                attr._data[cur_key]['error'] += f"Annotation error: attribute {str(head)} has an unexpected value {val}.\n"
            continue
        if cur_key not in attr._data:
            data_error("parser:txt", error.INVALID_ATTR_VALUE, t._loc,
                       f"Annotation error: attribute reflexverb does not have the expected value structure")
            attr._data['error'] = True
            cur_key = val
            attr._data[cur_key] = {'error': f"Annotation error: attribute {head} does not have the expected value structure.\n"}
            continue
        if isinstance(t, tok.Identifier):
            if 'ids' in attr._data[cur_key]:
                data_error("parser:txt", error.INVALID_ATTR_VALUE, t._loc,
                           f"Annotation error: {cur_key} and {t} should be stated only once for a LU!")
                attr._data[cur_key]['ids'].append(Ref(t._val, 'LexicalUnit'))
            else:
                attr._data[cur_key]['ids'] = [Ref(t._val, 'LexicalUnit')]
            continue
        if isinstance(t, tok.Aspect):
            if 'example' in attr._data[cur_key]:
                attr._data[cur_key]['example'] += val+": "
            else:
                attr._data[cur_key]['example'] = val+": "
        elif val == '%' and 'example' not in attr._data[cur_key]:
            attr._data[cur_key]['example'] = val
        elif val and 'example' in attr._data[cur_key]:
            attr._data[cur_key]['example'] += val
    for key in attr._data:
        if key != "error":
            if 'example' in attr._data[key]:
                # remove extra whitespace
                attr._data[key]['example'] = ' '.join(attr._data[key]['example'].split())
            if attr._data[key]['error'] == '':
                del attr._data[key]['error']
    assert isinstance(attr._data, dict)
    return attr


@AttributeFactory.register_parser('exceptions')
def parse_exceptions(head, body, default_aspect='all') -> Attrib:
    """
        Parses the exceptions attribute

        Returns:
            A hash whose keys are the names of tests such that a TestFailed result should be ignored for this LU.
    """
    attr = Attrib(str(head))
    attr._data = [key._src for key in body if type(key) not in [tok.Newline, tok.WhiteSpace, tok.Comma, tok.Semicolon]]
    return attr
