import logging
import os
import sys
import tempfile

import vallex.term

from pathlib import Path

from vallex.cli.common import load_lexicons
from vallex.cli.lib import main_command, root
from vallex.config import FROZEN, __version__
from vallex.server import maint
from vallex.server.sql_store import SQLStore
from vallex.term import STATUS
from vallex.log import log

try:
    from PyQt5.QtWidgets import QApplication  # type: ignore
    from PyQt5.QtGui import QIcon  # type: ignore

    from vallex.gui.main_window import MainWindow

    GUI_AVAILABLE = True
except Exception as ex:
    GUI_AVAILABLE = False
    _GUI_ERROR = ex
    log("cli:gui", logging.DEBUG, "Could not import PyQt", _GUI_ERROR)


@main_command()
def main(options={}):
    """Run the QtWebEngine based gui."""
    if not GUI_AVAILABLE:
        STATUS.print("GUI not available", _GUI_ERROR)
        log("cli:gui", logging.ERROR, "Could not import PyQt", _GUI_ERROR)
        return -1

    lexicon_files = options.get('load-lexicons', [])
    if lexicon_files:
        # If there are input files specified on the command line
        # we create a temporary store and initialize it with the
        # data from the inputs and then pass that to the webapp
        coll = load_lexicons(options)
        _, store_path = tempfile.mkstemp(suffix='.db')
        store = SQLStore(store_path)
        maint.webdb_migrate(store)
        maint.webdb_add_lexicons(store, coll.lexicons)
        remove_db_on_exit = True  # FIXME: unused variable, probably copy-pasted from code in web.py
        root.config.web_db = Path(store_path)
        root.config.web_lexicons = [Path(lexicon._path) for lexicon in coll.lexicons]
    else:
        remove_db_on_exit = False  # FIXME: unused variable, probably copy-pasted from code in web.py
        store = SQLStore(root.config.web_db)
        maint.webdb_migrate(store)
        maint.webdb_update(store)
        maint.webdb_add_lexicons(store, root.config.web_lexicons)

    if sys.platform == 'win32' and FROZEN:
        # This is needed to enable QtWebEngine process
        # to find Qt dlls.
        os.environ['PATH'] += ';'+sys._MEIPASS
    _qtapp = QApplication(sys.argv)
    _qtapp.setWindowIcon(QIcon(str(Path(__file__).parent.parent.parent/'pyvallex.ico')))
    gui = MainWindow(root.config, store)
    vallex.term.MODE = vallex.term.IOMode.QT
    ret = _qtapp.exec_()
    gui.app_server.stop()
    return ret
