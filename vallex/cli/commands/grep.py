from vallex.cli.common import load_lexicons, save_lexicons
from vallex.cli.lib import main_command, option
from vallex.grep import parse_pattern, filter_coll
from vallex.scripts import run_scripts
from vallex.term import FG, RED, STATUS


@option('--restrict-to-attrs',  str, help='A comma separated list of attributes which will be included in the output; other attributes will be discarded.')
@main_command()
def main(pattern='', options={}):
    """Search the lexicons.

       Expects a single argument --- the search pattern. Each search pattern
       is an '&'-separated list of conditions which are AND-ed together and
       each lexical unit is matched against the condition. In the end, only
       lexemes with matching lexical units are printed.

       Each condition is a key=pattern pair, where key is a match key and
       pattern is a regular expression. The condition is evaluated exactly
       as in the web frontend.
    """
    try:
        pattern = parse_pattern(pattern)
    except Exception as ex:
        STATUS.print(FG(RED) | "Invalid grep pattern", "'"+pattern+"'", "Error:", str(ex))
        return -1

    coll = load_lexicons(options)

    progress = STATUS.progress_bar("Running test scripts")
    _, failures, warnings = run_scripts(coll, 'test', progress.update)
    progress.done()

    for test_name, obj, message in failures:
        obj._errors.append((test_name, str(message)))
    for test_name, obj, message in warnings:
        obj._warnings.append((test_name, str(message)))

    if 'restrict-to-attrs' in options:
        restrict = options['restrict-to-attrs'].split(',')
    else:
        restrict = None

    coll = filter_coll(coll, pattern+options.get('post-pattern', []), no_sort=options['no-sort'], restrict_to_attrs=restrict)

    save_lexicons(coll, options)
