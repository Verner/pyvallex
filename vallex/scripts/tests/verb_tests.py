import re

from vallex.scripts import TestDoesNotApply, TestFailed, TestWarning, requires
from vallex.data_structures.constants import ATTR_REFLEXVERB_OLD_VALUES_REGEX, ATTR_REFLEXVERB_VALUES_REGEX, REFLEXIVE_LU_ID_REGEX


def test_lu_diat_acc_without_deagent(lu):
    """
        Verbs that allow accusative form of one of their complementations
        must also allow the deagentive diathesis.
    """
    if ('pos' not in lu.dynamic_attribs
            or lu.dynamic_attribs['pos']._data != 'verb'):
        raise TestDoesNotApply

    forms = sum([fe.forms for fe in lu.frame.elements], [])
    has_acc = '4' in forms or 'adj-4' in forms

    if not has_acc or lu.isReflexive:
        raise TestDoesNotApply

    deagent = 'diat' in lu.attribs and 'deagent' in lu.attribs['diat']._data

    if not deagent:
        raise TestFailed()


def test_lu_lvc_example_count(lu):
    """
        Atribut example by měl mít přesně tolik příkladů pro každý vid, kolik je hodnot v lvc.
    """
    failures = []
    lvc_varianty = [k for k in lu.attribs.keys() if k.startswith('lvc')]
    for attrib in lvc_varianty:
        ex_name = 'example' if attrib == 'lvc' else 'example' + attrib[-1]
        if ex_name not in lu.attribs:
            failures.append("Missing examples for "+attrib)
        else:
            exs = lu.attribs[ex_name]._data
            expected_len = sum([len(v) for v in lu.attribs[attrib]._data.values()])
            for aspect, vexs in exs.items():
                if len(vexs) != expected_len:
                    failures.append("Number of examples ("+str(len(exs[aspect]))+") for "+attrib+" does not match number of ids ("+str(expected_len)+") in aspect "+aspect)
    if failures:
        raise TestFailed("\n".join(failures))


@requires('lumap')
def test_lu_lvc_references(lu, lumap):
    """
        In attributes with links, each link should point to an existing lu.
    """
    failures = []
    lvc_variants = [k for k in lu.attribs.keys() if k.startswith('lvc')]
    if not lvc_variants:
        raise TestDoesNotApply
    applies = False
    for attrib in lvc_variants:
        if not isinstance(lu.attribs[attrib]._data, dict):
            continue
        refs = lu.attribs[attrib]._data['ids']
        if refs:
            applies = True
            for ref in refs:
                if not ref._id.startswith('@') and ref._id not in lumap['lus']:
                    failures.append(str(ref._id))
    if failures:
        raise TestFailed("The following references not found in the LexiconCollection: "+','.join(failures))
    elif not applies:
        raise TestDoesNotApply()


def test_lu_reflexverb_present(lu):
    """
        Každé sloveso s reflexivem v lemmatu nebo v id má atribut "reflexverb" (nekontroluje se volné SE/SI).
    """
    if ('pos' not in lu.dynamic_attribs
            or lu.dynamic_attribs['pos']._data != 'verb'):
        raise TestDoesNotApply

    if lu.isReflexive == 'always' or REFLEXIVE_LU_ID_REGEX.search(lu._id):
        reflexverb_attribs = [attr for attr in lu.attribs if attr.startswith('reflexverb')]
        if not reflexverb_attribs:  # no reflexverb attribute
            raise TestFailed()
    else:
        raise TestDoesNotApply


def test_lu_reflexverb_reciprocal_verbs_attrs(lu):
    """
        Verbs having reflexverb=derived-recipr need to also have
        values for `recipr`, `reciprevent` and `reciprverb`
        attributes.
    """
    if ('pos' not in lu.dynamic_attribs
            or lu.dynamic_attribs['pos']._data != 'verb'):
        raise TestDoesNotApply

    reflexverb_attrs = [attr for attr_name, attr in lu.attribs.items() if attr_name.startswith('reflexverb')]
    if not reflexverb_attrs:
        raise TestDoesNotApply

    failures = []
    applies = False

    for reflexverb in reflexverb_attrs:
        for key in reflexverb._data.keys():
            if key.startswith('derived-recipr'):
                applies = True
                if not ('recipr' in lu.attribs or 'recipr1' in lu.attribs):
                    failures.append("recipr")
                if not ('reciprevent' in lu.attribs or 'reciprevent1' in lu.attribs):
                    failures.append("reciprevent")
                if not ('reciprverb' in lu.attribs or 'reciprverb1' in lu.attribs):
                    failures.append("reciprverb")
                if failures:
                    raise TestFailed(f"Missing attributes: {','.join(failures)}")
                break
    if not applies:
        raise TestDoesNotApply


@requires('lumap')
def test_lu_reflexverb_value(lu, lumap):
    """
        Check validity of the 'reflexverb' attribute.
        Valid values (see vallex/data_structures/constants.py):
            tantum | derived-decaus | derived-autocaus | derived-conv | 
            derived-partobject | derived-deaccus | derived-nonspecific
    """
    MUST_BE_SINGLE = ['tantum', 'derived-nonspecific']

    numbered_reflexverb = {}
    error, warning, applies = '', '', False
    for attr_name, attr_val in lu.attribs.items():
        if attr_name.startswith('reflexverb'):
            applies = True
            if not attr_val._data:
                error += f"{attr_name} has an empty value or was not properly parsed.\n"
                continue
            elif 'error' in attr_val._data:
                error += f"There was en error during parsing {attr_name}.\n"
                continue

            for key, val in attr_val._data.items():
                if 'error' in val:
                    error += val['error']
                if not ATTR_REFLEXVERB_VALUES_REGEX.match(key):  # check that key has a valid value
                    if ATTR_REFLEXVERB_OLD_VALUES_REGEX.match(key):
                        warning += f"Outdated value - {attr_name}: {key}!\n"
                    else:
                        error += f"Invalid value - {attr_name}: {key}!\n"

                """
                    Má-li LU v atributu "reflexverb" uvedenu kteroukoli hodnotu 
                    vyjma "derived-nonspecific" nebo "tantum",
                    pak za ní následuje odkaz k nereflexivnímu slovesu v podobě id jednotky. 
                    Je třeba zkontrolovat, zda taková nerefl LU existuje.
                """
                if key not in MUST_BE_SINGLE:
                    if not val.get('ids', None):
                        error += f"{attr_name}: {key} is missing a reference to a non-reflexive LU!\n"
                    else:
                        for ref in val['ids']:
                            if not ref._id in lumap['lus']:
                                error += f"{attr_name} references LU {ref._id}, which could not be found!\n"
                            else:
                                lu = lumap['lus'][ref._id]
                                if lu.isReflexive == 'always' or REFLEXIVE_LU_ID_REGEX.search(ref._id):
                                    error += f"{attr_name} references LU {lu._id}, which is reflexive!\n"

                """
                    Má-li LU v atributu "reflexverb" buď hodnotu "tantum", nebo "derived-nonspecific", 
                    jde o jedinečnou hodnotu a další atribut "reflexverb" s jinou hodnotou se u dané LU nemůže opakovat.
                """
                # FIXME: this does not really test presence of other 'reflexverb1',... attributes
                # remove this warning when we join all 'reflexverbX' attributes into a single one
                if key in MUST_BE_SINGLE and len(attr_val._data) > 1:
                    error += "Unexpected number of reflexverb values!\n"

                """
                    Jediná LU může mít uvedeno více hodnot z následujcí množiny:
                    "derived-decaus[1-5] | derived-autocaus[1-4] | derived-conv[1-11] | derived-partobject[1-3] |
                     derived-deaccus[1-2] | derived-recipr[1-3] | derived-quasiconv[1-2]", 
                    pak je každá uvedena v samostatném atributu "reflexverb", který je číslovaný.
                """
                if key not in MUST_BE_SINGLE:
                    if attr_name not in numbered_reflexverb:
                        numbered_reflexverb[attr_name] = key
                    else:
                        error += f"Attribute name {attr_name} is not unique due to wrong numbering.\n"

                """
                    Vyskytuje-li se u jediné LU v atributech "reflexverb" hodnoty "derived-decaus" a "derived-conv", 
                    pak je v rámci dané LU přítomen MEANS nebo CAUS s formou 7 
                    (další formy jsou přípustné a nekontrolují se).
                """
                joined_values = " ".join(numbered_reflexverb.values())
                if (re.search(r'derived-decaus[1-5]', joined_values) and
                        re.search(r'derived-conv[1-11]', joined_values)):
                    condition_satisfied = False
                    for f in lu.frame._data:
                        if (f.functor == 'CAUS' or f.functor == 'MEANS') and str(7) in f.forms:
                            condition_satisfied = True
                    if not condition_satisfied:
                        error += (f'Condition on reflexverb unsatisfied!' +
                                  f'{lu._id} contains both "derived-decaus" and "derived-conv"' +
                                  f'while "CAUS" or "MEANS" with form eq. to 7 is missing in the frame!\n')

    if error:
        raise TestFailed(error)
    if warning:
        raise TestWarning(warning)

    if not applies:
        raise TestDoesNotApply


ATTRIBUTE_NAME_REFLEX = re.compile(r'reflex[0-9]?$')


@requires('lumap')
def test_lu_reflexverb_consistency(lu, lumap):
    """
        Checks the consistency of annotations.
    """
    linked_lus = set()
    reflexverb_values = set()
    derived_autocaus_linked_to_LU_with_reflex = []
    warning, applies = '', False
    for attr_name, attr_val in lu.attribs.items():
        if attr_name.startswith('reflexverb'):
            applies = True
            for key, val in attr_val._data.items():
                reflexverb_values.add(key)
                if val.get('ids', None):
                    for ref in val['ids']:
                        linked_lus.add(ref._id)

                """
                    Vypisovat případy LU s hodnotou "derived-autocaus" v atributu "reflexverb", 
                    u nichž právě tato hodnota odkazuje k LU s atributem "reflex". 
                    Opět tyto případy mohou být podezřelé, ale ne chybné.
                """
                if key == 'derived-autocaus' and 'ids' in val:
                    for ref in val['ids']:
                        if ref._id in lumap['lus']:
                            linked_lu = lumap['lus'][ref._id]
                            linked_lus_reflex = [attrib for attrib in linked_lu.attribs if ATTRIBUTE_NAME_REFLEX.match(attrib)]
                            if linked_lus_reflex:
                                derived_autocaus_linked_to_LU_with_reflex.append(ref._id)

    if derived_autocaus_linked_to_LU_with_reflex:
        warning += '"derived-autocaus" refers to an LU with attribute "reflex": '
        warning += ', '.join(derived_autocaus_linked_to_LU_with_reflex) + '.\n'

    if len(linked_lus) > 1:
        warning += 'Different LUs are referenced within a single LUs reflexverb attributes:'
        warning += ', '.join(linked_lus) + '.\n'

    derived_autocaus = [value for value in reflexverb_values if value.startswith('derived-autocaus')]
    derived_partobject = [value for value in reflexverb_values if value.startswith('derived-partobject')]
    if derived_autocaus and derived_partobject:
        warning += f"Both {', '.join(derived_autocaus)} and {', '.join(derived_partobject)} are present.\n"

    if warning:
        raise TestWarning(warning)
    elif not applies:
        raise TestDoesNotApply
