from vallex.scripts import TestDoesNotApply, TestFailed, requires
from vallex.data_structures.constants import ATTRIBUTE_NAMES


def test_collection_id_uniqueness(collection):
    """
        IDs of lexical units should be unique across the whole collection
    """

    ids = {}
    bad_ids = set()
    for lu in collection.lexical_units:
        if lu._id in ids:
            bad_ids.add(lu._id)
        else:
            ids[lu._id] = True

    if bad_ids:
        raise TestFailed(f"The following lu id's were not unique: {', '.join(bad_ids)}.")


@requires('luidcount')
def test_lu_id_uniqueness(lu, luidcount):
    """
        IDs of lexical units should be unique across a lexicon
    """
    if luidcount[lu._id] > 1:
        raise TestFailed(f"The lu id {str(lu._id)} is not unique, multiplicity: {str(luidcount[lu._id])}.")


def test_lexeme_id_prefixes(lexeme):
    """
        ID prefixes of all lexical units in a given lexeme should match
    """
    if len(lexeme.lexical_units) < 2:
        raise TestDoesNotApply

    pref = lexeme.lexical_units[0]._id
    pref = pref[:pref.rfind('-')]

    mismatched_lus = []
    for lu in lexeme.lexical_units:
        if not lu._id.startswith(pref):
            mismatched_lus.append(lu._id)

    if mismatched_lus:
        raise TestFailed(f"The following lu id's do not match the expected prefix '{str(pref)}' (from the first lu): {','.join(mismatched_lus)}.")


def test_lu_pos_present(lu):
    """
        Each lu has a (dynamically computed) part of speech.
    """
    if 'pos' not in lu.dynamic_attribs:
        raise TestFailed('Lexical unit', lu._id, 'does not have dynamically computed attribute pos')
    if lu.dynamic_attribs['pos']._data not in ['verb', 'adjective', 'stem noun', 'root noun', 'adverb']:
        raise TestFailed('Unexpected pos: '+str(lu.dynamic_attribs['pos']))


def test_lexicon_lemma_chybejici_varianta(lexicon):
    """
        Slova, která jsou jednou uvedená s variantou,
        by se nikdy neměla vyskytovat bez ní (ignorujeme se/si).
    """
    import re
    variants = {}
    for lex in lexicon.lexemes:
        for lu in lex.lexical_units:
            for lemma in lu.lemma._data.values():
                # Throw away any si / se
                lemma = re.sub(r"\s*\(?\s+s[ei]([\s)]|$)\s*", "", lemma)
                vars = lemma.split('/')
                if len(vars) > 1:
                    variants.update({v.strip(): lu for v in vars})

    if not variants:
        raise TestDoesNotApply

    for lex in lexicon.lexemes:
        for lu in lex.lexical_units:
            for lemma in lu.lemma._data.values():
                # Throw away any si / se
                lemma = re.sub(r"\s*\(\s*s[ei]\s*\)\s*", "", lemma).strip()
                if '/' not in lemma:
                    if lemma in variants:
                        raise TestFailed("Lemma "+lemma+" has no variant in "+lu._id+" but has a variant in "+variants[lemma]._id)
    return str(len(variants))+" instances passed"


def test_lu_frame_correct_obligatory_type(lu):
    """
        Obligatornost "opt" je přípustná pouze u funktorů ACT|PAT|ADDR|EFF|ORIG|OBST|DIFF|INTT,
        naopak u těchto funktorů je nepřípustná obligatornost "typ"
    """
    applies = False
    for frame_elt in lu.frame.elements:
        if frame_elt.oblig == 'typ':
            applies = True
            if frame_elt.functor in ['ACT', 'PAT', 'ADDR', 'EFF', 'ORIG', 'OBST', 'DIFF', 'INTT']:
                raise TestFailed()
        elif frame_elt.oblig == 'opt':
            applies = True
            if frame_elt.functor not in ['ACT', 'PAT', 'ADDR', 'EFF', 'ORIG', 'OBST', 'DIFF', 'INTT']:
                raise TestFailed()

    if not applies:
        raise TestDoesNotApply


def test_lu_frame_dublovana_forma(lu):
    """
        Rámce by neměly mít v témže slotu jednu formu dvakrát.
    """
    for frame_elt in lu.frame.elements:
        if len(frame_elt.forms) != len(set(frame_elt.forms)):
            raise TestFailed()


def test_lu_frame_chybejici_act(lu):
    """
        Rámce, ve kterých je PAT|CPHR|DPHR, ale ne ACT
        (tedy zanedbaný princip posouvání).
    """

    functors = {fe.functor for fe in lu.frame.elements}

    if 'PAT' not in functors and "CPHR" not in functors and 'DPHR' not in functors:
        raise TestDoesNotApply

    if 'ACT' not in functors:
        # if ('pos' in lu.dynamic_attribs
        #    and lu.dynamic_attribs['pos']._data != 'verb'):
        raise TestFailed()


def test_lu_frame_chybejici_pat(lu):
    """
        Rámce, ve kterých je ADDR|EFF|ORIG, ale ani PAT ani [CD]PHR
        (tedy zanedbaný princip posouvání).
    """
    functors = {fe.functor for fe in lu.frame.elements}

    if 'ACT' not in functors:
        raise TestDoesNotApply

    if 'ADDR' not in functors and "ORIG" not in functors and 'EFF' not in functors:
        raise TestDoesNotApply

    if 'CPHR' in functors or 'DPHR' in functors:
        raise TestDoesNotApply

    if 'PAT' not in functors:
        # if ('pos' in lu.dynamic_attribs
        #    and lu.dynamic_attribs['pos']._data != 'verb'):
        raise TestFailed()


def test_lu_attribute_unexpected(lu):
    """
        Complains about unexpected attribute names.
    """
    error = []
    for attr_name, attr_val in lu.attribs.items():
        if attr_name not in ATTRIBUTE_NAMES and \
                (not attr_val.duplicate or attr_val.duplicate not in ATTRIBUTE_NAMES) and \
                attr_name != 'idiom':
            error.append(attr_name)
    if error:
        raise TestFailed("Unexpected attribute name(s): " + ", ".join(error) + ".")


def test_lu_attribute_duplicate(lu):
    """
        Except for `note`, each attribute may appear at most once.
    """
    error = []
    for attr_name, attr_val in lu.attribs.items():
        if attr_val.duplicate and attr_val.duplicate != 'note':
            error.append(attr_val.duplicate)
    if error:
        raise TestFailed("Duplicated attribute(s): " + ", ".join(error) + ".")
