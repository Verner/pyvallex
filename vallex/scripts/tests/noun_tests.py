import re

from vallex.scripts import requires, TestDoesNotApply, TestFailed, TestWarning


derived_references_exceptions_lexeme = {
    # set of LUs that may have a lexeme id (rather than an LU id) in the derivedFrom attribute
    'blu-v-none'
}


@requires('lumap')
def test_lu_derived_references(lu, lumap):
    """
        The derived attribute should point to an existing lu.
    """
    failures = []
    lexeme_warnings = []
    derived_varianty = [k for k in lu.attribs.keys() if k.startswith('derived')]
    if not derived_varianty:
        raise TestDoesNotApply
    applies = False
    for attrib in derived_varianty:
        if not isinstance(lu.attribs[attrib]._data, dict):
            continue
        refs = lu.attribs[attrib]._data['ids']
        if refs:
            applies = True
            for ref in refs:
                if ref._id in lumap['lexemes'] and lu._id not in derived_references_exceptions_lexeme:
                    lexeme_warnings.append(str(ref._id))
                elif not (ref._id.startswith('@') or
                          ref._id in lumap['lus'] or
                          (lu._id in derived_references_exceptions_lexeme and ref._id in lumap['lexemes']) or
                          (ref._id[-3:] == '-no' and ref._id[:-3] in lumap['lexemes']) or
                          ref._id in ['Vallex-no', 'no']
                          ):
                    failures.append(str(ref._id))
        else:
            continue
    errors = ''
    warnings = ''
    if failures:
        errors += "The following references not found in LexiconCollection: "+', '.join(failures)
    if lexeme_warnings:
        warnings += ("The following ids are lexeme references (not LU references): " +
                     ', '.join(lexeme_warnings) +
                     f"; if that is intended, add '{lu._id}' to the list of exceptions.")
    if errors != '':
        raise TestFailed(errors)
    if warnings != '':
        raise TestWarning(warnings)
    if not applies:
        raise TestDoesNotApply


PRED_REGEX = re.compile('[.]PRED(?!_(NEG|S[EI]))')
PRED_NEG_REGEX = re.compile('[.]PRED_NEG')
PRED_SE_SI_REGEX = re.compile('[.]PRED_S[EI]')
PRED_EXCEPTION_REGEX = re.compile(r';\s*#.*exceptions:\s*mark_PRED')

mark_PRED_exceptions = {
    "TODO",
    "NO",
}


def test_lu_examplerich_mark_PRED(lu):
    """
        Tests that each line in the examplerich attribute contains exactly one
        instance of the .PRED mark,
        and at most one instance of .PRED_SE or .PRED_SI,
        and no .PRED_NEG unless the lu is known to have one.
    """
    if ('pos' not in lu.dynamic_attribs
            or not lu.dynamic_attribs['pos']._data in ('stem noun', 'root noun', 'adjective')):
        raise TestDoesNotApply

    try:
        examplerich = lu.attribs['examplerich']
    except Exception:
        raise TestDoesNotApply

    failures_number, failures_SE_SI, warnings = [], [], []
    examples = examplerich.all_examples
    for example in [str(ex) for ex in examples]:
        examplePRED = PRED_REGEX.findall(example)
        examplePRED_NEG = PRED_NEG_REGEX.findall(example)
        examplePRED_SE_SI = PRED_SE_SI_REGEX.findall(example)
        if example in mark_PRED_exceptions:
            continue
        if (len(examplePRED) + len(examplePRED_NEG) != 1 or
            (examplePRED_SE_SI and len(examplePRED_SE_SI) != len(examplePRED) + len(examplePRED_NEG)) or
            len(examplePRED_NEG) > 0
            ):
            index = examplerich.src.find(example)
            if index >= 0:
                index += len(example)
                if PRED_EXCEPTION_REGEX.match(examplerich.src, index):
                    continue
        if len(examplePRED) + len(examplePRED_NEG) != 1:
            failures_number.append(example)
        elif examplePRED_SE_SI and len(examplePRED_SE_SI) != len(examplePRED)+len(examplePRED_NEG):
            failures_SE_SI.append(example)
        if examplePRED_NEG:
            warnings.append(example)
    failures = ''
    if failures_number:
        failures += 'Either no or multiple occurrences of PRED or PRED_NEG in\n    ' + '\n    '.join(failures_number)
    if failures_SE_SI:
        failures += 'Number of PRED_SE/PRED_SI does not equal the number of PRED and PRED_NEG in\n    ' + '\n    '.join(failures_SE_SI)
    if failures:
        raise TestFailed(failures)
    if warnings:
        raise TestWarning('PRED_NEG not expected in\n    ' + '\n    '.join(warnings))


def test_lu_valdiff_eq_valdiffCurrent(lu):
    if not 'valdiff' in lu.attribs:
        raise TestDoesNotApply
    elif not 'valdiffCurrent' in lu.dynamic_attribs:
        raise TestFailed('LU has valdiff attribute but no valdiffCurrent value was computed.')
    elif lu.attribs['valdiff'] != lu.dynamic_attribs['valdiffCurrent']:
        raise TestFailed("previous:" + str(lu.attribs['valdiff']) + "\ncurrent: " + str(lu.dynamic_attribs['valdiffCurrent']))
