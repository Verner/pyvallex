""" A transform for joining numbered reflexverb attributes
    into a single unnumbered reflexverb attribute.
    TODO: repeated attribute 'reflexverb'
    TODO: attribute 'reflexverb' with unexpected structure




"""

from vallex.scripts import changes
from vallex.term import STATUS
from vallex.data_structures.attribs import Reflexverb


@changes('reflexverb')
@changes('reflexverb1')
@changes('reflexverb2')
@changes('reflexverb3')
@changes('reflexverb4')
@changes('reflexverb5')
def transform_lu_join_reflexverb(lu):
    numbered_reflexverb = [key for key in lu.attribs if key.startswith('reflexverb') and key != 'reflexverb']
    if numbered_reflexverb:
        data = {}
        src = ''
        for key in numbered_reflexverb:
            STATUS.print(lu.attribs[key]._data)
            for value, info in lu.attribs[key]._data.items():
                if value in data:
                    STATUS.print(f"Repeated value for {lu._id}: attribute {key} contains {value} which has already been present before.")
                data[value] = info
                lu.attribs[key] = Reflexverb(attr_name=key, data={})
        lu.attribs['reflexverb'] = Reflexverb(attr_name='reflexverb', data=data)
