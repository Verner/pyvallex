""" A transform for adding/recomputing the valdiff attribute in noun data




    ./vallex-cli --no-sort --output-format txt -o ../aktualni_data/data-txt/ scripts transform vallex/scripts/transforms/add_valdiff.py

    Note that the file with the verb data (v-vallex.txt) is exported empty.
"""
from vallex import Valdiff
from vallex.scripts import changes
from copy import deepcopy


@changes('valdiff')
@changes('valdiffNew')
def transform_lu_add_valdiff(lu):
    if 'valdiff' in lu.attribs:
        if 'valdiffCurrent' in lu.dynamic_attribs and \
                lu.dynamic_attribs['valdiffCurrent'] != lu.attribs['valdiff']:
            lu.attribs['valdiffNew'] = Valdiff(attr_name='valdiffNew', data=deepcopy(lu.dynamic_attribs['valdiffCurrent']._data))
    elif 'valdiffCurrent' in lu.dynamic_attribs:
        lu.attribs['valdiff'] = Valdiff(attr_name='valdiff', data=deepcopy(lu.dynamic_attribs['valdiffCurrent']._data))
