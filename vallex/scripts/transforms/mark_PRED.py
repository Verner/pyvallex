""" A transform for marking the lexeme lemmas in the examplerich attribute






"""

import logging
import sys
import os        # for better debugging
import requests  # type: ignore  # for calling an external api
import time      # for waiting between submitting requests
import re

from vallex import Lemma
from vallex.data_structures import Text, Comment
from vallex.log import log
from vallex.scripts import changes, requires, TestDoesNotApply, TestFailed
from typing import List


def call_morphodita(text: str, splitSentences=False) -> list:
    """
        Calls an external parser on a piece of text.
        Returns
          - if splitSentences=False:
              a list of dicts of the form
              {"token":"Děti","lemma":"dítě","tag":"NN","space":" "}.
          - if splitSentences=True:
              a list of lists of the form given above,
              one for each sentence
    """
    url = 'https://lindat.mff.cuni.cz/services/morphodita/api/tag'
    parameters = {
        "data": text,
        "output": "json",
        "model": "czech-morfflex-pdt-161115",
        "convert_tagset": "strip_lemma_id"
    }
    try:
        time.sleep(1/14)  # there is a limit of 15 requests per second for the morphodita API
        response = requests.get(url, parameters)
        result = response.json()['result']
        if splitSentences:
            return result
        else:
            return [word for sentence in result for word in sentence]
    except Exception as ex:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        log("mark_PRED", logging.ERROR, "attempt to call an external api resulted in exception of ", exc_type, ":", ex)
        return [None]


def mark_lemma_in_parsed_text(parsedText: list, testlemmas: list, testreflexives: list, pos: str) -> str:
    """
        Gets a parsedText in the form of a list of dicts of the form
            {"token":"Děti","lemma":"dítě","tag":"NNFP1-----A----","space":" "}.
        and returns a string consisting of concatenated tokens and spaces,
        with tokens whose lemma appears in testlemmas marked as ".PRED"
        or ".PRED_NEG" (for the negated form)
        and "se" or "si" marked as ".PRED_SE/.PRED_SI" if it possibly could be
        part of the lemma.
    """
    text = ''
    lastlemma = ''
    for word in parsedText:
        text += word['token']
        if word['token'] in testreflexives and \
                (pos == 'adjective' or lastlemma in testlemmas):
            text += '.PRED_' + word['token'].upper()  # se/si which may be part of the lemma
        elif word['lemma'] in testlemmas:
            if word['tag'][10] == 'N':  # the 11th position is negation
                text += '.PRED_NEG'
            else:
                text += '.PRED'
        elif 'ne'+word['lemma'] in testlemmas and word['tag'][10] == 'N':
            text += '.PRED'
        if 'space' in word.keys():
            text += word['space']
        lastlemma = word['lemma']
    return text


known_errors = {
    # A dictionary linking lu ids to changes that need to be performed
    # at the end of transform_lu_mark_PRED()
    # to revert changes that are known to be erroneous.
    'blu-a-jistý-si-2': [['Tam si.PRED_SI m', 'Tam si m']],
    'blu-a-spokojený-1': [['spokojení', 'spokojení.PRED']],
    'blu-a-dotčený-2': [['dotčen ', 'dotčen.PRED '],
                        ['dotčení,', 'dotčení.PRED,'],
                        ['dotčení ', 'dotčení.PRED ']],
    'blu-a-chudý-1': [['Chudí ', 'Chudí.PRED ']],
    'blu-a-lačný-1': [['lační,', 'lační.PRED,']],
    'blu-a-opravňovaný-oprávněný-1': [['opravňován ', 'opravňován.PRED '],
                                      ['opravňováni ', 'opravňováni.PRED ']],
    'blu-a-rozhodující-se-1': [['se.PRED_SE v podstatě', 'se v podstatě'],
                               ['se.PRED_SE zaměřuje', 'se zaměřuje']],
    'blu-a-rozhodující-2': [['naloží.PAT se.PRED_SE', 'naloží.PAT se'],
                            ['se.PRED_SE stává.PAT', 'se stává.PAT'],
                            ['se.PRED_SE ukázala', 'se ukázala'],
                            ['co by se.PRED_SE mělo', 'co by se mělo']],
    'blu-n-vzpomínání-si-vzpomenutí-si-1': [['Je to možná pozdní si vzpomenutí.PRED', 'Je to možná pozdní si.PRED_SI vzpomenutí.PRED']],
    'blu-n-bání-se-1': [['nějaké to bání se', 'nějaké to bání.PRED se.PRED_SE']],
    'blu-a-ovladatelný-ovládatelný-ovládnutelný-1': [['Otázkou samozřejmě zůstává, jak dalece jsou takovíto lidé snadno ovládatelní církvemi.ACT právě prostřednictvím těchto náboženství, jejich mýtů, rituálů a symbolů.', 'Otázkou samozřejmě zůstává, jak dalece jsou takovíto lidé snadno ovládatelní.PRED církvemi.ACT právě prostřednictvím těchto náboženství, jejich mýtů, rituálů a symbolů.'],
                                                     ['Ze státu, který byl v mnoha ohledech soběstačný, udělali za pár let montážní stát, který je snadno ovládatelný nejen nadnárodním kapitálem.ACT, ale i státy.ACT, kde má či mají své sídlo',
                                                         ' Ze státu, který byl v mnoha ohledech soběstačný, udělali za pár let montážní stát, který je snadno ovládatelný.PRED nejen nadnárodním kapitálem.ACT, ale i státy.ACT, kde má či mají své sídlo'],
                                                     ['Například podle Constantina Baldissara z Grimaldi Lines budou autonomní plavidla velmi zranitelná vůči hackerským útokům a také budou snadno ovládnutelná počítačovými piráty.ACT.',
                                                         'Například podle Constantina Baldissara z Grimaldi Lines budou autonomní plavidla velmi zranitelná vůči hackerským útokům a také budou snadno ovládnutelná.PRED počítačovými piráty.ACT.'],
                                                     ['produkci většiny pastvin, s výjimkou některých extrémních stanovišť, víc podmiňují faktory člověkem.ACT ovládnutelné',
                                                         'produkci většiny pastvin, s výjimkou některých extrémních stanovišť, víc podmiňují faktory člověkem.ACT ovládnutelné.PRED'],
                                                     ['mechanismy korupce a cest k moci v hospodářsky i duchovně zbídačelé společnosti, snadno ovládnutelné terorem.MEANS',
                                                         'mechanismy korupce a cest k moci v hospodářsky i duchovně zbídačelé společnosti, snadno ovládnutelné.PRED terorem.MEANS'],
                                                     ['Hněv je ovládnutelný pokojným přemýšlením.MEANS, které vede k pochopení', ' Hněv je ovládnutelný.PRED pokojným přemýšlením.MEANS, které vede k pochopení'],
                                                     ['ovládnutelné.PRED.PRED', 'ovládnutelné.PRED']],
    'blu-a-ovladatelný-ovládatelný-ovládnutelný-2': [['díky klávesnici je spousta funkcí ovládatelná jednou rukou.MEANS', 'díky klávesnici je spousta funkcí ovládatelná.PRED jednou rukou.MEANS'],
                                                     ['Plně ovládnutelné auto totiž také žádá nadstandardní schopnosti od řidiče.', 'Plně ovládnutelné.PRED auto totiž také žádá nadstandardní schopnosti od řidiče.']],
    'blu-a-vlastní-1': [['Křesťanská spiritualita je vlastní nejen obrazům.ACT', 'Křesťanská spiritualita je vlastní.PRED nejen obrazům.ACT']],
    'blu-a-zmatený-1': [['A diváci zmatení telenovelami.PAT se asi domnívají, že skutečné city takhle nějak vypadají.', 'A diváci zmatení.PRED telenovelami.PAT se asi domnívají, že skutečné city takhle nějak vypadají.'],
                        ['ptáci jsou podle některých odborníků zmatení vývojem.PAT počasí', 'ptáci jsou podle některých odborníků zmatení.PRED vývojem.PAT počasí']],
    'blu-a-znatelný-1': [['Váhový rozdíl je znatelný pouhým okem.MEANS.', 'Váhový rozdíl je znatelný.PRED pouhým okem.MEANS.'],
                         ['jindy je na spolucestujících.LOC znatelná únava či se v jejich očích lesknou slzy', 'jindy je na spolucestujících.LOC znatelná.PRED únava či se v jejich očích lesknou slzy'],
                         ['Domácí měli v dalším průběhu znatelnou převahu, ale jen s minimálním efektem.', 'Domácí měli v dalším průběhu znatelnou.PRED převahu, ale jen s minimálním efektem.']],
}


def iterate_over_examples(examples: list, testlemmas: list, testreflexives: list, lu_id: str, lu_pos: str) -> None:
    for i in range(len(examples)):
        example = str(examples[i])
        if 'mark_PRED BYLO ZDE' in example:
            continue
        cleanExample = example.replace('.PRED_NEG', '').replace('.PRED_SE', '').replace('.PRED_SI', '').replace('.PRED', '')
        parsedExample = call_morphodita(cleanExample, splitSentences=False)
        adjustedExample = mark_lemma_in_parsed_text(parsedExample, testlemmas, testreflexives, lu_pos)
        if lu_id in known_errors.keys():
            for change in known_errors[lu_id]:
                adjustedExample = adjustedExample.replace(change[0], change[1])
        if '.PRED' in example and adjustedExample != example:
            examples[i] = (Text(txt=adjustedExample + ";\n        " + 'mark_PRED BYLO ZDE: ' + str(examples[i]), comments=examples[i].comments))
        else:
            examples[i] = Text(txt=adjustedExample, comments=examples[i].comments)


@changes('examplerich')
def transform_lu_mark_PRED(lu):
    if ('pos' not in lu.dynamic_attribs
            or not lu.dynamic_attribs['pos']._data in ('stem noun', 'root noun', 'adjective')):
        log("mark_PRED", logging.WARNING, "mark_PRED expects only nouns and adjectives:", lu._id)
        raise TestDoesNotApply

    try:
        examplerich = lu.attribs['examplerich']._data
    except Exception as ex:
        log("mark_PRED", logging.DEBUG, "can only be applied if the LU contains an examplerich attribute:", lu._id)
        raise TestDoesNotApply
    log("mark_PRED", logging.DEBUG, "the original value of the examplerich attribute of", lu._id, " is:\n", str(lu.attribs['examplerich']))

    try:
        testlemmas = lu.lemma.lemma_set(discern_homo=False, noiter=False, treat_reflex='remove')
    except Exception as ex:
        log("mark_PRED", logging.ERROR, "computation of lemmas failed for", lu._id)
    try:
        testreflexives = lu.lemma.reflexives(include_optional=True)
    except Exception as ex:
        log("mark_PRED", logging.ERROR, "computation of reflexives failed for", lu._id)

    for exs in examplerich.values():
        if isinstance(exs, list):
            exs = [exs]
        else:
            exs = exs.values()
        for examples in exs:
            iterate_over_examples(examples, testlemmas, testreflexives, lu._id, lu.dynamic_attribs['pos']._data)

    log("mark_PRED", logging.DEBUG, "the resulting value of the examplerich attribute of", lu._id, " is:\n ", str(lu.attribs['examplerich']))
