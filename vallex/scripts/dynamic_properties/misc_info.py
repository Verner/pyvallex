import logging
from vallex.log import log
from vallex.scripts import requires
from vallex.scripts.tests.noun_tests import derived_references_exceptions_lexeme  # type: ignore
import sys
import os  # for better debugging

import re

from vallex import Attrib, Valdiff

PDTVALLEX_POS_MAPPING = {
    'V':  'verb',
    'N':  'stem noun',
    'Nx': 'root noun',
    'A':  'adjective',
    'D':  'adverb'
}


def compute_pos(lu):
    attr = Attrib('pos', dynamic=True, help='Detailed part of speech (verb / stem noun / root noun / adjective / unknown)')
    attr._data = 'unknown'
    if 'blu-v' in lu._id:
        attr._data = 'verb'
    if 'blu-a' in lu._id:
        attr._data = 'adjective'
    elif 'blu-n' in lu._id:
        attr._data = 'stem noun'
        for var in ['', '1', '2', '3', '4']:
            if 'no-aspect'+var in lu.lemma._data.keys():
                attr._data = 'root noun'
                break
    elif 'v-w' in lu._id:
          # next(iter(dictionary)) gets a key from that dict
          # we rely on the fact that in pdt-vallex, the lu has a single lemma, so there is only one key
        attr._data = PDTVALLEX_POS_MAPPING[next(iter(lu.lemma._data))]
    lu.dynamic_attribs[attr.name] = attr


REFLEXIVE_REGEX = re.compile(r'.*\s+\bs[ei]\d?\b\s*$')
"""Regex for recognizing reflexive lemmas."""
OPT_REFLEXIVE_REGEX = re.compile(r'.*\s+\(s[ei]\d?\)\s*$')
"""Regex for recognizing optionally reflexive lemmas."""


def compute_isReflexive(lu):
    attr = Attrib('isReflexive', dynamic=True, help='Is the lemma reflexive (always/optionally/never)?')
    if sum(1 for val in lu.lemma._data.values() if REFLEXIVE_REGEX.match(val)) > 0:
        attr._data = 'always'
    elif sum(1 for val in lu.lemma._data.values() if OPT_REFLEXIVE_REGEX.match(val)) > 0:
        attr._data = 'optionally'
    else:
        attr._data = 'never'
    lu.dynamic_attribs[attr.name] = attr


@requires('lumap')
def compute_valdiffCurrent(lu, lumap):
    """
        Computes the value of the valdiff attribute for a lexical unit,
        taking into account all lus linked in the derivedFrom attribute.
    """
    if 'pos' not in lu.dynamic_attribs or not lu.dynamic_attribs['pos']._data in ('stem noun', 'root noun', 'adjective'):
        return
    if 'derivedFrom' not in lu.attribs or lu.attribs['derivedFrom']._data['ids'] == [] or \
            lu.attribs['derivedFrom']._data['ids'][0]._id == 'Vallex-no':
        return
    source_id = lu.attribs['derivedFrom']._data['ids'][0]._id
    if source_id in lumap['lexemes'] and lu._id in derived_references_exceptions_lexeme:
        return
    try:
        source_lu = lumap['lus'][source_id]
    except Exception as ex:
        log("compute_valdiffCurrent", logging.WARN, "error adding valdiffCurrent to",  lu._id, "failed for:", ex)
        return
    try:
        if lu._id in ['blu-n-velení-1', 'blu-n-velení-3', 'blu-n-vyučování-vyučení-1', 'blu-n-výuka-1']:
            systemic_changes = {x: Valdiff.SYSTEMIC_CHANGES_VERB_NOUN[x] for x in Valdiff.SYSTEMIC_CHANGES_VERB_NOUN if x != '3'}   # in those two LUs, 3>2,pos should not be treated as a typical change
        elif lu.dynamic_attribs['pos']._data == 'adjective':
            systemic_changes = Valdiff.SYSTEMIC_CHANGES_VERB_ADJECTIVE
        else:
            systemic_changes = Valdiff.SYSTEMIC_CHANGES_VERB_NOUN
    except Exception as ex:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        log("compute_valdiffCurrent", logging.ERROR, "computing changeset for ", lu._id, " resulted in exception of ", exc_type, "in file", fname, "at line", exc_tb.tb_lineno)

    try:
        ret = lu.frame.diff(source_lu.frame, systemic_changes=systemic_changes)  # type: Valdiff
        ret.name = 'valdiffCurrent'
    except Exception as ex:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        log("compute_valdiffCurrent", logging.ERROR, "computing valdiffCurrent for ", lu._id, " and the first source source_lu ",
            source_lu._id, " resulted in exception of ", exc_type, "in file", fname, "at line", exc_tb.tb_lineno)

    if len(lu.attribs['derivedFrom']._data['ids']) > 1:
        for source_lu_ref in lu.attribs['derivedFrom']._data['ids'][1:]:
            try:
                source_lu = lumap['lus'][source_lu_ref._id]
            except Exception as ex:
                log("compute_valdiffCurrent", logging.WARN, "error adding later value of derivedFrom to valdiffCurrent of ", lu._id, " with derivedFrom=", str(lu.attribs['derivedFrom']._data), " failed with: ", ex)
                # FIXME raise TestFailed

            try:
                valdiff = lu.frame.diff(source_lu.frame, systemic_changes=systemic_changes)

                for slot in valdiff._data:
                    list_slots = [i for i, oldslot in enumerate(ret._data) if oldslot.functor == slot.functor]
                    if slot.spec == "-" and len(list_slots) == 0:
                        ret._data.append(slot)
                    elif len(list_slots) != 1:
                        log("compute_valdiffCurrent", logging.ERROR, "there is not a unique slot with the", slot.functor, "functor:", ' '.join([ret._data[i] for i in list_slots]))
                    else:
                        list_slots = [i for i, oldslot in enumerate(ret._data) if oldslot.functor == slot.functor]
                        if len(list_slots) != 1:
                            log("compute_valdiffCurrent", logging.ERROR, "there is not a unique slot with the", slot.functor, "functor: ", ' '.join([ret._data[i] for i in list_slots]))
                        else:
                            old_index = list_slots[0]
                            if slot.spec == '=':
                                ret._data[old_index].spec = '='  # even if it was added relative to the first source_lu

                            for form in slot.forms_eq:  # it is in the noun frame -> it was either eq or added
                                if form in ret._data[old_index].forms_add:
                                    ret._data[old_index].forms_eq.append(form)
                                    # ret._data[old_index].forms_add.remove(form) would remove form also from the frame of the lu, because Frame.diff does not contain a deep copy of the elements...
                                    ret._data[old_index].forms_add = [x for x in ret._data[old_index].forms_add if x != form]
                            for form in slot.forms_del:  # it is not in the noun frame -> it either was not in valdiff or is already in forms_del
                                if not form in ret._data[old_index].forms_del:
                                    ret._data[old_index].forms_del.append(form)
                                    # we do not need to do anything -- the form is either already added, or even eq

            except Exception as ex:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                log("compute_valdiffCurrent", logging.ERROR, "computing valdiff between ", lu._id, " and additional source source_lu ",
                    source_lu._id, " resulted in exception of ", exc_type, "in file", fname, "at line", exc_tb.tb_lineno)

    lu.dynamic_attribs['valdiffCurrent'] = ret  # type: Valdiff
