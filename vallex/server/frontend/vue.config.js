// Returns the path part of an url (i.e. http://test.com:81/auto => /auto)
function strip_server(url) {
  if (url.startsWith('http')) {
      if (url.indexOf('/',8) > -1) {
        return url.slice(url.indexOf('/', 8))
      } else {
          return ''
      }
  }
  else return url
}

// Get git version info and populate the env vars with it so that
// it can be read in src/config.js
let fs = require('fs')
const version_path = '../../__version__'
let [git_hash, git_tag, git_date] = fs.existsSync(version_path) ? 
      fs.readFileSync(version_path, 'utf8').trim().split('\n') :
      ['Unknown', 'Unknown', 'Unknown']

process.env.VUE_APP_GIT_HASH = git_hash
process.env.VUE_APP_GIT_TAG = git_tag
process.env.VUE_APP_GIT_DATE = git_date

module.exports = {
  publicPath: strip_server(process.env.VUE_APP_BASE_URL),
  assetsDir: 'static',
  outputDir: process.env.OUTPUT_DIR || 'dist',

// see https://webpack.js.org/configuration/dev-server/
  devServer: {

// Our version of the dev server doesn't support the client object
//     client: {
      overlay: true,
      progress: true,
//     },

// Doesn't seem to work in our version of the dev server
//     onListening: function (devServer) {
//         var process = require('process');
//         if (!devServer) {
//             throw new Error('webpack-dev-server is not defined');
//         }
//
//         const port = devServer.server.address().port;
//         console.log('Listening on port:', port);
//         console.log(`This process is pid ${process.pid}`);
//         if (process.pid) {
//             console.log('This process is your pid ' + process.pid);
//         }
//     },
    proxy: {
      /* Send all requests to /api to the pyvallex backend server which will
       * be running on a different port during development */
      '/api': {
          target: process.env.BACKEND_URL,
      },
    },

// Neither does our version support the following options
//  setupExitSignals: true, // Exit on sigint and sigterm
//  watch: { usePolling: true } // to disable inotify ?
  }
}
