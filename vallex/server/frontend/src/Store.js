import api from "./Api"
import LogRocket from "logrocket"
import config from "./config.js"

const store = {
    state: {
        /** Histogram form state */
        histogram: {
            key: null /** The currently selected histogram key */,
            selector: null /** The optional regex for selecting the value to count */,
            computed: {
                /** The computed histograms */
                occurrences: { bins: {}, total: 1 },
                unique_lus: { bins: {}, total: 1 },
            },
            computing: false,
        },

        /** Frontend Version info &c. */
        frontend_info: {
            short_title: config.app.short_title /** The application title (e.g. NomVallex) */,
            title:
                process.env
                    .VUE_APP_TITLE /** The application title (e.g. NomVallex) */,
            tag: config.app.git_tag /** The latest tag from which the frontend was built */,
            hash: config.app.git_hash /** The git hash of the commit from which the frontend was built */,
            date: config.app.git_date /** The date of the commit from which the frontend was built */,
        },

        backend_info: {
            tag: null /** The latest tag that the backend code is running. */,
            hash: null /** The git hash of the commit that the backend code is running */,
            date: null /** The date of the commit that the backend code is running */,
            vallex_version: [
                "Unknown",
                "Unknown",
            ] /** The version of the lexicon data */,
        },

        /** An array of conditions, each being string of the form `key=regex` */
        current_query: [],

        /** Indicates whether a search is currently in progress */
        searching: false,

        /** The current search results (a list of lexeme ids) */
        search_result_ids: [],

        /** The current search results statistics */
        search_stats: {},

        /** The global lexicon statistics */
        lexicon_stats: {},

        /** Indicates whether the initial data from the server is loaded */
        api_loaded: false,

        /** A list of the source-file names to search in */
        search_in: [],

        /** When we last asked the server for an update */
        last_update: Date.now() / 1000,

        /** Frontend Configuration loaded from the server */
        config: {},

        /** A list of messages, which can be displayed to the user.*/
        messages: [],

        /** The online help */
        help: {},

        /** TOC for the document browser & online help */
        doc_toc: {},

        logged_in: null,

        /** GUI State (e.g. whether certain dialogs are hidden or shown, ...) */
        gui: {
            /** Various display settings */
            show_chrome: true /* Allows hiding all control elements when printing */,
            show_source: false /* Whether to show formatted lus or lu source */,
            show_all_lus: false /* Whether to show only matched lus or all (in a lexeme with a matched lu) */,
            result_lexemes_per_page: 10 /* Number of lexemes to show on a single page*/,

            /** The following values control the showing/hiding of dialogs */
            display_settings_dlg: false,
            stats_dlg: false,
            login_dlg: false,
            match_map_dlg: false,
            help_dlg: false,
            doc_browser: false,
            lu_reference_dlg: false,
            lu_errors_dlg: false,
            lu_errors_dlg_type: 'error' /** The current color for the lu_errors_dlg dialog, either 'error' or 'warning' */,
            comments_dlg: false,
            about_dlg: false,
            busy_dlg: false,

            current_help_topic: undefined /** The current help topic displayed in the help dialog */,
            current_lu: undefined /** The current lexical unit opened in the editor for changes */,
            current_reference_lu: undefined /** The current referenced lu shown in the lu_reference_dlg */,
            current_matchmap_lu: undefined /** The current lu shown in the match map dlg */,
            current_validation_errors: undefined /** The current validation errors shown in the errors_dlg */,
            current_comments: undefined /** The current comments to show in the comments dialog */,

            search_in_progress: false /** Whether we are currently waiting for a search on the server to finish */,

            /** Used in app initialization to show progress */
            loading: {
                what: "",
                progress: 0,
            },

            /** Used to show transient notifications  */
            notification: {
                show: false /** A notification is in progress */,
                text: "" /** The text of the notification */,
                timeout: 3000 /** The duration before the notification is dismissed */,
            },
        },
        perma_link: (target) => {
            const base_url = window.location.origin + window.location.pathname
            if (target == "search") {
                return (
                    base_url +
                    "#search:" +
                    encodeURI(
                        JSON.stringify({
                            current_query: store.state.current_query,
                            search_in: store.state.search_in,
                        })
                    )
                )
            } else if (target == "histogram") {
                return (
                    base_url +
                    "#histogram:" +
                    encodeURI(
                        JSON.stringify({
                            current_query: store.state.current_query,
                            search_in: store.state.search_in,
                            histogram: store.state.histogram,
                        })
                    )
                )
            }
        },
        bug_url: () => {
            const base_url = "https://gitlab.com/Verner/pyvallex/issues/new?"
            const desc =
                "Describe the bug here\n\n************\n" +
                (store.state.frontend_info.hash == store.state.backend_info.hash
                    ? "Git Hash: " + store.state.frontend_info.hash.slice(0, 8)
                    : "Frontend Git Hash: " +
                    store.state.frontend_info.hash.slice(0, 8) +
                    " Backend Git Hash: " +
                    store.state.backend_info.hash.slice(0, 8)) +
                "\nVallex Data Rev.: " +
                store.state.backend_info.vallex_version[0] +
                "\n[LogRocket Session](" +
                LogRocket.sessionURL +
                ")" +
                "\n[Current Query](" +
                store.state.perma_link("search") +
                ")"
            return (
                base_url +
                encodeURI("issue[description]=" + desc).replace("#", "%23")
            )
        },
        /** Returns the url pointing to a repository with either frontend, backend code or lexicon data
         *  depending on the what the `type` parameter specifies.
         *
         *  @param {string} type determines which repo url to return, can be one of `frontend`, `backend` or `vallex`*/
        repo_url: (type) => {
            if (type == "frontend")
                return (
                    process.env.VUE_APP_CODE_REPO +
                    "/" +
                    store.state.frontend_info.hash
                )
            if (type == "backend")
                return (
                    process.env.VUE_APP_CODE_REPO +
                    "/" +
                    store.state.backend_info.hash
                )
            const vallex_repo_base_url =
                "https://svn.ms.mff.cuni.cz/trac/vallex/browser/trunk/aktualni_data/data-txt?rev="
            return (
                vallex_repo_base_url +
                store.state.backend_info.vallex_version[0]
            )
        },
    },
    mutations: {
        /** Refines the current search query by adding another condition
         *
         * @param {string} q the condition to add (a `key=regex` pair) */
        restrictQuery: (q) =>
            (store.state.current_query = store.state.current_query.concat(q)),

        /** Removes conditions from the current search query
         *
         *  @param {number} start the first condition to remove
         *  @param {number} stop the last condition to remove */
        sliceQuery: (start, stop) =>
        (store.state.current_query = store.state.current_query.slice(
            start,
            stop
        )),
    },
    actions: {
        /** Hide chrome */
        async hide_chrome() {
            store.state.gui.show_chrome = false
        },
        /** Show chrome */
        async show_chrome() {
            store.state.gui.show_chrome = true
        },
        /** Toggle LU source view */
        async toggle_sources_display() {
            store.state.gui.show_source = !store.state.gui.show_source
        },
        /** Toggle display all lus from matched lexeme */
        async toggle_show_all_lus() {
            store.state.gui.show_all_lus = !store.state.gui.show_all_lus
        },
        /** Set Result Pagination
         *
         *  @param {int} count ... the number of lexemes to show on a single page; if undefined
         *                         shows all the results on a single page
         */
        async set_result_pagination(count) {
            if (count) store.state.gui.result_lexemes_per_page = count
            else
                store.state.gui.result_lexemes_per_page =
                    store.state.search_result_ids.length
        },
        /** Show the document browser */
        async show_docs() {
            store.state.gui.doc_browser = true
        },
        /**  Show the  help dialog with a given topic */
        async show_help(topic) {
            if (!store.state.help[topic]) {
                try {
                    store.state.help[topic] = await api.get_help(topic)
                } catch (ex) {
                    store.actions.exception(
                        ex,
                        "Unable to load help for " + topic
                    )
                    return
                }
            }
            store.state.gui.current_help_topic = topic
            store.state.gui.help_dlg = true
        },
        /** Shows the reference dialog with an lu
         *
         *  @param {string} lexical_unit_id  the id of the lexical unit to show */
        async show_reference(lexical_unit_id) {
            store.state.gui.current_reference_lu = await api.getLUs(
                lexical_unit_id
            )
            store.state.gui.lu_reference_dlg = true
        },
        /** Shows the data validation errors/warnings dialog.
         *
         *  @param {Array[{String, Array[String]}]} errors  the errors to show */
        async show_errors(errors, type) {
            store.state.gui.current_validation_errors = errors
            store.state.gui.lu_errors_dlg_type = type
            store.state.gui.lu_errors_dlg = true
        },
        /** Shows the match map dialog.
         *
         *  @param {Object} lu  the lexical unit whose match-map should be shown */
        async show_match_map(lu) {
            store.state.gui.current_matchmap_lu = lu
            store.state.gui.match_map_dlg = true
        },
        /** Shows the comments dialog.
         *
         *  @param {Array[Object]} comments the comments to show in the comments dialog */
        async show_comments(comments) {
            store.state.gui.current_comments = comments
            store.state.gui.comments_dlg = true
        },
        /** Shows the about dialog.*/
        async show_about() {
            store.state.gui.about_dlg = true
        },
        /** Shows the stats dialog. */
        async show_stats() {
            store.state.gui.stats_dlg = true
        },
        /** Shows the login dialog. */
        async show_login() {
            store.state.gui.login_dlg = true
        },
        /** Shows the display settings dialog. */
        async show_display_settings() {
            store.state.gui.display_settings_dlg = true
        },
        /** Shows the busy dialog. */
        async show_busy_dialog() {
            store.state.gui.busy_dlg = true
        },
        /** Hides the busy dialog. */
        async hide_busy_dialog() {
            store.state.gui.busy_dlg = false
        },

        /** Performs a search on the server using the current query */
        async search() {
            const state = store.state
            const query = state.current_query.concat(
                state.search_in.length > 0
                    ? ["lexicon=" + state.search_in.map(
                        // Need to escape special characters in the path,
                        // especially the Windows directory separator '\'
                        (lx_path) => lx_path.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&')
                    ).join("|")
                    ]
                    : []
            )
            try {
                state.gui.search_in_progress = true
                const [lex_ids, stats] = await api.search(query)
                state.search_result_ids = lex_ids
                state.search_stats = stats
            } finally {
                state.gui.search_in_progress = false
            }
        },

        /** Performs a search on the server using the current query */
        async computeHistogram() {
            const state = store.state
            try {
                state.histogram.computing = true
                state.histogram.computed = await api.computeHistogram(
                    state.current_query,
                    state.histogram.key,
                    state.histogram.selector
                )
            } finally {
                state.histogram.computing = false
            }
        },

        /** Logs an error message to show the user.
         *
         * @param {bool} transient  Whether to show as a permanent (dismissable) message at the top
         *                          of the screen or to show as a transient notification at the bottom.
         */
        error(options) {
            const msg = Array.prototype.slice.call(arguments).join(" ")
            if (options && options.transient) {
                store.actions.notify(msg)
            } else
                store.state.messages.push({
                    type: "error",
                    message: msg,
                    unread: true,
                    opts: options,
                })
        },

        /** Shows a persistent message to the user.
         *
         * @param {String} message the message to show
         * @param {Object} options an options objec; The keys of the options object
         * are interpreted as follows:
         *     type {String}                   ... used to provide a css class to the message
         *                                         (defaults to 'info')
         *     actions {List[{title, action}]} ... a list of available actions; will be displayed
         *                                         as buttons with title `title` that, when
         *                                         clicked, will run the function specified by `action`
         */
        show_message(message, options) {
            store.state.messages.push({
                type: options.type ? options.type : "info",
                message: message,
                unread: true,
                opts: options,
            })
        },

        /** Logs an error from an exception. */
        exception(ex, message, options) {
            let msg = ""
            if (ex.response && ex.response.data && ex.response.error)
                msg = message
                    ? message + ": " + ex.response.data.error
                    : ex.response.data.error
            else msg = message ? message + ": " + ex.message : ex.message

            if (options && options.transient) store.actions.notify(msg)
            else
                store.state.messages.push({
                    type: "error",
                    message: msg,
                    unread: true,
                    opts: options,
                })
        },

        notify(text, timeout) {
            store.state.gui.notification.text = text
            store.state.gui.notification.show = true
            if (timeout) store.state.notification.timeout = timeout
        },
    },
}

export default store
