const conditions = [
  ["vallexClusterLemma", /(^[*] [^[]*[^[ ])/mg],
  ["vallexAttribute", /(^\s+-(\w|-)+:)/mg],
  ["aspect", /((?<![~].*)\b((biasp|impf|pf|iter|no-aspect)[1234]?|(V|Nx?|A|D))[:])/g],
  ["id", /((?<!: id:.*)blu-[vn]-\S*|v-w\S*|(Nom)?Vallex-no|no\s*$|PDT-Vallex-no)/g],
  ["vallexVerbLemma", /(^\s*: id: blu-v-.*\n.*[~] .*$)/mg],
  ["vallexNounLemma", /(^\s*: id: blu-n-.*\n.*[~] .*$)/mg],
  ["PDTvallexLemma", /(^\s*: id: v-w.*[~] .*$)/mg],
  ["vallexFrame", /(^\s+[+]i? .*$)/mg],
  ["attrValue", /((ACMP|ACT|ADDR|AIM|APP|AUTH|BEN|CAUS|COMPL|COND|CPHR|CPR|CRIT|DIFF|DIR[123]?|DPHR|EFF|EXT|HER|ID|INTF|INTT|LOC|MANN|MAT|MEANS|NORM|OBST|ORIG|PAT|RCMP|REG|RESL|RESTR|RSTR|SUBS|TFHL|TFRWH|THL|THO|TOWH|TPAR|TSIN|TTILL|TWHEN)(-(ACMP|ACT|ADDR|AIM|APP|AUTH|BEN|CAUS|COMPL|COND|CPHR|CPR|CRIT|DIFF|DIR[123]?|DPHR|EFF|EXT|HER|ID|INTF|INTT|LOC|MANN|MAT|MEANS|NORM|OBST|ORIG|PAT|RCMP|REG|RESL|RESTR|RSTR|SUBS|TFHL|TFRWH|THL|THO|TOWH|TPAR|TSIN|TTILL|TWHEN)){1,2})/g],
  ["attrValue", /(NONE|(\+?(ACMP|ACT|ADDR|AIM|APP|AUTH|BEN|CAUS|COMPL|COND|CPHR|CPR|CRIT|DIFF|DIR[123]?|DPHR|EFF|EXT|HER|ID|INTF|INTT|LOC|MANN|MAT|MEANS|NORM|OBST|ORIG|PAT|RCMP|REG|RESL|RESTR|RSTR|SUBS|TFHL|TFRWH|THL|THO|TOWH|TPAR|TSIN|TTILL|TWHEN))+(\([^)]*\)[?]?)?:)/g],
  ["functor", /((?<!\s+\+.*|-)\.?(PRED(_S[EI]|_NEG)?|ACMP|ACT|ADDR|AIM|APP|AUTH|BEN|CAUS|COMPL|COND|CPHR|CPR|CRIT|DIFF|DIR[123]?|DPHR|EFF|EXT|HER|ID|INTF|INTT|LOC|MANN|MAT|MEANS|NORM|OBST|ORIG|PAT|RCMP|REG|RESL|RESTR|RSTR|SUBS|TFHL|TFRWH|THL|THO|TOWH|TPAR|TSIN|TTILL|TWHEN)(?![123+:-]))/g],
  ["functor", /(\.(ACMP|ACT|ADDR|AIM|APP|AUTH|BEN|CAUS|COMPL|COND|CPHR|CPR|CRIT|DIFF|DIR[123]?|DPHR|EFF|EXT|HER|ID|INTF|INTT|LOC|MANN|MAT|MEANS|NORM|OBST|ORIG|PAT|RCMP|REG|RESL|RESTR|RSTR|SUBS|TFHL|TFRWH|THL|THO|TOWH|TPAR|TSIN|TTILL|TWHEN)(-(ACMP|ACT|ADDR|AIM|APP|AUTH|BEN|CAUS|COMPL|COND|CPHR|CPR|CRIT|DIFF|DIR[123]?|DPHR|EFF|EXT|HER|ID|INTF|INTT|LOC|MANN|MAT|MEANS|NORM|OBST|ORIG|PAT|RCMP|REG|RESL|RESTR|RSTR|SUBS|TFHL|TFRWH|THL|THO|TOWH|TPAR|TSIN|TTILL|TWHEN)){1,2})/g],
  ["attrValue", /(appoint noun|appoint verb|cause motion|change|combining|communication|contact|emission|exchange|expansion|extent|intervention|location|mental action|modal verb|motion|perception|phase of action|phase verb|providing|psych state|psych verb|social interaction|transport)/g],
  ["attrValue", /(no_recipient|no_poss-result|no_passive|no_deagent0?)/g],
  ["attrValue", /(recipient(-dostat|-dostávat)?|poss-result(-conv|-nconv)?(-mít|-mívat)?|passive(-být|-bývat)?|verbonominal-(subj|obj|exper)(-být|-bývat)?|deagent(0)?|coref[34])/g],
  ["attrValue", /((II|I): (Inference|Information|Path|Phenomenon|Stimulus|Affliction-Patient|Bearer_of_action-Location|Hole-Affected_object|Locatum-Location( \((destruction|destruction person|formation|formation person|objectless)\))?|Material-Product|Substance-Source|Theme-Path( \(objectless\))?|Theme_1-Theme_2|Area|Boundary|Goal|Impactee|Judgment|Location|Path|Source|State( (objectless))?|Subject_matter( (objectless))?))/g],
  ["attrValue", /(tantum|derived-nonspecific|derived-decaus_(theme|patient_PAT|patient_ADDR|phenomenon[12]|CPHR)|derived-autocaus_(patient_PAT|patient_ADDR|recipient)|derived-partobject_(PAT|EFF|DPHR)|derived-recipr_(patient_(PAT_PAT|PAT_ADDR)|recipient_(ADDR_ADDR|PAT_PAT|ADDR_PAT))|derived-conv_(bearer-of-action_location|donor_recipient_ACT_(ADDR_ORIG|PAT_PAT)|experiencer_stimulus|locatum_location[12]|speaker_recipient|subject_object_(PAT|ORIG)|substance_source)|derived-quasiconv1|derived-deaccus[1-2])/g],
  ["attrValue", /(událost|událost \/ abstraktní výsledek děje|abstraktní výsledek děje|abstraktní výsledek děje \/ substance|substance|vlastnost|kontejner|jiné)/g],
  ["comment", /(#.*$)/mg]
]
export default conditions 

