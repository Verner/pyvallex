Each file in this directory corresponds to a single [Vue component](https://vuejs.org/v2/guide/components.html).
Roughly speaking, a component is a custom html element. For example the file [Lexeme.vue](./Lexeme.vue) defines the custom
html element `<Lexeme/>` which is used to display lexemes, e.g.:

```
<Lexeme :lexeme='props.item' />
```

Each component consists of three parts:

  1. Template
  2. Javascript logic
  3. Styling

### Template block
The template is contained in a `<template></template>` block and
this defines the html code of the custom element, i.e. the custom
element will be replaced with the html code inside the template block.
The template block needs to have exactly one root html element, everything else must be a child of this root element. For more info, see the [template syntax](https://vuejs.org/v2/guide/syntax.html).

### Javascript logic
Here one defines javascript functions and data, which are available
for use in the template block. The logic is contained in a `<script></script>` block and should export a single object describing the component, e.g.
```
<script>
    export default {
       ...
    }
</script>
```

where `...` are properties describing the object. The most important
are:

  - the `data` property, which must be a function and should return
    an object which will be available to the template
  - the `props` property, is an array of attribute names which can be
    used to pass data to the component, e.g. the `Lexeme` component
    can (and, actually does) have a `lexeme` in its props and if one writes
```
   <Lexeme :lexeme='obj'>
```
    then `obj` is available to the component as `lexeme`.


### Styling
Styling is contained in a `<style scoped></style>` block and is just a
css-stylesheet (or [scss](https://sass-lang.com/) in case the block is
`<style lang='scss' scoped></style>`. The recommended optional `scoped` attribute makes the style apply only to the component and its children.
