import axios from "axios"
import * as Sentry from "@sentry/browser"

import Vue from "vue"
import "./plugins/vuetify"
import App from "./App.vue"


import "vue-material-design-icons/styles.css"
import "./scss/icons.scss"
import "./scss/utils.scss"
import "./scss/print.scss"

import config from "./config.js"


Vue.config.productionTip = false

if (config.debug.vue_perf) {
    Vue.config.performance = true;
}

if (config.debug.vue_devtools) {
    Vue.config.devtools = true;
}

new Vue({
    render: (h) => h(App),
}).$mount("#app")

if (config.debug.sentry.enabled) {
    Sentry.init({
        dsn: config.debug.sentry.dsn,
        integrations: [
            new Sentry.Integrations.Vue({
                Vue,
                attachProps: true,
            }),
            new Sentry.Integrations.ExtraErrorData({
                depth: 3, // default value
            })
        ],
        beforeSend: (event) => {
            axios.post(
                config.server + "/api/rpc/log",
                {
                    area: 'sentry',
                    level: 'ERROR',
                    message: event,
                },
            );
            return event;
        },
    })
}
