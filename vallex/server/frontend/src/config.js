export default {
    server: process.env.VUE_APP_BASE_URL,
    debug: {
        vue_perf: process.env.VUE_APP_ENABLE_PERF,
        vue_devtools: process.env.VUE_APP_ENABLE_DEV,
        sentry: {
            enabled: !process.env.VUE_APP_DISABLE_SENTRY,
            dsn: "https://6511a8ea7b8c4750a2b573af1f4e2ad2@sentry.io/1405151"
        },
        logrocket: {
            id: "k5gbta/pyvallex",
            enabled: !process.env.VUE_APP_DISABLE_LOGROCKET,
        }
    },
    app: {
        git_hash: process.env.VUE_APP_GIT_HASH,
        git_tag: process.env.VUE_APP_GIT_TAG,
        git_date: process.env.VUE_APP_GIT_DATE,
        short_title: process.env.VUE_SHORT_APP_TITLE || 'PyVallex',
        title: process.env.VUE_APP_TITLE || 'PyVallex Web Frontend',
        repo: 'https://gitlab.com/Verner/pyvallex/tree/',
    }
}
