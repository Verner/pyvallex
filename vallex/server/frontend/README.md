# Vallex Search Frontend

The Frontend is coded using the [Vue.js](https://vuejs.org/) framework
and UI components from the [Vuetify](https://vuetifyjs.com/en/) library (v1.5!). The directory layout is as follows:

### Project setup
First make sure you have [NodeJS](https://nodejs.org/en/) installed
and then run:

```
$ npm install -g yarn
$ yarn install
```

### Project Layout

##### Source files
  - `src` the actual source code
    + `src/assets`     images, fonts, etc.
    + `src/scss`       SASS style files
    + `src/components` VUE components
  - `public/*` some static files (favicon, images) + the `index.html` template; these are compiled/copied to the
    dist folder during compilation

The entry-point for the frontend application is located in [`src/App.vue`](./src/App.vue) (technically speaking the main entry point is
in `src/main.js`, but the interesting stuff happens in `App.vue`).
This file contains the main Vue component (see [`src/components/README.md`](src/components/README.md) for more info on components.).

##### Configuration files

  - `src/config.js` config values available to the frontend app (via `import config from "./config.js"`); some of them can also
    be set in the `.env.*` files (see below) or as environment variables when compiling the javascript files.
  - `.env.*` per-site settings
    + `.env.client` settings when the app is run inside the Qt GUI
    + `.env.server` settings when the app is deployed to a server
    + `.env.development` settings when the app is run in development

    The files contain `KEY=VALUE` pairs, one per line, with `#` indicating comments, the format
    is basically a `shell` restricted script which gives values to variables. The settings are read
    when the javascript source files are compiled (e.g. via `yarn run build`) and may be overriden
    by environment variables (e.g. `VUE_APP_DISABLE_SENTRY=true yarn run build`). The available
    settings currently are:

        BACKEND_URL                 # where the pyvallex backend is running
        VUE_APP_BASE_URL=           # the base path under which the app is served (e.g. /pyvallex, http://localhost:8111, ...)
        VUE_APP_DISABLE_SENTRY      # set to "true" to disable sending error logs to sentry.io
        VUE_APP_DISABLE_LOGROCKET   # set to "true" to disable sending error logs to logrocket.io
        VUE_APP_ENABLE_DEV          # set to "true" to enable vue development mode
        VUE_APP_ENABLE_PERF         # set to "true" to enable vue profiling mode
        VUE_APP_TITLE               # can be used to set the browser window title & app title in the about dialog
        OUTPUT_DIR                  # the directory (under valles/server/frontend) where compiled js files will be stored


  - `vue.config.js` this is read when compiling the javascript files; it makes the git version available to the app and also
    contains settings for the development web server

  - `webui-config.json` read by the pyvallex backend server and served under `/api/config` (this can be used to tweak the
    app without needing to recompile the javascript sources)

  - `package.json` a javascript equivalent of `pyproject.toml`; lists javascript libraries which are needed to compile the
    code, contains eslint configuration, definitions of scripts runnable via `yarn run`, and some other stuff


##### External libraries
  - `node_modules`: contains external libraries, installed using [yarn](https://yarnpkg.com/en/)
  - `src/plugins`: contains vue plugins ([vuetify.js](https://v15.vuetifyjs.com/en))

##### Compiled files
  - `dist-server`: compiled & minified frontend code (this contains the static main page (`index.html`) and the necessary javascript, image and style files files, which are served by the python server
  - `dist`: similar to `dist-server` compiled & minified frontend code (this contains the static main page (index.html) and the necessary javascript, image and style files files, which are served by the python server


### Running the app in production/from the Qt GUI
To run the app in the GUI or on a server, we first need to compile & minify the javascript (to make it small, so that users
don't have to download too much data). This is done by running:

```
$ yarn run build --mode client
```
or
```
$ yarn run build --mode server
```

The compiled files (under `dist-server` or `dist` for client) are then served either by an external
webserver (e.g. nginx, see the config files in `deploy/production`) or, in the case of the client, by
the pyvallex backend server (when running `vallex-cli gui`), which is configured to serve the files
from the `dist` directory under the `/static` path (e.g. http://localhost:8080/static).


### Running the app in development
Under development we don't want to use the compiled and minified files, which are basically unreadable. Also, we'd like
to be able to make changes to the source files and have them reflected immediately in the UI without reloading the pages.
This is typically done by running a special webserver for development purposes, which serves the js files in a readable form,
recompiles them when they change and tells the page to reload itself.

The problem with this setup is, that the server occupies a network port so it is inaccessible to the backend. Previously,
we have worked around this by having a config option which tells the app to search for the backend on a different port.
However, this lead to some problems with [CORS](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing).
Currently, we deal with this by telling the development webserver to proxy requests for urls starting with `/api` to
a different port, where the pyvallex backend is running. So, the app can remain ignorant of this detail and everything seems
to be located on a single address/port pair as far as the browser is concerned, solving the CORS problems. (This is actually
very similar to how its done in production, where the pyvallex backend only takes care of serving the api requests, while the
static files are served by nginx itself).

To make this work, we need to tell the dev server where the pyvallex backend is running. We do this in the `deploy/devel/run.sh`
script by first finding a free port for pyvallex to run on, start it and then running the dev server `vue-cli-service serve`
on a different port, passing the pyvallex backend address in an env variable.




### Running tests
```
$ yarn run test
```

### Lints and fixes files
```
$ yarn run lint
```
