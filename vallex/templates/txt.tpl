{% macro indent(x) %}{% for i in range(x) %} {% endfor %}{% endmacro %}
{% set separator=' ' %}
{% for lexicon in collection.lexicons if lexicon.lexemes %}
{% for c in lexicon._preamble %}
# {{ str(c) }}
{% endfor %}
{% for lex in lexicon.lexemes %}

* {{ lex._name }}{% for c in lex.comments %}{% if not loop.first %}{{ indent(len(lex._name) + 2) }}{% endif %} # {{ str(c) }}{% if not loop.last %}

{% endif %}{% endfor %}{{ '' }}
{% for lu in lex.lexical_units %}{% if not loop.first %}

{% endif %}
 : id: {{ lu._id }}{% for c in lu.comments %}{% if not loop.first %}{{ indent(len(lu._id) + 7) }}{% endif %} # {{ str(c) }}{% if not loop.last %}

{% endif %}{% endfor %}{{ '' }}
 ~ {% for k, v in lu.lemma._data.items() %}{{ k }}: {{ v.strip() }}{% if not loop.last %}{{ separator }}{% endif %}{% endfor %}{{ '' }}{% for c in lu.lemma.comments['all'] %} #{{ str(c) }}{% endfor %}{{ '' }}
 +{% if 'idiom' in lu.attribs %}i{% endif %}{% for f in lu.frame.elements %} {{ str(f) }}{% endfor %}{% for c in lu.frame.comments['all'] %} #{{ str(c) }}{% endfor %}{{ '' }}
  {% for attr in lu.attribs.values() %}{% if attr.duplicate %}{% set attr_name = attr.duplicate %}{% else %}{% set attr_name = attr.name %}{% endif %}
      {% if transformed_attrs and attr_name not in transformed_attrs and attr_name != 'idiom' %}
    {{ attr.src.strip() }}
      {% elif attr_name in ['examplerich', 'pdt-vallex'] or attr_name.startswith('example') %}
    -{{ attr_name }}: {{ attr.indented_str(len(attr_name)+6) }}
      {% elif attr_name.startswith('example') %}
    -{{ attr_name }}: {{ attr.indented_str(len(attr_name)+6) }}
      {% elif attr_name == 'synon' %}
      {% set val_indent = len(attr_name)+6 %}
      {% set show_aspects = len(attr._data.keys()) > 1%}
    -synon:
      {%- for aspect, groups in attr._data.items() -%}
        {%- if not loop.first %}{{ indent(val_indent) }}{% endif -%}
        {%- if show_aspects and aspect != 'all' %} {{ aspect }}: {{ indent(4-len(aspect)) }}{% else %}{{ indent(1) }}{% endif %}
              {%- for g in groups -%}{% for s in g %}{{ s }}{% if not loop.last %}, {% endif %}{% endfor %}{% if not loop.last %}; {% endif %}{% endfor %}{% if attr.comments[aspect] %}{% for c in attr.comments[aspect] %}{% if c %} #{{ str(c) }} {% endif  %}{% endfor %}{% endif %}{{ '' }}
          {% endfor %}
      {% elif attr_name.startswith('recipr') and len(attr_name)<=7 %}
        {% set val_indent = len(attr_name)+6 %}
    -{{ attr_name }}:
        {%- if 'all' in attr._data.keys() -%}
          {%- for spec, ex in attr._data['all'].items() -%}
            {%- if not loop.first %}{{ indent(val_indent+6) }}{% endif -%}
  {{ '' }} {{ spec }} {% for e in ex %}%{{e}}%{% if not loop.last %} {% endif %}{% endfor %}{{ '' }}
          {%- endfor -%}{% if 'all' in attr.comments %}{% for c in attr.comments['all'] %} #{{ str(c) }} {% endfor %}{% endif %}{{ '' }}
        {% endif %}
        {%- for aspect, val in attr._data.items()  -%}{%- if aspect != 'all' -%}
          {%- if not loop.first or 'all' in attr._data.keys() -%}{{ indent(val_indent) }}{%- endif -%}
  {{ '' }}{% if aspect != 'all' %} {{ aspect }}: {{ indent(4-len(aspect)) }}{% else %}{{ indent(1) }}{% endif %}
            {%- for spec, ex in val.items() -%}
              {%- if not loop.first %}{{ indent(val_indent+6) }}{% endif -%}
  {{ '' }}{{ spec }} {% for e in ex %}%{{e}}%{% if not loop.last %} {% endif %}{% endfor %}{{ '' }}
            {%- endfor -%}{% if attr.comments[aspect] %}{% for c in attr.comments[aspect] %}{% if c %} #{{ str(c) }} {% endif  %}{% endfor %}{% endif %}{{ '' }}
        {% endif %}{% endfor %}
      {% elif 'lvc' in attr_name or 'derivedLUs' in attr_name or 'derivedFrom' in attr_name or attr_name == 'ref' %}
    -{{ attr_name }}: {% for ref in attr._data['ids'] %}{{ ref._id }}{% if not loop.last %}{{ separator }}{% endif %}{% endfor %}{% if len(attr._data['lemmas']) > 0 %}|{% for l in attr._data['lemmas']%} {{ l }}{% endfor %}{% endif %}{% for c in attr.comments['all'] %}{{'\n'}}
                        #{{ str(c) }} {% endfor %}{{ '' }}
      {% elif 'reflexverb' in attr_name %}
    -{{ attr_name }}: {{ str(attr) }}
      {% elif attr_name == 'note' %}{% for note in attr._data %}
    -{{ attr_name }}: {{ note }}
      {% endfor %}
      {% elif attr_name == 'valdiff' or attr_name == 'valdiffNew' %}
    -{{ attr_name }}: {{ str(attr) }}{% for c in attr.comments['all'] %} #{{ str(c) }}{% if not '\n' in str(c) -%}{{ '\n' }}{% endif %}{% endfor %}{{ '' }}
      {% elif attr_name in ['class', 'note', 'semcategory', 'status', 'otherforms', 'specval', 'use', 'control', 'diat', 'split', 'conv', 'multiple', 'reflex'] or 'map' in attr_name or 'instig' in attr_name or 'reciprevent' in attr_name or 'reciprverb' in attr_name  %}
    -{{ attr_name }}: {% for i in attr._data %}{{ str(i) }}{% if not '\n' in str(i) -%}{{ '\n' }}{% endif %}{% endfor %}{% for c in attr.comments['all'] %} #{{ str(c) }}{% if not '\n' in str(c) -%}{{ '\n' }}{% endif %}{% endfor %}{{ '' }}
      {% elif attr_name != 'idiom' %}
    -{{ attr_name }}: {{ str(attr._data) }}{% for c in attr.comments['all'] %} #{{ str(c) }}{% if not '\n' in str(c) -%}{{ '\n' }}{% endif %}{% endfor %}{{ '' }}
    {% endif %}
  {% endfor %}
{% endfor %}
{% endfor %}
{% endfor %}
