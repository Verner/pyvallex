from vallex.scripts import TestFailed, TestDoesNotApply, TestWarning, provides, requires


@provides('fake_data')
def fake(config, collection):
    return {'cfg': config, 'coll': collection}


@requires('fake_data')
def test_lu_fake_test(lu, fake_data):
    if not fake_data['coll'].id2lu(lu._id) == lu:
        raise TestFailed("FakeTest")


def test_lu_skip(lu):
    raise TestDoesNotApply


def test_lu_pass(lu):
    return "pass"


def test_lu_warning(lu):
    raise TestWarning("warning")


def test_lu_fail(lu):
    raise TestFailed("fail")


def test_lu_error(lu):
    return 1/0
