from configparser import ConfigParser
from vallex.data_structures import LexiconCollection, LexicalUnit
from vallex import Valdiff
from vallex.txt_parser import parse, parse_valdiff
from vallex.txt_tokenizer import TokenStream

from vallex.scripts.utils import construct_lumap
from vallex.scripts.dynamic_properties.misc_info import compute_pos, compute_valdiffCurrent  # type: ignore
from vallex.scripts.transforms.add_valdiff import transform_lu_add_valdiff  # type: ignore

SRC = """
* PODEZÍRANÝ, PODEZŘÍVANÝ
 : id: blu-a-podezíraný-podezřívaný-1
 ~ impf1: podezíraný impf2: podezřívaný
  + ACT(7,od+2;obl) PAT(z+2,zda,že;obl) ADDR(||;obl)
    -derivedFrom: blu-v-podezírat-podezřívat-1
    #-valdiff: =ACT(>:1->7,od+2) =PAT(=:z+2,zda,že) =ADDR(+:||;-:4)

* PODEZÍRAT/PODEZŘÍVAT
 : id: blu-v-podezírat-podezřívat-1
 ~ impf: podezírat/podezřívat
  + ACT(1;obl) ADDR(4;obl) PAT(z+2,zda,že;obl)

* PODEZÍRAVÝ, PODEZŘÍVAVÝ
 : id: blu-a-podezíravý-podezřívavý-1
 ~ no-aspect1: podezíravý no-aspect2: podezřívavý
  + ADDR(k+3;obl) PAT(zda,že;obl) ACT(||;obl)
    -derivedFrom: blu-v-podezírat-podezřívat-1
    -valdiff: =ADDR(+:k+3;-:4) =PAT(=:že;-:z+2,zda) =ACT(+:||;-:1)
    #-valdiffCurrent: =ADDR(+:k+3;-:4) =PAT(=:zda,že;-:z+2) =ACT(+:||;-:1)

* PODEZŘELOST
 : id: blu-n-podezřelost-1
 ~ no-aspect: podezřelost
  + ADDR(2,pos;obl)
    -derivedFrom: blu-a-podezřelý-2
    -valdiff: =ADDR(+:2,pos;-:||) -ACT(-:3) -PAT(-:7)

* PODEZŘELÝ
 : id: blu-a-podezřelý-1
 ~ no-aspect: podezřelý
  + PAT(z+2,zda,že;obl) ADDR(||;obl)
    -derivedFrom: blu-v-podezírat-podezřívat-1
    -valdiff: =PAT(=:z+2,že;-:zda) =ADDR(+:||;-:4) -ACT(-:1)
    #-valdiffCurrent: =PAT(=:z+2,zda,že) =ADDR(+:||;-:4) -ACT(-:1)
 : id: blu-a-podezřelý-2
 ~ no-aspect: podezřelý
  + ACT(3;obl) PAT(7;obl) ADDR(||;obl)
    -derivedFrom: blu-v-podezírat-podezřívat-1
    -valdiff: =ACT(+:3;-:1) =PAT(+:7;-:z+2,zda,že) =ADDR(+:||;-:4)
 : id: blu-a-podezřelý-3
 ~ no-aspect: podezřelý
  + EMPTY ADDR(||;obl)
    -derivedFrom: blu-v-podezírat-podezřívat-1
    -valdiff: =ADDR(+:||;-:4) +EMPTY() -ACT(-:1) -PAT(-:z+2,zda,že)

* PODEZŘENÍ
 : id: blu-n-podezření-1
 ~ no-aspect: podezření
  + ACT(2,pos;obl) PAT(2,na+4,o+6,zda,že,cont;obl)
    -derivedFrom: Vallex-no
 : id: blu-n-podezření-2
 ~ no-aspect: podezření
  + ACT(2,pos;obl) ADDR(2,pos,vůči+3;obl) PAT(2,z+2,zda,že,cont;obl)
    -derivedFrom: blu-v-podezírat-podezřívat-1
    -valdiff: =ACT(>:1->2,pos) =ADDR(>:4->2,pos;+:vůči+3) =PAT(=:z+2,zda,že;+:2,cont)
"""


def assert_attribute_does_not_exist(lu: LexicalUnit, attribute: str, message: str = ''):
    # assert not gettattr(lu, attribute), message  ## FIXME: how to do lu.attribute ???
    assert attribute not in lu.attribs, message
    assert attribute not in lu.dynamic_attribs, message


coll = LexiconCollection()
coll.add_lexicon(parse(SRC, 'stdin'))
cfg = ConfigParser()
lumap = construct_lumap(config=cfg, coll=coll)


def test_add_valdiff_no_derivedFrom():
    lu = lumap['lus']['blu-v-podezírat-podezřívat-1']
    compute_pos(lu)
    compute_valdiffCurrent(lu, lumap)
    assert not lu.valdiff, 'LU without derivedFrom should not have valdiff'
    assert_attribute_does_not_exist(lu, 'valdiff', 'LU without derivedFrom should not have valdiff')
    assert not lu.valdiffCurrent, 'LU without derivedFrom should not have valdiffCurrent'
    assert_attribute_does_not_exist(lu, 'valdiffCurrent', 'LU without derivedFrom should not have valdiff')
    transform_lu_add_valdiff(lu)
    assert not lu.valdiff, 'after add_valdiff, LU without derivedFrom should not have valdiff'
    assert_attribute_does_not_exist(lu, 'valdiff', 'after add_valdiff, LU without derivedFrom should not have valdiff')
    assert not lu.valdiffCurrent, 'after add_valdiff, LU without derivedFrom should not have valdiffCurrent'
    assert_attribute_does_not_exist(lu, 'valdiffCurrent', 'after add_valdiff, LU without derivedFrom should not have valdiff')


def test_add_valdiff_derivedFrom_is_Vallex_no():
    lu = lumap['lus']['blu-n-podezření-1']
    compute_pos(lu)
    compute_valdiffCurrent(lu, lumap)
    assert not lu.valdiff, 'LU with derivedFrom=Vallex-no should not have valdiff'
    assert_attribute_does_not_exist(lu, 'valdiff', 'LU with derivedFrom=Vallex-no should not have valdiff')
    assert not lu.valdiffCurrent, 'LU with derivedFrom=Vallex-no should not have valdiffCurrent'
    assert_attribute_does_not_exist(lu, 'valdiffCurrent', 'LU with derivedFrom=Vallex-no should not have valdiff')

    transform_lu_add_valdiff(lu)
    assert not lu.valdiff, 'after add_valdiff, LU with derivedFrom=Vallex-no should not have valdiff'
    assert_attribute_does_not_exist(lu, 'valdiff', 'after add_valdiff, LU with derivedFrom=Vallex-no should not have valdiff')
    assert not lu.valdiffCurrent, 'after add_valdiff, LU with derivedFrom=Vallex-no should not have valdiffCurrent'
    assert_attribute_does_not_exist(lu, 'valdiffCurrent', 'after add_valdiff, LU with derivedFrom=Vallex-no should not have valdiff')


def test_add_valdiff():
    lu = lumap['lus']['blu-a-podezíraný-podezřívaný-1']
    compute_pos(lu)
    compute_valdiffCurrent(lu, lumap)
    assert not lu.valdiff, 'LU without valdiff but with derivedFrom should not have valdiff'
    assert_attribute_does_not_exist(lu, 'valdiff', 'LU without valdiff but with derivedFrom should not have valdiff')
    assert lu.valdiffCurrent, 'LU with derivedFrom should have valdiffCurrent'
    assert isinstance(lu.dynamic_attribs['valdiffCurrent'], Valdiff), 'valdiffCurrent is of type Valdiff'
    assert 'valdiffCurrent' in lu.dynamic_attribs, 'LU with derivedFrom should have valdiffCurrent'
    assert 'valdiffCurrent' not in lu.attribs, 'valdiffCurrent is not in lu.attribs'
    assert lu.dynamic_attribs['valdiffCurrent'].name == 'valdiffCurrent', 'name of the valdiffCurrent attribute is valdiffCurrent'
    stream = TokenStream('=ACT(>:1->7,od+2) =PAT(=:z+2,zda,že) =ADDR(+:||;-:4)')
    parsed_valdiff = parse_valdiff(None, stream)
    assert lu.dynamic_attribs['valdiffCurrent']._data == parsed_valdiff._data, 'valdiffCurrent should have the expected _data'
    assert lu.dynamic_attribs['valdiffCurrent'] == parsed_valdiff, 'valdiffCurrent should have the expected overall value'

    transform_lu_add_valdiff(lu)
    assert lu.valdiff, 'after add_valdiff, LU which previously had none but had derivedFrom should have valdiff'
    assert 'valdiff' in lu.attribs, 'after add_valdiff, LU which previously had none but had derivedFrom should have valdiff'
    assert 'valdiff' not in lu.dynamic_attribs, 'valdiff should not appear in dynamic_attribs'
    assert lu.attribs['valdiff'].name == 'valdiff', 'after add_valdiff, the name of the valdiff attribute should be valdiff'
    assert lu.valdiffCurrent, 'after add_valdiff, LU with derivedFrom should have valdiffCurrent'
    assert isinstance(lu.dynamic_attribs['valdiffCurrent'], Valdiff), 'after add_valdiff, valdiffCurrent is of type Valdiff'
    assert 'valdiffCurrent' in lu.dynamic_attribs, 'after add_valdiff, LU with derivedFrom should have valdiffCurrent'
    assert 'valdiffCurrent' not in lu.attribs, 'after add_valdiff, valdiffCurrent should be not in lu.attribs'
    assert lu.dynamic_attribs['valdiffCurrent'].name == 'valdiffCurrent', 'after add_valdiff, name of the valdiffCurrent attribute should be valdiffCurrent'
    assert lu.dynamic_attribs['valdiffCurrent']._data == parsed_valdiff._data, 'after add_valdiff, valdiffCurrent should have the expected _data'
    assert lu.dynamic_attribs['valdiffCurrent'] == parsed_valdiff, 'after add_valdiff, valdiffCurrent should have the expected overall value'
    assert lu.dynamic_attribs['valdiffCurrent'] == lu.attribs['valdiff'], 'after add_valdiff, valdiffCurrent and valdiff should be equal'
    assert not lu.dynamic_attribs['valdiffCurrent'] is lu.attribs['valdiff'], 'after add_valdiff, valdiffCurrent and valdiff should be equal, but not identical'
    assert not lu.dynamic_attribs['valdiffCurrent']._data is lu.attribs['valdiff']._data, 'after add_valdiff, valdiffCurrent and valdiff should be equal, but not identical'
