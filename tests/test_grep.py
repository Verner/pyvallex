import locale
import re

from vallex.data_structures import LexiconCollection
from vallex.grep import match_lu, match_lu_patterns, parse_pattern, _changedlocale, grep, get_enriched_patterns
from vallex.txt_tokenizer import TokenStream
from vallex.txt_parser import parse_lexical_unit, parse


def test_match():
    lu = parse_lexical_unit(TokenStream("""
        : id: blu-n-výklad1-1
        ~ no-aspect: výklad # Frame Comment
        + NONE ACT(4;oblig) PAT(4,2)# + Comment
            -derivedFrom: blu-v-vykládat1-vyložit1-4 # Attr Comment
            -note: Tohle je poznamka
                    a tohle pf: taky
            -pdt-vallex: PDT-Vallex-no
            -examplerich: APP: Prohlíželi si výklady obchodů.APP;
                        NONE: Pokud ale převracejí popelnice a rozbíjejí výklady, pak to v pořádku není a taková hudba se mně nelíbí.;
            -note: A tohle je: druha poznamka
    """.strip()), None)

    key, pat = parse_pattern('id=^blu-n')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('id=')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('src=~ no-aspect')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('comment=Attr Comment')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame.functor=ACT')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame.oblig=')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame.oblig=oblig')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame.form=4')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame.form=4,2')[0]
    assert not match_lu(lu, pat, key)

    key, pat = parse_pattern('frame.forms=4,2')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame.functor:form=(ACT|PAT):2')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame.oblig=xsad')[0]
    assert not match_lu(lu, pat, key)

    key, pat = parse_pattern('frame=')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame=(ACT|PAT)')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame=xasdf')[0]
    assert not match_lu(lu, pat, key)

    key, pat = parse_pattern('derivedFrom.comment=Attr Comment')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('synon=')[0]
    assert not match_lu(lu, pat, key)

    key, pat = parse_pattern('examplerich=')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('examplerich.no-aspect.APP=')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('examplerich.no-aspect.NONE=Pokud')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('examplerich.no-aspect.NONE=xsfadf')[0]
    assert not match_lu(lu, pat, key)

    key, pat = parse_pattern('examplerich.no-aspect.NONE=Pokud')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('note=poznamka')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('pdt-vallex=PDT-Vallex-no')[0]
    assert match_lu(lu, pat, key)


def test_enriched_patterns1():
    enriched_patterns = get_enriched_patterns([(['examplerich', 'impf', 'ACT+PAT'], re.compile('.', re.DOTALL))])
    assert enriched_patterns == [[
        (['examplerich', 'impf', 'ACT+PAT'], re.compile('.', re.DOTALL), False),
        (['examplerich', 'impf1', 'ACT+PAT'], re.compile('.', re.DOTALL), False),
        (['examplerich', 'impf2', 'ACT+PAT'], re.compile('.', re.DOTALL), False),
    ]]


def test_enriched_patterns2():
    """Correctly switches order of the aspect and functor"""
    enriched_patterns = get_enriched_patterns([
        (['examplerich', 'ACT+PAT', 'impf'], re.compile('.', re.DOTALL)),
        (['lemma'], re.compile('rozkaz', re.DOTALL))
    ])
    assert enriched_patterns == [
        [(['examplerich', 'impf', 'ACT+PAT'], re.compile('.', re.DOTALL), False),
         (['examplerich', 'impf1', 'ACT+PAT'], re.compile('.', re.DOTALL), False),
         (['examplerich', 'impf2', 'ACT+PAT'], re.compile('.', re.DOTALL), False)],
        [(['lemma'], re.compile('rozkaz', re.DOTALL), False)]
    ]


def test_match_enriched_patterns():
    lu = parse_lexical_unit(TokenStream("""
     : id: blu-n-přesvědčování-přesvědčení-1
     ~ impf: přesvědčování pf: přesvědčení
     + ACT(2,7,pos;obl) ADDR(2,pos;obl) PAT(k+3,o+6,inf,aby,že,cont;obl) MEANS(7;typ)
        -derivedFrom: blu-v-přesvědčit-přesvědčovat-1
        -pdt-vallex: impf: PDT-Vallex-no
                     pf: v-w4992
        -synon: impf: přemlouvání, přivádění někoho ke změně názoru
                pf: přemluvení, přivedení někoho ke změně názoru
        -examplerich: impf1: ACT+ADDR+PAT+MEANS: Ve stavebních kruzích Palubní deník zaregistroval jeho.ACT přesvědčování stavařů.ADDR k větší vstřícnosti.PAT argumentem.MEANS, že Hudeček je sice jednička, ale;
                            ACT+ADDR+PAT: ... dalo usuzovat z nabídek výrobců tiskových zařízení a jejich.ACT přesvědčování zákazníků.ADDR, že barevný tisk potřebují.PAT.;
                      impf2: ACT+PAT: Podlehla jsem manželovu.ACT přesvědčování o kvalitách.PAT cestování na kole;
                            ADDR+PAT: Mám jim pomoct v přesvědčování šéfa.ADDR, aby se změnami souhlasil.PAT.;
                                      Třetí cestou, z níž nelze v žádném případě sejít, je pak vzdělávání pacientů a jejich.ADDR přesvědčování o nutnosti.PAT úpravy životního stylu.;
                      pf: ACT+ADDR: Po nepřesvědčení městského zastupitelstva.ADDR členem.ACT a odborníkem.ACT, jemuž byl vlastní námět svěřen k propracování, pokud se nákladů stavebních týká, dochází k zajištění ploch od p. Leop. Skály.;
                          ADDR+PAT: Podle Jiřího Kastnera nebývá s přesvědčením českých zaměstnanců.ADDR k pohybu.PAT zvláštní problém: „Češi vždy patřili, a věřím, že patřit budou, mezi sportovní národy.;
                                    Podpora prodeje vyžaduje cílené předávání všech informací, které jsou nezbytné k přesvědčení zákazníka.ADDR o tom, že by si měl produkt koupit.PAT.;
                          ADDR+MEANS: Uveďme zde i další postřeh – totiž že v politickém životě se setkáme spíše s vyjednáváním než s diskusí a spíše s kompromisem než přesvědčením protivníka.ADDR svými argumenty.MEANS.;
                          ADDR: Zástupci ohrožených sportů měli deset minut na přesvědčení delegátů.ADDR.;
        -otherforms: ACT(ze_strany+2): Super cesta je neúnavné přesvědčování úředníků.ADDR Ministerstva dopravy a ŘSD ze strany radních.ACT města a všech ochotných místních politiků.ACT.;
    """.strip()), None)

    key, pat = parse_pattern('id=^blu-v')[0]
    assert not match_lu_patterns(lu, [(key, pat)])

    key, pat = parse_pattern('otherforms=neúnavné přesvědčování')[0]
    assert match_lu_patterns(lu, [(key, pat)])

    key, pat = parse_pattern('otherforms.ACT(ze_strany+2)=neúnavné přesvědčování')[0]
    assert not match_lu_patterns(lu, [(key, pat)])

    key, pat = parse_pattern('examplerich=jeho.ACT přesvědčování stavařů')[0]
    assert match_lu_patterns(lu, [(key, pat)])

    key, pat = parse_pattern('examplerich.impf=jeho.ACT přesvědčování stavařů')[0]
    assert match_lu_patterns(lu, [(key, pat)])

    key, pat = parse_pattern('examplerich.impf1=jeho.ACT přesvědčování stavařů')[0]
    assert match_lu_patterns(lu, [(key, pat)])

    key, pat = parse_pattern('examplerich.impf.ACT+ADDR+PAT+MEANS=jeho.ACT přesvědčování stavařů')[0]
    assert match_lu_patterns(lu, [(key, pat)])

    key, pat = parse_pattern('examplerich.ACT+ADDR+PAT+MEANS.impf1=jeho.ACT přesvědčování stavařů')[0]
    assert match_lu_patterns(lu, [(key, pat)])

    key, pat = parse_pattern('examplerich.ACT+ADDR+PAT+MEANS.impf2=jeho.ACT přesvědčování stavařů')[0]
    assert not match_lu_patterns(lu, [(key, pat)])

    key, pat = parse_pattern('examplerich.pf=jeho.ACT přesvědčování stavařů')[0]
    assert not match_lu_patterns(lu, [(key, pat)])


SRC = """
* ČETBA
 : id: blu-n-četba-2
 ~ no-aspect: četba
 + PAT(o+6;opt)
     -derivedFrom: blu-v-číst-1+3
     -pdt-vallex: v-w322
     -synon: čtivo, text určený ke čtení
     -examplerich: PAT: tenkrát v 4. nebo 5. třídě jsme měli povinnou četbu o letadélku.PAT Káněti, právě od Říhy;
                 APP: Dobrou pomůckou bude i při výuce dějepisu a jistě bude i vyhledávanou četbou studentů.APP.;
                     Patřili nebo patří Tři mušketýři k vaší.APP oblíbené četbě?;
                 NONE: Seznam povinné četby ke státní maturitě z českého jazyka je stále neúplný a pravidelně se mění.;
                         To byla jediná četba, co jsme v cele měli.;
     -note: class: communication (neuvádíme);
     -status: hotovo topublish
     -semcategory: substance
     -valdiff: =PAT(=:o+6;-:4,zda,že,cont) -ACT(-:1) -ADDR(-:3)

* CIT
 : id: blu-n-cit-3
 ~ no-aspect: cit
 + ACT(2;obl)
    -derivedFrom: Vallex-no
    -pdt-vallex: PDT-Vallex-no
    -synon: duševní schopnost daná přirozeným nadáním n. zaměstnáním
    -examplerich: ACT+APP: Naše společnost je nemocná a rozdělená a můj.APP cit terapeuta.ACT mi říká, bude to dále pokračovat;
                           Jeho.APP cit básníka.ACT, vkus, přirozená zemitost a suverenita pouťového zpěváka kupletů se naplno rozzářily;
    -class: mental action
    -note: Lze parafráze: cit někoho (básníka) - cit, jaký má někdo (básník).
           Forma vyjádření ACT pouze 2, nikoli pos.
    -status: hotovo topublish
    -semcategory: vlastnost


* VZÍT SI, BRÁT SI
 : id: blu-v-brát-si-vzít-si-7 #OK ML,VK kontrola
 ~ impf: brát si pf: vzít si iter: brávat si
 + ACT(1;obl) CPHR(4;obl)
    -full:
    -lvc:  | dovolená volno
           #NOUN-LEMMAS: dovolená, volno
    -map: ACTv-ACTn   #OK typ A
    -example: impf: Dokonce i Bůh si v srpnu bere dovolenou.;
                    Na dnešní den si všichni v obci berou volno.
              pf: Kdyby si chtěla vzít dovolenou, určitě by mi to řekla.;
                  Budu si muset vzít volno.
    -reflexverb: derived-nonspecific


 : id: blu-v-brát-si-vzít-si-1
 ~ impf: brát si pf: vzít si iter: brávat si
 + ACT(1;obl) PAT(4;obl) EFF(za+4;opt)
    -synon: oženit se / vdát se
    -example: impf: brát si někoho za ženu
              pf: vzal si Marii za manželku
    -recipr: impf: ACT-PAT %Petr a Marie se brali%
             pf: ACT-PAT %Petr a Marie se vzali%
    -reciprevent: joint
    -reciprverb: gram
    -note: při reciprokalizací se "ztrácí" si
    -reflexverb: derived-nonspecific
    -use: posun
    -diat: no_passive
           no_poss-result
"""


def test_sort():
    with _changedlocale(('cs_CZ', 'UTF-8')):
        assert sorted(['Štěpán', 'Stephen', 'Cteni', 'Čtení'], key=locale.strxfrm) == ['Cteni', 'Čtení', 'Stephen', 'Štěpán']

    coll = LexiconCollection()
    coll.add_lexicon(parse(SRC, 'stdin'))

    sorted_lexemes, _ = grep(coll.lexemes, id_only=True, no_sort=False)
    assert sorted_lexemes == [
        ("blu-n-cit-3", "blu-n-cit"),
        ("blu-n-četba-2", "blu-n-četba"),
        ("blu-v-brát-si-vzít-si-7", "blu-v-brát-si-vzít-si"),
        ("blu-v-brát-si-vzít-si-1", "blu-v-brát-si-vzít-si"),
    ]
