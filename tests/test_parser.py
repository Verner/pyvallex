from vallex.data_structures import LexicalUnit, Ref, Frame, FrameElement, Examplerich
from vallex.txt_tokenizer import TokenStream
from vallex.txt_parser import parse_lexeme, parse_lexical_unit, parse_attr


class TestParseTxtAttrib:

    def setup_method(self, method):
        stream = TokenStream(method.__doc__.strip())
        self._attr = parse_attr(stream)

    def test_synon(self):
        """-synon: vysvětlení, vykládání, obšírné poučení, objasnění; vyprávění"""
        assert self._attr._data == {'all': [['vysvětlení', 'vykládání', 'obšírné poučení', 'objasnění'], ['vyprávění']]}

    def test_example(self):
        """ -example: abc; def; #test
                      gh;
        """
        assert self._attr._data['all'] == ["abc", "def", "gh"]

    def test_reflexverb(self):
        """ -reflexverb: derived-nonspecific"""
        assert self._attr._data == {'derived-nonspecific': {}}

    def test_reflexverb1(self):
        """ -reflexverb1: derived-quasiconv blu-v-zlámat-zlomit-1 
                                pf1: %Třiadvacetiletá řidička z Hati za volantem Volkswagenu Venta nezvládla řízení, vyjela mimo cestu a narazila do plotu rodinného domu a sloupu elektrického vedení, který se nárazem zlomil.% (SYN) 
                                pf1: %vlna se zlomila o vlnolam% (made-up)"""
        assert self._attr._data == {'derived-quasiconv':
                                    {'ids': [Ref('blu-v-zlámat-zlomit-1', 'LexicalUnit')],
                                     'example': 'pf1: %Třiadvacetiletá řidička z Hati za volantem Volkswagenu Venta nezvládla řízení, vyjela mimo cestu a narazila do plotu rodinného domu a sloupu elektrického vedení, který se nárazem zlomil.% (SYN) pf1: %vlna se zlomila o vlnolam% (made-up)'
                                     }}

    def test_recipr(self):
        """
             -recipr: ACT-ADDR  %zjednodušit také komunikaci mezi uživateli.ACT-ADDR%
                                %když mohli potrestat špatnou komunikaci mezi domácím gólmanem.ACT-ADDR a obranou.ACT-ADDR%
                                %Takové uspořádání usnadňuje vzájemnou komunikaci lidí.ACT-ADDR o pracovních úkolech.PAT a jejich šéfové mají lepší přehled o tom , co kdo dělá.%
                                %Nyní tato forma komunikace spotřebitelů.ACT-ADDR o zkušenostech.PAT s konkrétními produkty dorazila i do Česka.%
                                %včera našly dvě černé skříňky zachycující údaje o letu a komunikaci posádky.ACT.%
                                %Kroupa má o syna strach. Přijíždí jeho bývalá manželka Táňa. Jejich.ACT-ADDR komunikace o dítěti.PAT je problematická.%
        """
        assert len(self._attr._data) == 1
        dct = self._attr._data['all']
        assert list(dct.keys()) == ['ACT-ADDR']
        assert len(dct['ACT-ADDR']) == 6

    def test_examplerich1(self):
        """
            -examplerich: ACT+ADDR+PAT: Načerpávám tady nové poznatky, které pak mohou zpestřit můj.ACT výklad žákům.ADDR o dané lokalitě.PAT;
                                Na 12 sálů naplněných historickými uměleckými skvosty z přelomu století vzbudilo zasloužený respekt hostů, stejně jako zasvěcený výklad Václava Havla.ACT belgickému panovníkovi.ADDR o secesi.PAT.;
                                Nehrozí tedy, že extremistické postoje paní Semelové, se kterými vystupuje veřejně v novinách, ovlivní její.ACT výklad historie.PAT dětem.ADDR?;
                  ACT+ADDR: Jeho.ACT zasvěcenému výkladu mladým návštěvníkům.ADDR jsem byl několikrát přítomen.;
                  ACT+PAT: Výklad Jana Uhlíře.ACT o historii.PAT chrámu svaté Barbory.;
                           exemplifikovaný výklad gramatického pravidla.PAT učitelem.ACT;
                           Nábožně jsme poslouchali jeho.ACT výklad, že například uzda a monogram na dece jsou.PAT pozlacené.;
                           Z balení, stěhování a Veroničiných.ACT výkladů o tom.PAT, že teď budou nějakou dobu samy, si patrně nic neodnesla.;
                           Cílem je torzo kostela svatého Mikuláše, kde se uskuteční přímo na místě odborný výklad archeologa.ACT o historii.PAT objevu této památky.;
                           autorčin.ACT výklad o projevech.PAT každodennosti v životě vyšších vrstev české společnosti 14. století;
                           Jedna absolvovala výklad o netopýrech.PAT od zoologa.ACT Josefa Hotového, druhá sledovala promítání filmů o nočních letcích a třetí si prohlédla sklepení hradu.;
                           Co vlastně znamená tento druhý emblém? Znakové privilegium Hořovic se nedochovalo, a tak uvěřme výkladu Václava Losa.ACT, že sled barev stříbrná, modrá, stříbrná je odvozen.PAT z lucemburského štítu.;
                           Zamysleme se ještě nad výkladem Kopečného.ACT, že ve větě koupil tři soudky piva je.PAT „základem výrazu skutečný objekt piva“.;
                           Součástí kurzu byl i výklad první pomoci.PAT od zdravotníka.ACT záchranné služby.;
                           Za zmínku stojí třeba výklad role.PAT Jana Husa od Jany Nechutové.ACT.;
                  ADDR+PAT: Máme tady zvířata, která lze využít i při výkladu školákům.ADDR o biologických zákonitostech.PAT;
                            výklad historie.PAT návštěvníkům.ADDR zámku;
                  ACT: počitelemtavý výklad Bohumila Pavlase.ACT;
                  ADDR: Poučný výklad dětem.ADDR přednesl poradce trhu práce Oldřich Struček.;
                  PAT: Nikde nenalézám výklad, zda existují.PAT nějaké stupně či schůdky, jež by přesněji určily, jak daleko ten který jedinec urazil na své cestě od analfabetismu k plné počítačové gramotnosti.;
                       Smysl pro historičnost i důraz na systémovost v přístupu ke zkoumání látky a jejímu.PAT výkladu, přivedly Vodičku koncem 30. let k účasti na práci v Pražském lingvistickém kroužku.;
        """
        assert sorted(self._attr._data['all'].keys()) == sorted(['ACT+ADDR+PAT', 'ACT+ADDR', 'ACT+PAT', 'ADDR+PAT', 'ACT', "ADDR", 'PAT'])
        assert len(self._attr._data['all']['PAT']) == 2
        assert len(self._attr._data['all']['ACT+PAT']) == 11
        assert self._attr._data['all']['ACT'] == ["počitelemtavý výklad Bohumila Pavlase.ACT"]

    def test_examplerich2(self):
        """
            -examplerich: impf: ACT+PAT: národní poslanci měli možnost přímo zasahovat do navrhování zákonů.PAT Evropskou komisí.ACT;
                                ADDR+PAT: nastavit obecný systém navrhování kandidátů.PAT prezidentovi.ADDR;
                                          Ministryně spravedlnosti Helena Válková se skoro dva měsíce k jeho.PAT navržení prezidentu.ADDR Miloši Zemanovi nemá.;
                                PAT: Jak dodal, někteří zastupitelé podali další návrhy na akce, které budou zahrnuty do letošního rozpočtu. "Jeho.PAT navrhováním a schvalováním se budeme znovu zabývat na lednovém zasedání," potvrdil Stanislav Havlín.;
                                     Kouč by tam měl působit jako podpora a pomoc ve zkoumání problémů a navrhování, jak je řešit.PAT.;
                          pf1: ACT+ADDR+PAT: tedy po mém.ACT navrhnutí Raulovi Castrovi.ADDR, že by se měla z Cuby udělat.PAT demokratická země;
                               ACT+PAT: Po mém.ACT navrhnutí souboje.PAT – ačkoli mi bylo jedno, jestli někdo souhlasí či je proti – se veverka začala rozhodovat.;
                               PAT: To vedlo k debatám o navrhnutí Impeachmentu.PAT.;
                                    Pilip prohlásil, že snížení nebylo součástí původního návrhu o státním rozpočtu a že jeho.PAT navrhnutí je populistickým krokem některých poslanců;
                                    Vypadá to, že jednací řád rady vyžaduje dvoutřetinový souhlas pro navrhnutí odvolat.PAT ředitelku.;
                                    Na nich je například navrhnutí, kam odkloní.PAT auta, jimž zakážou vjezd do centra.;
                          pf2: ACT+PAT: Po navržení sumy.PAT zastupitelstvem.ACT se s rozpočtem budou moci seznámit ostatní občané na vývěsní desce.;
        """
        assert sorted(self._attr._data['impf'].keys()) == sorted(['ACT+PAT', 'ADDR+PAT', 'PAT'])
        assert len(self._attr._data['pf1']['PAT']) == 4
        assert len(self._attr._data['pf1'].keys()) == 3
        assert self._attr._data['pf2']['ACT+PAT'] == ['Po navržení sumy.PAT zastupitelstvem.ACT se s rozpočtem budou moci seznámit ostatní občané na vývěsní desce.']

    def test_empty_examplerich(self):
        """
            -examplerich:
        """
        assert isinstance(self._attr._data, dict) and not self._attr._data  # empty examplerich will return empty dict

    def test_comments(self):
        """
            -examplerich: impf: ACT: ochránit jej před učitelovým.ACT rozkazováním;
                                     koalice s ČSSD vedená Onderkou byla však víc o rozkazování jednoho muže.ACT;
                          pf: ACT: Toto se děje na moje.ACT rozkázání; # SSS
                                   Toto je jen TEST;
                                   Toto je jen TEST2;
                                   Rozkázáním krále.ACT Václava utopen jest kněz Johánek; # COMMENT2 TEST
                              PAT: Pár naivních si sice pořád myslí, že černé zlato je oním cinkavým důvodem;
                          no-aspect: ACT: ADAD; # COMMENT
                          biasp: ACT: ACAC; # COMMENT3 TEST
                                 PAT: ABAB; # COMMENT
        """
        assert isinstance(self._attr, Examplerich)
        assert len(self._attr.comments) == 4
        assert len(self._attr.comments['pf']) == 2
        assert len(self._attr.comments['pf']['ACT']) == 4
        assert not self._attr.comments['impf']['ACT']
        assert self._attr.comments['pf']['ACT'][0].content == 'SSS'
        assert not self._attr.comments['pf']['ACT'][1]
        assert not self._attr.comments['pf']['ACT'][2]
        assert self._attr.comments['pf']['ACT'][3].content == 'COMMENT2 TEST'
        assert not self._attr.comments['pf']['PAT']
        assert self._attr.comments['no-aspect']['ACT'][0].content == 'COMMENT'
        assert self._attr.comments['biasp']['ACT'][0].content == 'COMMENT3 TEST'
        assert self._attr.comments['biasp']['PAT'][0].content == 'COMMENT'

    def text_exceptions(self):
        """
            -exceptions: frame_chybejici_act"""
        assert 'frame_chybejici_act' in self._attr._data


class TestParseTxtLexicalUnit:

    def setup_method(self, method):
        stream = TokenStream(method.__doc__.strip())
        self._lexical_unit = parse_lexical_unit(stream, None)

    def test_A(self):
        """
            : id: blu-n-výklad1-1
            ~ no-aspect: výklad (se) 
            + EMPTY TODO
                -derivedFrom: blu-v-vykládat1-vyložit1-4
                -pdt-vallex: PDT-Vallex-no
                -examplerich: APP: Prohlíželi si výklady obchodů.APP;
                            NONE: Pokud ale převracejí popelnice a rozbíjejí výklady, pak to v pořádku není a taková hudba se mně nelíbí.;
                -class: TODO
                -status: hotovo topublish
                -synon: TODO
                -type: TODO
                -reflexverb1: derived-decaus2 blu-v-narovnat-narovnávat-1 no-aspect: %dummy-example%
                -reflexverb2: tantum
                -reflexverb3: derived-partobject1 blu-v-narovnat-narovnávat-1
                -exceptions: frame_chybejici_act
        """
        assert self._lexical_unit._id == 'blu-n-výklad1-1'
        assert self._lexical_unit.lemma._data == {'no-aspect': 'výklad (se)'}
        assert self._lexical_unit.frame.elements[0].functor == 'EMPTY'
        assert self._lexical_unit.frame.elements[1].functor == 'TODO'
        assert self._lexical_unit.frame.elements[0].forms == []
        assert self._lexical_unit.frame.elements[1].forms == []
        assert self._lexical_unit.frame.elements[0].oblig == None
        assert self._lexical_unit.frame.elements[1].oblig == None
        assert sorted(self._lexical_unit.attribs.keys()) == sorted(['derivedFrom', 'pdt-vallex', 'examplerich', 'class', 'status', 'synon', 'type', 'reflexverb1', 'reflexverb2', 'reflexverb3', 'exceptions'])
        assert self._lexical_unit.attribs['type']._data == ['TODO']
        assert self._lexical_unit.attribs['examplerich']._data['no-aspect']['APP'] == ['Prohlíželi si výklady obchodů.APP']
        assert self._lexical_unit.attribs['derivedFrom']._data['ids'] == [Ref('blu-v-vykládat1-vyložit1-4', 'LexicalUnit')]
        assert self._lexical_unit.attribs['reflexverb1']._data == {'derived-decaus2':
                                                                   {"ids": [Ref("blu-v-narovnat-narovnávat-1", "LexicalUnit")],
                                                                    "example": "no-aspect: %dummy-example%"}}
        assert self._lexical_unit.attribs['reflexverb2']._data == {'tantum': {}}
        assert self._lexical_unit.attribs['reflexverb3']._data == {'derived-partobject1':
                                                                   {"ids": [Ref("blu-v-narovnat-narovnávat-1", "LexicalUnit")]}}
        assert 'frame_chybejici_act' in self._lexical_unit.attribs['exceptions']._data

    def test_B(self):
        """
            : id: v-whsb_210hsa_211
            ~ V: zpytovat
            + ACT(1;obl) PAT(4,zda,jestli,cont;obl)
                -synon: knižní, pečlivě zkoumat
                -example: Nezpytoval jsem to.
                -status: active
        """
        assert self._lexical_unit._id == 'v-whsb_210hsa_211'
        assert self._lexical_unit.lemma._data == {'V': 'zpytovat'}
        assert self._lexical_unit.attribs['example']._data['V'] == ['Nezpytoval jsem to.']
        assert isinstance(self._lexical_unit.frame, Frame)
        assert self._lexical_unit.frame.elements == [FrameElement(functor='ACT', forms=['1'], oblig='obl'), FrameElement(functor='PAT', forms=['4', 'zda', 'jestli', 'cont'], oblig='obl')]

    def test_lemma_si_spacing(self):
        """
  : id: blu-n-myšlení-1
  ~ impf: myšlení (si)
  + ACT(2,pos;obl) PAT(2,o+6,že;obl)
     -derivedFrom: blu-v-myslet-myslit-1
     -pdt-vallex: v-w1921f1
        """
        assert list(self._lexical_unit.lemma._data.items()) == [('impf', 'myšlení (si)')]

    def test_comments(self):
        """
            : id: blu-n-výklad1-1 # LU Comment A
            # LU Comment B
            ~ no-aspect: výklad # Lemma Comment
            # LU Comment C
            + NONE # Frame Comment
                -derivedFrom: blu-v-vykládat1-vyložit1-4 # Attr Comment
                -pdt-vallex: PDT-Vallex-no
                -examplerich: APP: Prohlíželi si výklady obchodů.APP;
                            NONE: Pokud ale převracejí popelnice a rozbíjejí výklady, pak to v pořádku není a taková hudba se mně nelíbí.;
                -class: TODO
                -status: hotovo topublish
                -synon: TODO
                -type: TODO
        """
        assert len(self._lexical_unit.comments) == 3
        ca, cb, cc = self._lexical_unit.comments
        assert str(ca) == 'LU Comment A'
        assert str(cb) == 'LU Comment B'
        assert str(cc) == 'LU Comment C'
        assert str(self._lexical_unit.frame.comments['all'][0]) == 'Frame Comment'
        assert str(self._lexical_unit.lemma.comments['all'][0]) == 'Lemma Comment'

    def test_notes(self):
        """
            : id: blu-n-výklad1-1
            ~ no-aspect: výklad # Frame Comment
            + NONE # + Comment
                -derivedFrom: blu-v-vykládat1-vyložit1-4 # Attr Comment
                -note: Tohle je poznamka
                       a tohle pf: taky
                -pdt-vallex: PDT-Vallex-no
                -examplerich: APP: Prohlíželi si výklady obchodů.APP;
                            NONE: Pokud ale převracejí popelnice a rozbíjejí výklady, pak to v pořádku není a taková hudba se mně nelíbí.;
                -note: A tohle je: druha poznamka
        """
        notes = [n for n in self._lexical_unit.attribs.values() if n.name == 'note' or n.duplicate == 'note']
        assert len(notes) == 2
        assert notes[0]._data[0] == "Tohle je poznamka\n                       a tohle pf: taky"
        assert notes[1]._data[0] == "A tohle je: druha poznamka"

    def test_lvc(self):
        """
            : id: blu-n-výklad1-1
            ~ no-aspect: výklad # Frame Comment
            + NONE # + Comment
                -lvc: blu-v-vykládat1-vyložit1-4 blu-v-a-b-c | test lemma # Attr Comment
                -lvc1: blu-v-vykladati
                -lvc3: | testing
        """
        attrs = self._lexical_unit.attribs

        assert [str(c) for c in attrs['lvc'].comments['all']] == ['Attr Comment']
        assert [str(r) for r in attrs['lvc']._data['ids']] == ['blu-v-vykládat1-vyložit1-4', 'blu-v-a-b-c']
        assert attrs['lvc']._data['lemmas'] == ['test', 'lemma']

        assert [str(r) for r in attrs['lvc1']._data['ids']] == ['blu-v-vykladati']
        assert attrs['lvc1']._data['lemmas'] == []

        assert attrs['lvc3']._data['ids'] == []
        assert attrs['lvc3']._data['lemmas'] == ['testing']

    def test_srcA(self):
        """
            : id: blu-n-výklad1-1
            ~ no-aspect: výklad # Frame Comment
            + NONE # + Comment
                -lvc: blu-v-vykládat1-vyložit1-4 a-b-c | test lemma # Attr Comment
                -lvc1: blu-v-vykladati
                -lvc3: | testing
        """
        assert self._lexical_unit.src == self.test_srcA.__doc__.strip()

    def test_srcB(self):
        """
            : id: blu-n-výklad1-1
            ~ no-aspect: výklad # Frame Comment
            + NONE # + Comment
                -lvc: blu-v-vykládat1-vyložit1-4 a-b-c | test lemma # Attr Comment
                -lvc1: blu-v-vykladati
                -lvc3: | testing

            : id: blu-n-cau
            ~ no-aspect:
        """
        assert self._lexical_unit.src.strip().endswith('testing')

    def test_single_aspect(self):
        """
            : id: blu-n-výklad1-1
            ~ impf: výklad (se) iter1: test_iter1 iter2: test_iter2 
            + NONE TODO
                -derivedFrom: blu-v-vykládat1-vyložit1-4
        """
        assert self._lexical_unit.has_single_noniter_aspect
        assert not self._lexical_unit.has_single_aspect


class TestParseTxtLexeme:
    def setup_method(self, method):
        stream = TokenStream(method.__doc__.strip())
        self._lexeme = parse_lexeme(stream)

    def test_lex_A(self):
        """
        * VYČÍTÁNÍ, VYČTENÍ #comment
        : id: blu-n-vyčítání-vyčtení-1+4
        ~ impf: vyčítání pf: vyčtení
        + ACT(2,pos) ADDR(3) PAT(2,pos,že,cont)
            -derived: blu-v-vyčíst-vyčítat-1
            -synon: impf: vytýkání, zazlívání, dělání výčitek pf: vytčení
            -class: communication
            -semcategory: událost
            -pdt-vallex: impf: PDT-Vallex-no
                        pf: PDT-Vallex-no

        : id: blu-n-vyčítání-vyčtení-2
        ~ impf: vyčítání pf: vyčtení
        + ACT(7,pos) PAT(2,pos,zda,že,cont)
            -derived: blu-v-vyčíst-vyčítat-2
            -synon: impf: zjišťování čtením pf: zjištění čtením; vypozorování
            -class: mental action
            -semcategory: událost
            -pdt-vallex: impf: PDT-Vallex-no
                        pf: PDT-Vallex-no

        : id: blu-n-vyčítání-vyčtení-3
        ~ impf: vyčítání pf: vyčtení
        + ACT(2,7,pos) PAT(2,pos)
            -derived: Vallex-no
            -synon: impf: vypočítávání pf: vypočtení, vyjmenování
            -class: mental action
            -semcategory: událost
            -pdt-vallex: impf: PDT-Vallex-no
                        pf: PDT-Vallex-no
        """
        assert self._lexeme._name == 'VYČÍTÁNÍ, VYČTENÍ'
        assert self._lexeme._id == 'blu-n-vyčítání-vyčtení'

    def test_lex_B(self):
        """
        * ZPYTOVAT  (v-whsa_210)
        : id: v-whsb_210hsa_211
        ~ V: zpytovat
        + ACT(1;obl) PAT(4,zda,jestli,cont;obl)
            -synon: knižní, pečlivě zkoumat
            -example: Nezpytoval jsem to.
            -status: active
        """
        assert self._lexeme._name == 'ZPYTOVAT'
        assert self._lexeme._id == 'v-whsa_210'
