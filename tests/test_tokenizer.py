import vallex.txt_tokenizer as tok


def test_augmented_word():
    s = tok.TokenStream('ahoj.ADDR')
    t = s.pop_left()
    assert isinstance(t, tok.AugmentedWord)
    assert t._val == 'ahoj'
    assert t._augment == 'ADDR'


TEST_CASES = [
    (
        """
* VYPOVÍDÁNÍ
""", [
            tok.Newline(),
            tok.LexemeStart(val='VYPOVÍDÁNÍ'),
            tok.Newline(),
            tok.EndOfSource()
        ]),
    (
        """
* VYPOVÍDÁNÍ # komentář
 : id: blu-n-vypovídání-1 # Comment
 ~ no-aspect: vypovídání (se)
 + EMPTY ACT(||;obl)
   -recipr: no-aspect: ADDR+EFF: %ahoj jak%
 : id: blu-n-vypovídání-2
""",
        [
            tok.Newline(),
            tok.LexemeStart(val='VYPOVÍDÁNÍ'), tok.WhiteSpace(val=' '), tok.Comment(val='komentář'),

            tok.Newline(),
            tok.WhiteSpace(val=' '), tok.LexicalUnitStart(val='blu-n-vypovídání-1'), tok.Comment(val='Comment'),

            tok.Newline(),
            tok.WhiteSpace(val=' '), tok.Lemma(), tok.Aspect(val='no-aspect'), tok.Word(val='vypovídání'),
            tok.WhiteSpace(val=' '), tok.OpenParen(), tok.Word(val='se'), tok.CloseParen(),

            tok.Newline(),
            tok.WhiteSpace(val=' '), tok.Frame(),
            tok.ValencySlot(val='EMPTY'), tok.WhiteSpace(val=' '),
            tok.ValencySlot(val='ACT'), tok.Newline(),
            tok.WhiteSpace(val='   '), tok.LexicalUnitAttr(val='recipr'), tok.Aspect(val='no-aspect'),
            tok.FunctorCombination(val='ADDR+EFF'), tok.Percent(), tok.Word(val='ahoj'), tok.WhiteSpace(val=' '),
            tok.Word(val='jak'), tok.Percent(),

            tok.Newline(),
            tok.WhiteSpace(val=' '), tok.LexicalUnitStart(val='blu-n-vypovídání-2'),

            tok.Newline(),
            tok.EndOfSource()
        ]),
    (
        """
    -semcategory: jiné
    -reflexverb: derived-decaus_theme blu-v-rozptýlit-2 %jiné% (SYN)
""",
        [
            tok.Newline(),
            tok.WhiteSpace(val='    '), tok.LexicalUnitAttr(val='semcategory'), tok.Value(val='jiné'),

            tok.Newline(),
            tok.WhiteSpace(val='    '), tok.LexicalUnitAttr(val='reflexverb'),
            tok.Value(val='derived-decaus_theme'),
            tok.WhiteSpace(val=' '), tok.Identifier(val='blu-v-rozptýlit-2'),
            tok.WhiteSpace(val=' '), tok.Percent(), tok.Word(val='jiné'), tok.Percent(),
            tok.WhiteSpace(val=' '), tok.OpenParen(), tok.Word(val='SYN'), tok.CloseParen(),

            tok.Newline(),
            tok.EndOfSource()
        ]
    ),
    (
        """
    -derivedFrom: blu-a-bohatý-1
    -note: Odpovídá bohatý-1: forma
""",
        [
            tok.Newline(),
            tok.WhiteSpace(val='    '), tok.LexicalUnitAttr(val='derivedFrom'), tok.Identifier(val='blu-a-bohatý-1'),

            tok.Newline(),
            tok.WhiteSpace(val='    '), tok.LexicalUnitAttr(val='note'), tok.Word(val='Odpovídá'),
            tok.WhiteSpace(val=' '), tok.Word(val='bohatý-1'), tok.Other(val=':'),
            tok.WhiteSpace(val=' '), tok.Word(val='forma'),

            tok.Newline(),
            tok.EndOfSource()
        ]
    ),
    (
        """
    -note: na -telnost: %Rozšířila
""",
        [
            tok.Newline(),
            tok.WhiteSpace(val='    '), tok.LexicalUnitAttr(val='note'), tok.Word(val='na'), tok.WhiteSpace(val=' '),
            tok.Word(val='-telnost'), tok.Other(val=':'), tok.WhiteSpace(val=' '), tok.Percent(),
            tok.Word(val='Rozšířila'),

            tok.Newline(),
            tok.EndOfSource()
        ]
    ),
    (
        """
    -example: inf: Jíst.
              adj-1: Milan.
""",
        [
            tok.Newline(),
            tok.WhiteSpace(val='    '), tok.LexicalUnitAttr(val='example'), tok.Word(val='inf'), tok.Other(val=':'),
            tok.WhiteSpace(val=' '), tok.Word(val='Jíst'), tok.Other(val='.'),

            tok.Newline(),
            tok.WhiteSpace(val='              '), tok.Word(val='adj-1'),
            tok.Other(val=':'), tok.WhiteSpace(val=' '), tok.Word(val='Milan'), tok.Other(val='.'),

            tok.Newline(),
            tok.EndOfSource()
        ]
    )
]


def test_push_left_loc():
    s = tok.TokenStream("\n \n")
    t = next(s)
    assert t._loc.pos == 0
    t = next(s)
    assert t._loc.pos == 1
    s.push_left(t)
    assert s.loc._pos == 1
    t = next(s)
    assert t._loc.pos == 1


def test_cat_until():
    s = tok.TokenStream("# ahoj\n     \n")
    toksA = s.cat_until([tok.Newline])
    assert next(s) == tok.Newline()
    toksB = s.cat_until([tok.Newline])
    assert isinstance(toksB[0], tok.WhiteSpace)
    assert next(s) == tok.Newline()


def test_cases():
    for (src, res) in TEST_CASES:
        s = tok.TokenStream(src)
        assert list(s) == res
