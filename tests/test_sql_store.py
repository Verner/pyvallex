import tempfile

from pathlib import Path
from time import time

import pytest  # type: ignore

from vallex.data_structures import LexiconCollection
from vallex.server.sql_store import SQLStore
from vallex.txt_parser import parse


@pytest.fixture(name="coll")
def fixture_coll():
    coll = LexiconCollection()
    with (Path(__file__).parent / 'data/verbs.txt').open(encoding='utf-8') as IN:
        coll.add_lexicon(parse(IN, fname=IN.name))
    return coll


@pytest.fixture(name="temp_file")
def fixture_temp_file():
    _, store_path = tempfile.mkstemp(suffix='.db')
    yield store_path
    Path(store_path).unlink()


def test_basic(coll):
    store = SQLStore(':memory:')
    for lexicon in coll.lexicons:
        store.update_lexicon(lexicon)

    assert len(store) == len(coll)


def test_multi_access(coll, temp_file):
    store_a = SQLStore(temp_file)
    for lexicon in coll.lexicons:
        store_a.update_lexicon(lexicon)

    store_b = SQLStore(temp_file)

    assert len(store_a) == len(coll)
    assert len(store_b) == len(coll)

    lu = store_a.id2lu('blu-v-brát-vzít-1')
    pre_update = time()
    pre_update_data = lu.attribs['use']._data
    lu.attribs['use']._data = 'ahoj'
    store_a.update_lu(lu, lu, 'test')

    assert store_b.id2lu('blu-v-brát-vzít-1').attribs['use']._data == pre_update_data

    changed = store_b.lu_changed_since(pre_update)
    assert len(changed) == 1
    assert changed[0]._id == lu._id
    assert changed[0].attribs['use']._data == 'ahoj'
    assert store_b.id2lu(lu._id).attribs['use']._data == 'ahoj'


def test_changed_lu_detection(coll, temp_file):
    store = SQLStore(temp_file)
    lu_lexicon = None
    for lexicon in coll.lexicons:
        if 'blu-v-brát-vzít-1' in [l._id for l in lexicon.lexical_units]:
            lu_lexicon = lexicon
        store.update_lexicon(lexicon)
    lu = store.id2lu('blu-v-brát-vzít-1')
    lu.attribs['use']._data = 'ahoj'
    store.update_lu(lu, lu, 'test')
    assert lu in store.changed_lexical_units()
    assert lu in store.changed_lexical_units(lu_lexicon)
