import hashlib
import os
import tempfile

from pathlib import Path

import pytest  # type: ignore

from vallex.data_structures import LexiconCollection, Comment
from vallex.json_utils import dumps, loads
from vallex.txt_parser import parse


SRC = """
# Comments

* VZÍT SE, BRÁT SE
 : id: blu-v-brát-se-vzít-se-1
 ~ impf: brát se  pf: vzít se iter: brávat se
 + ACT(1;obl) PAT(o+4,za+4;obl)
    -synon: impf: podporovat; ujímat se; zastávat se pf: podpořit; ujmout se; zastat se
    -example: impf: brát se o něco / za někoho
              pf: vzal se o něco / za něj
    -reflexverb: derived-nonspecific
    -recipr: impf: ACT-PAT %brali se za sebe%
             pf: ACT-PAT %vzali se za sebe%
    -reciprevent: distributed #VK neutral
    -reciprverb: gram
    -reflexverb: impf: derived-odvoz.
             pf: derived-odvoz.
    -use: posun
 : id: blu-v-brát-se-vzít-se-2
 ~ impf: brát se  pf: vzít se
 + ACT(1;obl) LOC(;obl)
    -synon: impf: objevovat se; vyskytovat se pf: objevit se; vyskytnout se
    -example: impf: Kde se tu bereš?
              pf: kde se vzal, tu se vzal čert; kde se tu vzaly ty hodinky?
    -reflexverb: derived-nonspecific
    -use: posun

* VZÍT SI, BRÁT SI
 : id: blu-v-brát-si-vzít-si-7 #OK ML,VK kontrola
 ~ impf: brát si pf: vzít si iter: brávat si
 + ACT(1;obl) CPHR(4;obl)
    -full:
    -lvc:  | dovolená volno
           #NOUN-LEMMAS: dovolená, volno
    -map: ACTv-ACTn   #OK typ A
    -example: impf: Dokonce i Bůh si v srpnu bere dovolenou.;
                    Na dnešní den si všichni v obci berou volno.
              pf: Kdyby si chtěla vzít dovolenou, určitě by mi to řekla.;
                  Budu si muset vzít volno.
    -reflexverb: derived-nonspecific


 : id: blu-v-brát-si-vzít-si-1
 ~ impf: brát si pf: vzít si iter: brávat si
 + ACT(1;obl) PAT(4;obl) EFF(za+4;opt)
    -synon: oženit se / vdát se
    -example: impf: brát si někoho za ženu
              pf: vzal si Marii za manželku
    -recipr: impf: ACT-PAT %Petr a Marie se brali%
             pf: ACT-PAT %Petr a Marie se vzali%
    -reciprevent: joint
    -reciprverb: gram
    -note: při reciprokalizací se "ztrácí" si
    -reflexverb: derived-nonspecific
    -use: posun
    -diat: no_passive
           no_poss-result
"""

SRC_CHECKSUM = hashlib.sha512(bytes(SRC, encoding='utf-8')).hexdigest()


def test_changes_detection():
    _, path = tempfile.mkstemp()

    with open(path, 'w', encoding='utf-8') as OUT:
        OUT.write(SRC)
    last_m = os.stat(path).st_mtime

    with open(path, 'r', encoding='utf-8') as IN:
        lexicon = parse(IN, fname=path)

    assert lexicon._checksum == SRC_CHECKSUM, "Checksum should be ready and correct after parsing"

    assert not lexicon.changed_on_disk(last_m)

    Path(path).unlink()
    with open(path, 'w', encoding='utf-8') as OUT:
        OUT.write(SRC)

    assert not lexicon.changed_on_disk(last_m), "Rewriting the file with no changes should not lead to change detection"

    with open(path, 'w', encoding='utf-8') as OUT:
        OUT.write(SRC+"\n")

    if os.stat(path).st_mtime < last_m:
        pytest.skip("Stat mtime did not change after rewriting!! Would lead to undetected change, so skipping the test.")

    assert lexicon.changed_on_disk(last_m), "A modified file should be correctly recognized"


def test_update_lu():
    coll = LexiconCollection()
    coll.add_lexicon(parse(SRC, 'stdin'))

    assert coll._lexicons['stdin']._checksum == SRC_CHECKSUM

    old_lu = coll.id2lu('blu-v-brát-si-vzít-si-1')
    new_lu = loads(dumps(old_lu))
    new_lu._parent = old_lu._parent
    new_lu.comments.append(Comment("new comment"))

    coll.update_lu(old_lu, new_lu)

    assert coll.id2lu('blu-v-brát-si-vzít-si-1') == new_lu
    assert old_lu not in new_lu._parent.lexical_units
    assert new_lu in new_lu._parent.lexical_units
