import os
import pytest  # type: ignore
import subprocess
import tempfile

import clipboard  # type: ignore

from contextlib import contextmanager
from pathlib import Path

from selenium.webdriver.common.by import By  # type: ignore
from selenium.webdriver.support.ui import WebDriverWait  # type: ignore
from selenium.webdriver.support import expected_conditions as EC  # type: ignore

from vallex import load_lexicon, Config
from vallex.server import utils
from vallex.server.sql_store import SQLStore


RUN_SELENIUM_TESTS = os.environ.get('RUN_SELENIUM_TESTS')
conditional_skip = pytest.mark.skipif(RUN_SELENIUM_TESTS != 'YES', reason="set the env var RUN_SELENIUM_TESTS to 'YES' to enable selenium tests")


TEST_LEXICON_SRC = (Path(__file__).parent.parent / 'data/verbs.txt').read_text(encoding='utf-8')
CLI_PATH = Path(__file__).parent.parent.parent / 'vallex' / 'main.py'
PYTHON_PATH = Path(__file__).parent.parent.parent


@contextmanager
def temp_path(suffix='', content=None):
    _, src_path = tempfile.mkstemp(suffix=suffix)
    if content:
        Path(src_path).write_text(content, encoding='utf-8')
    try:
        yield Path(src_path)
    finally:
        Path(src_path).unlink()


@pytest.fixture(scope="module", name="my_web_server")
def web_server():
    with temp_path(suffix='-lexicon.txt', content=TEST_LEXICON_SRC) as lex_path:
        with temp_path(suffix='.db') as db_path:
            with temp_path(suffix='.ini') as cfg_path:
                config = Config(search_default_locations=False)
                config.web_port = utils.find_free_port(8800)
                config.web_db = db_path
                config.web_lexicons = [lex_path]
                cfg_path.write_text(str(config), encoding='utf-8')
                os.environ['PY_VALLEX_CONFIG'] = str(cfg_path)
                os.environ['PYTHONPATH'] = str(PYTHON_PATH)
                with subprocess.Popen(['python', str(CLI_PATH), 'web', '--no-browser'], stdout=subprocess.PIPE, stderr=subprocess.PIPE) as server_proc:
                    utils.wait_for_port(config.web_port)
                    print(lex_path, db_path, config.web_port, server_proc)
                    yield (lex_path, db_path, config.web_port, server_proc)
                    server_proc.kill()
                    print(server_proc.stdout.read())
                    print(server_proc.stderr.read())


class performance_mark:
    def __init__(self, mark_name):
        self.mark_name = mark_name

    def __call__(self, driver):
        ret = driver.execute_script("return performance.getEntriesByName('"+self.mark_name+"')")
        if ret:
            return ret[0]
        else:
            return False


class performance_marks:
    def __init__(self, marks):
        self.marks = marks
        self.marks_done = {}

    def __call__(self, driver):
        for mark in self.marks:
            if mark not in self.marks_done:
                ret = driver.execute_script("return performance.getEntriesByName('"+mark+"')")
                if ret:
                    self.marks_done[mark] = ret
        if len(self.marks_done) == len(self.marks):
            return self.marks_done
        else:
            return False


class elt_shown:
    def __init__(self, elt):
        self.elt = elt

    def __call__(self, driver):
        return self.elt.is_displayed()


class first_elt_shown:
    def __init__(self, elts):
        self.elts = elts

    def __call__(self, driver):
        for elt in self.elts:
            if elt.is_displayed():
                return elt
        return False


class elt_hidden:
    def __init__(self, elt):
        self.elt = elt

    def __call__(self, driver):
        return not self.elt.is_displayed()


class has_value:
    def __init__(self, elt, value):
        self.elt = elt
        self.value = value

    def __call__(self, driver):
        return self.elt.get_attribute('value') == self.value


class App:
    class Search:
        help_btn = None

    def __init__(self, my_web_server, selenium):
        _, _, port, _ = my_web_server
        self.selenium = selenium
        self.base_url = f'http://localhost:{port}/'
        selenium.get(self.base_url)
        WebDriverWait(selenium, 4).until(
            performance_mark('api/load/lexicon/process:end')
        )
        self.search = App.Search()
        self.search.help_btn = selenium.find_element_by_xpath("//form//i[text()='help']")
        self.search.link_btn = selenium.find_element_by_xpath("//form//i[text()='link']")
        self.search.query_input, self.search.source_input = selenium.find_elements_by_xpath("//form//input")
        self.help_dlg = selenium.find_element_by_xpath('//*[@id="App-HelpDlg"]/..')
        self.help_dlg_close = selenium.find_element_by_xpath('//*[@id="App-HelpDlg"]//i[text()="close"]')
