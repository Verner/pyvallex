import logging
import re

from pathlib import Path

from vallex.data_structures import LexiconCollection
from vallex.txt_parser import parse
from vallex.log import root_logger

DATA = [
    Path(__file__).parent / 'data/verbs.txt'
]

NAME_RE = re.compile(r'\*\s+([^#]*)(#.*)?$')
ID_RE = re.compile(r'\s\:\sid([^#]*)(#.*)?$')

root_logger.setLevel(logging.ERROR)


def test_line_locations():
    for df in DATA:
        loc_coll = {}
        coll = LexiconCollection()
        with df.open(encoding='utf-8') as IN:
            coll.add_lexicon(parse(IN, fname=df.name))

        with df.open('r', encoding='utf-8') as IN:
            ln_num = 0
            for ln in IN.readlines():
                m = NAME_RE.match(ln)
                if m:
                    loc_coll[m.groups()[0].strip()] = ln_num
                ln_num += 1

        diff = []
        for lex in coll.lexemes:
            assert lex._name in loc_coll
            if not lex._src_start.line == loc_coll[lex._name]:
                diff.append((lex._src_start.line, lex._name, lex._src_start.line-loc_coll[lex._name]))
        assert diff == []
