from configparser import ConfigParser
from pathlib import Path

from vallex import add_file_to_collection, LexiconCollection
from vallex.scripts import SCRIPTS, load_script_file, run_script, run_scripts, prepare_requirements, RES_ERROR, RES_FAIL, RES_PASS, RES_SKIP, RES_WARNING
from vallex.scripts.utils import _REQUIREMENTS


def setup():
    SCRIPTS.clear()
    _REQUIREMENTS.clear()


def test_invalid_script():
    scrs = load_script_file(Path(__file__).parent/'data'/'scripts'/'invalid_script')
    assert not scrs


def test_valid_script():
    coll = LexiconCollection()
    cfg = ConfigParser()
    add_file_to_collection(coll, (Path(__file__).parent/'data'/'verbs.txt').open('r', encoding='utf-8'))

    scripts = load_script_file(Path(__file__).parent/'data'/'scripts'/'scripts.py')
    assert len(scripts) == 6

    prepare_requirements(cfg, coll)

    tst_scripts = [s for s in scripts if s.__name in ['fake_test', 'error']]
    assert len(tst_scripts) == 2

    for script in tst_scripts:
        res, msg = run_script(coll, script)
        if script.__name == 'fake_test':
            assert res == RES_PASS
            assert msg == ""
        elif script.__name == 'error':
            assert res == RES_ERROR
            assert msg.startswith('Traceback (most recent call last)')
        # FIXME: the remaining cases do not do anything because these tests are not in tst_scripts!!!
        elif script.__name == 'skip':
            assert res == RES_SKIP
            assert msg == ""
        elif script.__name == 'pass':
            assert res == RES_PASS
            assert msg == "pass"
        elif script.__name == 'warning':
            assert res == RES_WARNING
            assert msg == 'warning'
        elif script.__name == 'fail':
            assert res == RES_FAIL
            assert msg == 'fail'
        # FIXME: here ends the part that needs to be moved/replaced
        # see also issue #

    stats, failures, warnings = run_scripts(coll, 'test')

    assert len(failures) == len(coll)
    assert failures[0][0] == 'fail'
    assert failures[0][2] == 'fail'
    assert len(warnings) == len(coll)
    assert warnings[0][0] == 'warning'
    assert warnings[0][2] == 'warning'

    for test_name in ['skip', 'pass', 'fail', 'warning']:
        for test_result in ['skip', 'pass', 'fail', 'warning']:
            if test_name == test_result:
                assert stats['test.lu'][test_name][test_result] == len(coll), test_name+" should return "+test_result
            else:
                assert stats['test.lu'][test_name][test_result] == 0, test_name+" should not return "+test_result
        assert 'error' not in stats['test.lu'][test_name]
        assert stats['test.lu']['error'][test_name] == 0

    assert stats['test.lu']['error']['error'] != ""
