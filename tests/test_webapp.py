import tempfile

from contextlib import contextmanager
from pathlib import Path

from pytest import fixture, mark
from webtest import TestApp as WrapApp


import vallex.server.views

from vallex import load_lexicon, Config
from vallex.server.bottle_utils import WebAppFactory
from vallex.server.app_state import AppState
from vallex.server.maint import webdb_add_lexicons, webdb_migrate
from vallex.server.sql_store import SQLStore

TEST_LEXICON_SRC = (Path(__file__).parent / 'data/verbs.txt').read_text(encoding='utf-8')


@contextmanager
def temp_path(suffix='', content=None):
    _, src_path = tempfile.mkstemp(suffix=suffix)
    if content:
        Path(src_path).write_text(content, encoding='utf-8')
    try:
        yield Path(src_path)
    finally:
        Path(src_path).unlink()


@contextmanager
def lexicon():
    with temp_path('-lexicon.txt', content=TEST_LEXICON_SRC) as temp_src_path:
        with temp_src_path.open('r', encoding='utf-8') as IN:
            yield load_lexicon(IN)


@fixture(name="app_store_tuple")
def fixture_app_store_tuple():
    with lexicon() as lex, temp_path(suffix='.db') as db_path:
        config = Config(search_default_locations=False)
        store = SQLStore(str(db_path))
        webdb_migrate(store)
        webdb_add_lexicons(store, [lex])
        state = AppState(config, store)
        web_app = WrapApp(WebAppFactory.create_app(state).wsgi)
        yield web_app, store, state


@mark.skip(reason="Frontend built files not present")
def test_index(app_store_tuple):
    testapp, _, _ = app_store_tuple
    response = testapp.get('/')
    assert response.status == '200 OK'
    assert response.status_int == 200


def test_attrs_endpoint(app_store_tuple):
    testapp, store, _ = app_store_tuple
    response = testapp.get('/api/attribs')
    assert response.status == '200 OK', "Attribs API should return 200 OK status"
    attrs = set([(a['name'], a['desc']) for a in response.json['result'] if a['name'] == 'error' or a['name'] == 'warning' or not (a['name'].startswith('error.') or a['name'].startswith('warning.'))])
    assert attrs == set(sum([lu.match_keys() for lu in store.lexical_units], [])), "Attribs API should return correct list of attributes"


UPDATED_LU = """
: id: blu-v-brát-vzít-1
 ~ impf: brát (si) pf: vzít (si) iter: brávat (si)
 + ACT(1;obl) PAT(4;obl) ORIG(od+2;opt) LOC(;typ) DIR1(;typ) RCMP(za+4;typ)
    -synon: impf: přijímat; získávat pf: přijmout; získat
    -example: impf: brát si od někoho mzdu / peníze za práci; vláda nebude mít odkud peníze brát; brát si snídani
              pf: vzal si od něj peníze za práci; vláda nebude mít odkud peníze vzít; vzít si snídani
    -note: mohli loni brát na odměnách.COMPL měsíčně 26 až 40 tisíc korun
           volné si
    -recipr: impf: ACT-ORIG %berou si od sebe peníze%
             pf: ACT-ORIG %nikdy si od sebe nevzali peníze%
    -reciprevent: distributed
    -use: primator
    -class: exchange
    -diat: no_poss-result
           no_recipient
           impf: deagent %za práci se bere mzda% [made-up]
                 passive-být %Málo se hodí pro 6. až 9. ročník, a už vůbec ne pro studenty středních škol a odborných učilišť, ačkoli obsah příspěvku je brán právě z tohoto prostředí.% [SYN] #OK freq: 0x ve 100
           pf: deagent %vezmou se od nich peníze za práci% [made-up]
               passive-být %Jsou-li ve státním rozpočtu peníze navíc, musely být odněkud vzaty (ze soukromé sféry) a tam chybějí a tam vytvářejí nižší poptávku, než by jinak vytvářely.% [SYN] #OK freq: 0x ve 100
""".lstrip()


def test_editing(app_store_tuple):
    testapp, _, state = app_store_tuple
    # Need to set mode to client, otherwise we get Forbidden errors
    state.config.web_mode = 'client'
    # blu-v-br%C3%A1t-vz%C3%ADt-1 = blu-v-brát-vzít-1
    response = testapp.post_json('/api/lexical_unit/blu-v-br%C3%A1t-vz%C3%ADt-1/', {'src': UPDATED_LU})
    assert response.status == '200 OK', "Lexical unit API should return 200 OK status on modifications to existing lu's"
    assert response.json['result']['source']['text'] == UPDATED_LU, "Edited lu should return the changed source"
    assert 'reciprverb' not in response.json['result']['attrs'], "Deleted attributes should not be present"
    assert response.json['result']['attrs']['use']['data'] == ['primator'], "Modified attributes should have new values"


@mark.skip(reason="For some reason, this fails on decompressing the response to changes. WTF?")
def test_changes(app_store_tuple):
    testapp, _, state = app_store_tuple
    # Need to set mode to client, otherwise we get Forbidden errors
    state.config.web_mode = 'client'
    # blu-v-br%C3%A1t-vz%C3%ADt-1 = blu-v-brát-vzít-1
    response = testapp.post_json('/api/lexical_unit/blu-v-br%C3%A1t-vz%C3%ADt-1/', {'src': UPDATED_LU})
    pre_ts = response.json['ts']
    assert response.status == '200 OK', "Lexical unit API should return 200 OK status on modifications to existing lu's"
    assert response.json['result']['source']['text'] == UPDATED_LU, "Edited lu should return the changed source"
    response = testapp.get(f'/api/changes?timestamp={pre_ts}')
    assert len(response.json['result']) == 1
    assert response.json['result'][0]['id'] == "blu-v-brát-vzít-1"
