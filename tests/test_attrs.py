from vallex import Valdiff, Frame, Lemma
from vallex.data_structures import FrameElement
from vallex.data_structures.utils import AttrAccessor
from vallex.txt_parser import parse_valdiff, parse_frame
from vallex.txt_tokenizer import TokenStream


def test_lemma():
    l = Lemma(data={
        'pf': 'brát',
        'impf': 'brát (si)',
        'iter': 'otvírat /otevírat',
        'biasp': 'brát se',
        'pf1': 'dělat1',
        'pf2': 'dělat2',
        'iter3': 'dělat2 si',
        'iter4': 'dělat si4'
    })

    assert l.lemma_set() == {'brát', 'otvírat', 'otevírat', 'brát se', 'dělat1', 'dělat2', 'dělat2 si', 'dělat si4'}
    assert l.lemma_set(noiter=True) == {'brát', 'brát se', 'dělat1', 'dělat2'}
    assert l.lemma_set(discern_homo=False, noiter=True) == {'brát', 'brát se', 'dělat'}
    assert l.lemma_set(discern_homo=False) == {'brát', 'otvírat', 'otevírat', 'brát se', 'dělat', 'dělat si'}


def test_frame_actants():
    stream = TokenStream('ACT(1,jako+1;) DIR1(1,add+4,2;) DIR2(1,po+3,2;opt) ADDR(4,za+5,2;obl)')
    parsed_frame = parse_frame(stream)

    assert parsed_frame.actant == ['ACT', 'ADDR']
    assert parsed_frame.actant.form == ['1', 'jako+1', '4', 'za+5', '2']
    assert parsed_frame.actant.forms == ['1,jako+1', '4,za+5,2']
    assert getattr(parsed_frame, 'actant:form') == ['ACT:1', 'ACT:jako+1', 'ADDR:4', 'ADDR:za+5', 'ADDR:2']


def test_accessor_valdiff():
    stream = TokenStream('=ACT(>:1->7,pos@jako+1->jako+2;+:3) =DIR1(=:1,add+4,2) =DIR2(=:2;+:3,po+4;-:1,po+3) +ORIG(+:4,za+5,2) -ADDR(-:4,za+5,2)')
    parsed_valdiff = parse_valdiff(None, stream)

    assert set(parsed_valdiff.functor) == set(['ACT', 'DIR1', 'DIR2', 'ADDR', 'ORIG'])
    assert parsed_valdiff.functor.eq[:] == ['ACT', 'DIR1', 'DIR2']
    assert parsed_valdiff.ACT.form.add == ['3']
    assert parsed_valdiff.ACT.eq.form.add == ['3']
    assert parsed_valdiff.ACT.del_.form.add == []
    assert parsed_valdiff.DIR1.eq.form.eq == ['1', 'add+4', '2']
    assert parsed_valdiff.DIR2.eq.form.eq == ['2']
    assert parsed_valdiff.DIR2.eq.form.add == ['3', 'po+4']
    assert parsed_valdiff.DIR2.eq.form.del_ == ['1', 'po+3']
    assert parsed_valdiff.ADDR.del_.form.del_ == ['4', 'za+5', '2']
    assert parsed_valdiff.ADDR.del_.form.add == []
    assert parsed_valdiff.ADDR.add.form.del_ == []
    assert parsed_valdiff.ADDR.add.form == []
    assert parsed_valdiff.ORIG.add.form.add == ['4', 'za+5', '2']
    assert parsed_valdiff.ORIG.add.form.del_ == []
    assert parsed_valdiff.ORIG.eq.form.add == []


def test_valdiff():
    verb_frame = Frame(data=[
        FrameElement(functor='ACT', forms=['1', 'jako+1']),
        FrameElement(functor='DIR1', forms=['1', 'add+4', '2']),
        FrameElement(functor='DIR2', forms=['1', 'po+3', '2'], oblig='opt'),
        FrameElement(functor='ADDR', forms=['4', 'za+5', '2'], oblig='obl'),
    ])
    noun_frame = Frame(data=[
        FrameElement(functor='ACT', forms=['7', 'pos', '3', 'jako+2']),
        FrameElement(functor='DIR1', forms=['1', 'add+4', '2']),
        FrameElement(functor='DIR2', forms=['3', 'po+4', '2'], oblig='typ'),
        FrameElement(functor='ORIG', forms=['4', 'za+5', '2'], oblig='obl'),
    ])
    valdiff = noun_frame.diff(verb_frame,
                              systemic_changes=Valdiff.SYSTEMIC_CHANGES_VERB_NOUN)
    stream = TokenStream('=ACT(>:1->7,pos@jako+1->jako+2;+:3) =DIR1(=:1,add+4,2) =DIR2(=:2;+:3,po+4;-:1,po+3) +ORIG(+:4,za+5,2) -ADDR(-:4,za+5,2)')
    parsed_valdiff = parse_valdiff(None, stream)
    assert valdiff._data == parsed_valdiff._data

    assert [slot.functor for slot in valdiff._data] == ['ACT', 'DIR1', 'DIR2', 'ORIG', 'ADDR'], "Frame.diff should preserve functor order"
    assert valdiff._data[0].forms_typ[0][1] == ['7', 'pos'], "Frame.diff should preserve form order"
    assert valdiff._data[1].forms_eq == ['1', 'add+4', '2'], "Frame.diff should preserve form order"
    assert valdiff._data[2].forms_add == ['3', 'po+4'], "Frame.diff should preserve form order"
    assert valdiff._data[2].forms_del == ['1', 'po+3'], "Frame.diff should preserve form order"

    stream = TokenStream('=ACT(=:1,add+4,2)')
    parsed_valdiff = parse_valdiff(None, stream)
    assert parsed_valdiff._data[0] == FrameElement(functor='ACT', forms=['1', 'add+4', '2'])

    assert valdiff.form.typ == ['1->7,pos', 'jako+1->jako+2']


def test_valdiff_actants():
    stream = TokenStream('=ACT(>:1->7,pos@jako+1->jako+2;+:3) =DIR1(=:1,add+4,2) =DIR2(=:2;+:3,po+4;-:1,po+3) +ORIG(+:4,za+5,2) -ADDR(-:4,za+5,2)')
    parsed_valdiff = parse_valdiff(None, stream)

    assert set(parsed_valdiff.actant) == set(['ACT', 'ADDR', 'ORIG'])
    assert parsed_valdiff.actant.eq[:] == ['ACT']
    assert set(parsed_valdiff.actant.form) == set(['1->7,pos', 'jako+1->jako+2', '3'])
    assert not parsed_valdiff.actant.form.eq
    assert parsed_valdiff.actant.form.add == ['3']

    stream = TokenStream('=ACT(>:1->7,pos@jako+1->jako+2;=:4,5;+:3) =DIR1(=:1,add+4,2) =DIR2(=:2;+:3,po+4;-:1,po+3) +ORIG(+:4,za+5,2) -ADDR(-:4,za+5,2)')
    parsed_valdiff = parse_valdiff(None, stream)
    assert parsed_valdiff.actant.form.eq == ['4', '5']
