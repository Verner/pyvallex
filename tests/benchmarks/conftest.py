import gc
import pytest  # type: ignore
import statistics
import timeit


from collections import defaultdict
from functools import wraps
from pathlib import Path

from selenium.webdriver.support.ui import WebDriverWait  # type: ignore

from tests.browser.utils import performance_marks  # type: ignore
from tests.benchmarks.stats import Stats, get_commit_info, get_machine_id, get_meta_info  # type: ignore

import vallex.json_utils as json


@pytest.fixture(scope="function")
def collect_marks(request):
    BENCHMARK_RESULTS = request.config._BENCHMARK_RESULTS

    def runner(marks, iterations, func, *args, **kwargs):
        collected = defaultdict(Stats)
        for i in range(iterations):
            selenium = kwargs['selenium']
            func(*args, **kwargs)
            end_marks = WebDriverWait(selenium, 20).until(
                performance_marks([mark+':end' for mark in marks])
            )
            start_marks = performance_marks([mark+':start' for mark in marks])(selenium)
            for mark in marks:
                collected[mark].update(end_marks[mark+':end'][0]['startTime']-start_marks[mark+':start'][0]['startTime'])
        BENCHMARK_RESULTS[func.__name__].update(collected)
        BENCHMARK_RESULTS[func.__name__]['__meta__'] = {'browser': selenium.capabilities['browserName'], 'version': selenium.capabilities.get('browserVersion', selenium.capabilities.get('version', 'Unknown'))}
    yield runner


@pytest.fixture(scope="function")
def collect_runs(request):
    BENCHMARK_RESULTS = request.config._BENCHMARK_RESULTS

    def runner(iterations, func, *args, **kwargs):
        collected = Stats()
        for i in range(iterations):
            gcold = gc.isenabled()
            gc.disable()
            try:
                start_time = timeit.default_timer()
                func(*args, **kwargs)
                end_time = timeit.default_timer()
                collected.update((end_time - start_time)*1000)
            finally:
                if gcold:
                    gc.enable()
        BENCHMARK_RESULTS[func.__name__]['run_time'] = collected
        BENCHMARK_RESULTS[func.__name__]['__meta__'] = {}
    yield runner


@pytest.hookimpl(hookwrapper=True)
def pytest_terminal_summary(terminalreporter):
    yield
    meta = {'machine': get_machine_id(), 'commit': get_commit_info()}
    BENCHMARK_RESULTS = terminalreporter.config._BENCHMARK_RESULTS
    if BENCHMARK_RESULTS:
        max_name_len = max([len(name) for name in BENCHMARK_RESULTS.keys()])
        max_mark_name = max([len(mark)+2 for name, results in BENCHMARK_RESULTS.items() for mark in results.keys()])
        first_col_len = max(max_name_len, max_mark_name)
        header = ("  {:^"+str(first_col_len)+"} ").format("test/mark")+Stats.header()
        terminalreporter.ensure_newline()
        terminalreporter.write_line(('{:=^'+str(len(header))+'}').format('  BENCHMARK RESULTS  '), bold=True)
        terminalreporter.write_line(str(meta))
        terminalreporter.write_line("Browser: "+str(list(BENCHMARK_RESULTS.values())[0]['__meta__']))
        terminalreporter.write_line('')
        terminalreporter.write_line(header)
        terminalreporter.write_line('-'*len(header))
        for bench_name, results in BENCHMARK_RESULTS.items():
            terminalreporter.write_line(bench_name, white=True)
            for mark, runs in results.items():
                if mark.startswith('__'):
                    continue
                terminalreporter.write_line(("  {:<"+str(first_col_len)+"} ").format(mark)+str(runs))


@pytest.mark.trylast  # force the other plugins to initialise, fixes issue with capture not being properly initialised
def pytest_configure(config):
    config._BENCHMARK_RESULTS = defaultdict(lambda: defaultdict())


@pytest.hookimpl(hookwrapper=True)
def pytest_sessionfinish(session, exitstatus):
    fname = session.config.getoption('bench_save',  None)
    if fname:
        fname = Path(fname)
        if fname.exists():
            bench_data = json.loads(fname.read_text(encoding='utf-8'))
        else:
            bench_data = []
        bench_data.append({
            'meta': get_meta_info(),
            'benchmarks': session.config._BENCHMARK_RESULTS
        })
        fname.write_text(json.dumps(bench_data), encoding='utf-8')
    yield


def pytest_addoption(parser):
    group = parser.getgroup("benchmark")
    group.addoption(
        "--bench-save",
        metavar="BENCHMARK_OUTPUT", type=str,
        help="Save benchmark results to file."
    )
