import pytest
import statistics

from tests.browser.utils import conditional_skip, web_server

from tests.benchmarks.conftest import collect_marks


@pytest.fixture(name="chrome_options")
def fixture_chrome_options(chrome_options):
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--disable-dev-shm-usage')
    return chrome_options


MARKS = ['api/load', 'api/load/cfg', 'api/load/lexicon/net', 'api/load/lexicon/process', 'App/gui/startup', 'App/gui/Sidebar', 'App/gui/SearchForm', 'App/gui/ResultTable']


def bench_startup(web_server, selenium):
    port = web_server[2]
    selenium.get('http://localhost:'+str(port)+'/')


@pytest.mark.skip_gitlab
@pytest.mark.slow
@pytest.mark.skip(reason="Selenium tests fail, probably they require appropriate webdriver")
def test_bench_startup(web_server, selenium, collect_marks):
    collect_marks(MARKS, 10, bench_startup, web_server, selenium=selenium)
